<?php

include_once(BASE_PATH . 'class/data/dao.php');
include_once(BASE_PATH . 'class/data/dao_exception.php');

class PhotoDAO extends DAO
{
// SQL STATEMENTS
    private $insertSql = "INSERT INTO photo(create_date, update_date, creator, album, file, description)   
                                 VALUES(:create_date, :update_date, :creator, :album, :file, :description)";

    private $selectSql = "SELECT * FROM photo WHERE id = :id";

    private $selectByAlbumSql = "SELECT * FROM photo WHERE album = :albumId";
    
    private $deleteSql = "DELETE FROM photo WHERE id = :id";

    private $deleteByAlbumSql = "DELETE FROM photo WHERE album = :albumId"; 

    private $updateSql = "UPDATE photo SET ";

    private $countSql = "SELECT COUNT(id) AS cnt FROM photo";

    private $countByAlbumSql = "SELECT COUNT(id) AS cnt FROM photo WHERE album = :albumId";

    private $listSql = "SELECT * FROM photo LIMIT :rows OFFSET :offset";

    public function insert($values)
    {
        if (!isset($values))
            throw new DAOException('The value array is empty!');

        if (!isset($values['creator']) || !is_numeric($values['creator']) || $values['creator'] <= 0)
            throw new DAOException('Invalid creator value - it should be numeric and greater than zero!');

        if (!isset($values['album']) || !is_numeric($values['album']) || $values['album'] <= 0)
            throw new DAOException('Invalid album value - it should be numeric and greater than zero!');

// TODO : check creator in user table and return zero when not exist! 
//      : this can only be done when user is implemented

        if (!isset($values['file']) || strlen(trim($values['file'])) <= 0)
            throw new DAOException('Empty photo file!');

        $id = 0;

        try {

            $stmt = $this->pdo->prepare($this->insertSql);

            $tempDate = date('Y-m-d H:i:s'); 

            $stmt->bindParam(':create_date', $tempDate);
            $stmt->bindParam(':update_date', $tempDate);
            $stmt->bindParam(':creator', $values['creator']);
            $stmt->bindParam(':file', $values['file']);
            $stmt->bindParam(':album', $values['album']);
            $stmt->bindParam(':description', $values['description']);

            try {
                $stmt->execute();
                $id = $this->pdo->lastInsertId(); 
            } 
            catch (PDOException $e) {
                $id = 0;
                throw new DAOException('Error in the database!', 0, $e);
            }
        }   
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }

        return $id;
    }

    /*
     *
     */
    public function select($id)
    {
        if (!isset($id)) 
            throw new DAOException('Empty ID!');

        if (!is_numeric($id))
            throw new DAOException('ID is not numeric');

        try {
            $stmt = $this->pdo->prepare($this->selectSql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $a = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($a)
                return $a;
            else
                return array();
        }
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

    public function selectAlbumPhoto($albumId)
    {
        if (!isset($albumId)) 
            throw new DAOException('Empty Album ID!');

        if (!is_numeric($albumId))
            throw new DAOException('Album ID is not numeric');

        try {
            $stmt = $this->pdo->prepare($this->selectByAlbumSql);
            $stmt->bindParam(':albumId', $albumId);
            $stmt->execute();
            $a = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($a)
                return $a;
            else
                return array();
        }
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

    /*
     *
     */
    public function update($id, $values)
    {
        if (!isset($id))
            throw new DAOException('ID is empty!');

        if (!is_numeric($id))
            throw new DAOException('ID is not numeric!');

        if (!isset($values))
            throw new DAOException('The value array is empty!');

        if (!is_array($values))
            throw new DAOException('Values shoule be type of "Array"!');

        if (!isset($values['album']) || $values['album'] == NULL)
            throw new DAOException('Album ID is empty!');

        if (!is_numeric($values['album']))
            throw new DAOException('Album ID is not numeric!');

        if ($values['album'] <= 0)
            throw new DAOException('Album ID should be greater than zero!');

        if (!isset($values['file']) || $values['file'] == NULL || strlen(trim($values['file'])) <= 0)
            throw new DAOException('File is empty!');

        if (is_array($values) && count($values) > 0) {
            $params = array();
            $sql = "";

            if (isset($values['creator']) && $values['creator'] > 0) {
                $sql .= " creator = ?"; 
                $params[] = $values['creator'];
            }

            if (isset($values['album']) && $values['album'] > 0) {
                if (strlen(trim($sql)) > 0)
                    $sql .= ", ";

                $sql .= " album = ?"; 
                $params[] = $values['album'];
            }

            if (isset($values['file']) && strlen(trim($values['file'])) > 0) {
                if (strlen(trim($sql)) > 0)
                    $sql .= ", ";

                $sql .= "file = ?";
                $params[] = $values['file'];
            } 

            if (isset($values['description']) && strlen(trim($values['description'])) > 0) {
                if (strlen(trim($sql)) > 0)
                    $sql .= ", ";

                $sql .= "description = ?";
                $params[] = $values['description'];
            } 
            
            $sql = $this->updateSql . $sql . " WHERE id = ?";
            $params[] = $id;

            try {
                if ($sql != "" && strlen($sql) > 0) {
                    $stmt = $this->pdo->prepare($sql);    

                    try {
                        $stmt->execute($params);
                    }
                    catch (PDOException $e) {
                        throw new DAOException('Error in the database!', 0, $e);
                    }
                }
            }
            catch (PDOException $e) {
                throw new DAOException('Error in the database!', 0, $e);
            }
        }
    }

    /*
     *
     */
    public function delete($id)
    {
        $ret = false;

        if (!isset($id)) 
            throw new DAOException('Empty ID!');

        if (!is_numeric($id))
            throw new DAOException('ID is not numeric');

        try {
            $stmt = $this->pdo->prepare($this->deleteSql);
            $stmt->bindParam(':id', $id);

            try {
                $ret = $stmt->execute();
            }
            catch (PDOException $e) {
                throw new DAOException('Error in the database!', 0, $e);
            }
        }
        catch (PDOException $e) {
             throw new DAOException('Error in the database!', 0, $e);
        }

        return $ret;
    }

    public function deleteAlbumPhoto($albumId)
    {
        $ret = false;

        if (!isset($albumId)) 
            throw new DAOException('Empty Album ID!');

        if (!is_numeric($albumId))
            throw new DAOException('Album ID is not numeric');

        try {
            $stmt = $this->pdo->prepare($this->deleteByAlbumSql);
            $stmt->bindParam(':albumId', $albumId);

            try {
                $ret = $stmt->execute();
            }
            catch (PDOException $e) {
                throw new DAOException('Error in the database!', 0, $e);
            }
        }
        catch (PDOException $e) {
             throw new DAOException('Error in the database!', 0, $e);
        }

        return $ret;
    }

    /*
     *
     */
    public function count()
    {
        try {
            $stmt = $this->pdo->prepare($this->countSql);
            $stmt->execute();
            $a = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($a)
                return $a['cnt'];
            else
                return 0;
        }
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

    public function countAlbumPhoto($albumId)
    {
        if (!isset($albumId)) 
            throw new DAOException('Empty Album ID!');

        if (!is_numeric($albumId))
            throw new DAOException('Album ID is not numeric');
       
        try {
            $stmt = $this->pdo->prepare($this->countByAlbumSql);
            $stmt->bindParam(':albumId', $albumId);

            $stmt->execute();
            $a = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($a)
                return $a['cnt'];
            else
                return 0;
        }
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

    public function selectList($page, $pageSize)
    {
        if (!isset($page))
            throw new DAOException('Page is empty!');

        if (!is_numeric($page))
            throw new DAOException('Page is not numeric!');
 
        if (!isset($pageSize))
            throw new DAOException('Pagesize is empty!');

        if (!is_numeric($pageSize))
            throw new DAOException('Pagesize is not numeric!');

        try {
            $stmt = $this->pdo->prepare($this->listSql);
            $stmt->bindParam(':rows', $pageSize);
            $offset = $page * $pageSize;
            $stmt->bindParam(':offset', $offset);
            $stmt->execute();
            $a = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($a)
                return $a;
            else
                return array();
        }
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

    public function insertList($list)
    {
        if (!isset($list))
            throw new DAOException('The list is empty!');

        if (!is_array($list))
            throw new DAOException('The list is not of type array!');

        foreach ($list as $a) {
            if (!isset($a))
                throw new DAOException('The value array is empty!');

            if (!isset($a['creator']) || !is_numeric($a['creator']) || $a['creator'] <= 0)
                throw new DAOException('Invalid creator value - it should be numeric and greater than zero!');

            if (!isset($a['album']) || !is_numeric($a['album']) || $a['album'] <= 0)
                throw new DAOException('Invalid album value - it should be numeric and greater than zero!');

    // TODO : check creator in user table and return zero when not exist! 
    //      : this can only be done when user is implemented

            if (!isset($a['file']) || strlen(trim($a['file'])) <= 0)
                throw new DAOException('Empty photo file!');
        }

        $id = NULL;

        try {

            $cnt = count($list);
            $id = array($cnt);
            $stmt = array($cnt);

            for ($i=0; $i<$cnt; $i++) {
                $stmt[$i] = $this->pdo->prepare($this->insertSql);

                $tempDate = date('Y-m-d H:i:s'); 
    
                $stmt[$i]->bindParam(':create_date', $tempDate);
                $stmt[$i]->bindParam(':update_date', $tempDate);
                $stmt[$i]->bindParam(':creator', $list[$i]['creator']);
                $stmt[$i]->bindParam(':file', $list[$i]['file']);
                $stmt[$i]->bindParam(':album', $list[$i]['album']);
                $stmt[$i]->bindParam(':description', $list[$i]['description']);
            }

            try {
                for ($i=0; $i<$cnt; $i++) {
                    $stmt[$i]->execute();
                    $id[$i] = $this->pdo->lastInsertId(); 
                }
            } 
            catch (PDOException $e) {
                $id = NULL;
                throw new DAOException('Error in the database!', 0, $e);
            }
        }   
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }

        return $id;
    }
}

?>
