<?php

/*
 * DAO related exceptions.
 */

class DAOException extends Exception {}
class ReferenceException extends DAOException {}

?>
