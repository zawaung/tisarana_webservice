<?php

abstract class DAO 
{
    protected $pdo = null;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function __destruct()
    {
        $this->pdo = null;
    }

    abstract public function insert($values);
    abstract public function select($id);
    abstract public function update($id, $values);
    abstract public function delete($id);
    abstract public function count();
    abstract public function selectList($page, $pageSize);
    abstract public function insertList($list);

    // TODO : functions to insert, update, delete multiple rows
}

?>
