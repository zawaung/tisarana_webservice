<?php

include_once(BASE_PATH . 'class/data/dao.php');
include_once(BASE_PATH . 'class/data/dao_exception.php');
include_once(BASE_PATH . 'class/data/security/user_dao.php');
include_once(BASE_PATH . 'class/data/security/group_dao.php');
include_once(BASE_PATH . 'class/data/security/action_dao.php');

class AclDAO extends DAO
{
// SQL STATEMENTS
    private $insertSql = "INSERT INTO sec_acl(principal, principal_type, action, create_date, creator) 
                            VALUES(:principal, :principal_type, :action, :create_date, :creator)";

    private $selectSql = "SELECT * FROM sec_acl WHERE principal = :principal AND principal_type = :principal_type AND action = :action";

    private $deleteSql = "DELETE FROM sec_acl WHERE principal = :principal AND principal_type = :principal_type AND action = :action";

//
// ACTION
//
    public function insert($values)
    {
        if (!isset($values))
            throw new DAOException('The value array is empty!');

        if (!isset($values['principal_type']) || strlen(trim($values['principal_type'])) <= 0)
             new DAOException('Empty principal_type!');

        if (trim($values['principal_type']) != 'U' && trim($values['principal_type']) != 'G')
            throw new DAOException('Type can only be U or G!');

        if (!isset($values['principal']) || !is_numeric($values['principal']))
            throw new DAOException('Invalid principal value - it should be numeric!');

        if (!isset($values['action']) || !is_numeric($values['action']) || $values['action'] <= 0)
            throw new DAOException('Invalid action value - it should be numeric and greater than zero!');

        if (!isset($values['creator']) || !is_numeric($values['creator']) || $values['creator'] <= 0)
            throw new DAOException('Invalid creator value - it should be numeric and greater than zero!');

        $pDao = null;
        $principal = '';

    // CHECK PRINCIPAL(GROUP/USER) ACTUALLY EXISTS IN THE DATABASE        
        if (trim($values['principal_type']) == 'U') {
            $pDao = new UserDao($this->pdo);
            $principal = 'User';
        } 
        elseif (trim($values['principal_type']) == 'G') {
            $pDao = new GroupDao($this->pdo);
            $principal = 'Group';
        }

        if (count($pDao->select($values['principal'])) <= 0)
            throw new ReferenceException($principal . ' with id (' . $values['principal'] . ') does not exist!');


    // CHECK ACTION ACTUALLY EXISTS IN THE DATABASE        
        $pDao = new ActionDao($this->pdo);
        if (count($pDao->select($values['action'])) <= 0) 
            throw new ReferenceException('Action with id (' . $values['action'] . ') does not exist!');
        $pDao = null;

        try {

            $stmt = $this->pdo->prepare($this->insertSql);

            $tempDate = date('Y-m-d H:i:s'); 
            $stmt->bindParam(':principal', $values['principal']);
            $stmt->bindParam(':principal_type', $values['principal_type']);
            $stmt->bindParam(':action', $values['action']);
            $stmt->bindParam(':create_date', $tempDate);
            $stmt->bindParam(':creator', $values['creator']);

            return $stmt->execute();
        }   
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

    public function select($values)
    {
        if (!isset($values))
            throw new DAOException('The value array is empty!');

        if (!isset($values['principal_type']) || strlen(trim($values['principal_type'])) <= 0)
            throw new DAOException('Empty principal_type!');

        if (trim($values['principal_type']) != 'U' && trim($values['principal_type']) != 'G')
            throw new DAOException('Type can only be U or G!');

        if (!isset($values['principal']) || !is_numeric($values['principal']))
            throw new DAOException('Invalid principal value - it should be numeric!');

        if (!isset($values['action']) || !is_numeric($values['action']) || $values['action'] <= 0)
            throw new DAOException('Invalid action value - it should be numeric and greater than zero!');

        try {
            $stmt = $this->pdo->prepare($this->selectSql);
            $stmt->bindParam(':principal', $values['principal']);
            $stmt->bindParam(':principal_type', $values['principal_type']);
            $stmt->bindParam(':action', $values['action']);
            $stmt->execute();
            $a = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($a)
                return $a;
            else
                return array();
        }
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

    // update is disabled intentionally in acl
    public function update($id, $values)
    {
        return false;
    }

    public function delete($values)
    {
        if (!isset($values['principal_type']) || strlen(trim($values['principal_type'])) <= 0)
            throw new DAOException('Empty principal_type!');

        if (trim($values['principal_type']) != 'U' && trim($values['principal_type']) != 'G')
            throw new DAOException('Type can only be U or G!');

        if (!isset($values['principal']) || !is_numeric($values['principal']))
            throw new DAOException('Invalid principal value - it should be numeric!');

        if (!isset($values['action']) || !is_numeric($values['action']) || $values['action'] <= 0)
            throw new DAOException('Invalid principal value - it should be numeric and greater than zero!');

        $ret = false;

        try {
            $stmt = $this->pdo->prepare($this->deleteSql);
            $stmt->bindParam(':principal', $values['principal']);
            $stmt->bindParam(':principal_type', $values['principal_type']);
            $stmt->bindParam(':action', $values['action']);

            $ret = $stmt->execute();
        }
        catch (PDOException $e) {
             throw new DAOException('Error in the database!', 0, $e);
        }

        return $ret;
    }

    // count is intentionally disabled in acl
    public function count()
    {
        return -1;
    }

    // select list is intentionally disabled in acl
    public function selectList($page, $pageSize)
    {
        return array();
    }

    // insert list is intentionally disabled in acl
    public function insertList($list)
    {
        return array();      
    }
}

?>
