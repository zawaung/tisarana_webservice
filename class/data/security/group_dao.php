<?php

include_once(BASE_PATH . 'class/data/dao.php');
include_once(BASE_PATH . 'class/data/dao_exception.php');

class GroupDAO extends DAO
{
// SQL STATEMENTS
    private $insertSql = "INSERT INTO sec_group(name, create_date, update_date, creator) 
                            VALUES(:name, :create_date, :update_date, :creator)";

    private $selectSql = "SELECT * FROM sec_group WHERE id = :id";

    private $deleteSql = "DELETE FROM sec_group WHERE id = :id";

    private $updateSql = "UPDATE sec_group SET ";

    private $countSql = "SELECT COUNT(id) AS cnt FROM sec_group";

    private $listSql = "SELECT * FROM sec_group LIMIT :rows OFFSET :offset";

//
// USER
//
    public function insert($values)
    {
        if (!isset($values))
            throw new DAOException('The value array is empty!');

        if (!isset($values['name']) || strlen(trim($values['name'])) <= 0)
            throw new DAOException('Empty group name!');

        if (!isset($values['creator']) || !is_numeric($values['creator']) || $values['creator'] <= 0)
            throw new DAOException('Invalid creator value - it should be numeric and greater than zero!');
        
        $id = 0;

        try {

            $stmt = $this->pdo->prepare($this->insertSql);

            $tempDate = date('Y-m-d H:i:s'); 
            $stmt->bindParam(':name', $values['name']);
            $stmt->bindParam(':create_date', $tempDate);
            $stmt->bindParam(':update_date', $tempDate);
            $stmt->bindParam(':creator', $values['creator']);

            $stmt->execute();
            $id = $this->pdo->lastInsertId(); 
        }   
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }

        return $id;
    }

    public function select($id)
    {
        if (!isset($id)) 
            throw new DAOException('Empty ID!');

        if (!is_numeric($id))
            throw new DAOException('ID is not numeric');

        try {
            $stmt = $this->pdo->prepare($this->selectSql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $a = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($a)
                return $a;
            else
                return array();
        }
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

    public function update($id, $values)
    {
        if (!isset($id))
            throw new DAOException('ID is empty!');

        if (!is_numeric($id))
            throw new DAOException('ID is not numeric!');

        if (!isset($values))
            throw new DAOException('The value array is empty!');

        if (is_array($values) && count($values) > 0) {
            $params = array();
            $sql = "";

            if (isset($values['creator']) && $values['creator'] > 0) {
                $sql .= " creator = ?"; 
                $params[] = $values['creator'];
            }

            if (isset($values['name']) && strlen(trim($values['name'])) > 0) {
                if (strlen(trim($sql)) > 0)
                    $sql .= ", ";

                $sql .= "name = ?";
                $params[] = $values['name'];
            } 

            if (isset($values['create_date']) && strlen(trim($values['create_date'])) > 0) {
                if (strlen(trim($sql)) > 0)
                    $sql .= ", ";

                $sql .= "create_date = ?";
                $params[] = $values['create_date'];
            } 

            if (isset($values['update_date']) && strlen(trim($values['update_date'])) > 0) {
                if (strlen(trim($sql)) > 0)
                    $sql .= ", ";

                $sql .= "update_date = ?";
                $params[] = $values['update_date'];
            } 

            $sql = $this->updateSql . $sql . " WHERE id = ?";
            $params[] = $id;

            try {
                if ($sql != "" && strlen($sql) > 0) {
                    $stmt = $this->pdo->prepare($sql);    
                    $stmt->execute($params);
                }
            }
            catch (PDOException $e) {
                throw new DAOException('Error in the database!', 0, $e);
            }
        }
    }

    public function delete($id)
    {
        $ret = false;

        if (!isset($id)) 
            throw new DAOException('Empty ID!');

        if (!is_numeric($id))
            throw new DAOException('ID is not numeric');

        try {
            $stmt = $this->pdo->prepare($this->deleteSql);
            $stmt->bindParam(':id', $id);

            $ret = $stmt->execute();
        }
        catch (PDOException $e) {
             throw new DAOException('Error in the database!', 0, $e);
        }

        return $ret;
    }

    public function count()
    {
        try {
            $stmt = $this->pdo->prepare($this->countSql);
            $stmt->execute();
            $a = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($a)
                return $a['cnt'];
            else
                return 0;
        }
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

    public function selectList($page, $pageSize)
    {
        if (!isset($page))
            throw new DAOException('Page is empty!');

        if (!is_numeric($page))
            throw new DAOException('Page is not numeric!');
 
        if (!isset($pageSize))
            throw new DAOException('Pagesize is empty!');

        if (!is_numeric($pageSize))
            throw new DAOException('Pagesize is not numeric!');

        try {
            $stmt = $this->pdo->prepare($this->listSql);
            $stmt->bindParam(':rows', $pageSize);
            $offset = ($page - 1) * $pageSize;
            $stmt->bindParam(':offset', $offset);
            $stmt->execute();
            $a = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($a)
                return $a;
            else
                return array();
        }
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

    // insert list is intentionally disabled in group
    public function insertList($list)
    {
        return array();      
    }
}

?>
