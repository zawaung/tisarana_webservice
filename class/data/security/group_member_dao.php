<?php

include_once(BASE_PATH . 'class/data/dao.php');
include_once(BASE_PATH . 'class/data/dao_exception.php');

class GroupMemberDAO extends DAO
{
// SQL STATEMENTS
    private $insertSql = "INSERT INTO sec_group_member(group_id, user_id, create_date, creator) 
                            VALUES(:group_id, :user_id, :create_date, :creator)";

    private $selectSql = "SELECT * FROM sec_group_member WHERE group_id = :group_id";

    private $deleteSql = "DELETE FROM sec_group_member WHERE group_id = :group_id"; 

    private $updateSql = "";

    private $countSql = "SELECT COUNT(*) AS cnt FROM sec_group_member";

    private $listSql = "SELECT * FROM sec_group_member LIMIT :rows OFFSET :offset";

//
// USER
//
    public function insert($values)
    {
        if (!isset($values))
            throw new DAOException('The value array is empty!');

        if (!isset($values['group_id']) || !is_numeric($values['group_id']) || $values['group_id'] <= 0)
            throw new DAOException('Invalid group id value - it should be numeric and greater than zero!');

        if (!isset($values['user_id']) || !is_numeric($values['user_id']) || $values['user_id'] <= 0)
            throw new DAOException('Invalid user id value - it should be numeric and greater than zero!');

        if (!isset($values['creator']) || !is_numeric($values['creator']) || $values['creator'] <= 0)
            throw new DAOException('Invalid creator value - it should be numeric and greater than zero!');
        
        $id = false;

        try {

            $stmt = $this->pdo->prepare($this->insertSql);

            $tempDate = date('Y-m-d H:i:s'); 
            $stmt->bindParam(':group_id', $values['group_id']);
            $stmt->bindParam(':user_id', $values['user_id']);
            $stmt->bindParam(':create_date', $tempDate);
            $stmt->bindParam(':creator', $values['creator']);

            $id = $stmt->execute();
        }   
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }

        return $id;
    }

    public function select($values)
    {
        if (!isset($values['group_id']) || !is_numeric($values['group_id']) || $values['group_id'] <= 0)
            throw new DAOException('Invalid group id value - it should be numeric and greater than zero!');

        try {
            $sql = $this->selectSql;
            $validUserId = (isset($values['user_id']) 
                                && is_numeric($values['user_id']) 
                                && $values['user_id'] > 0);

            if ($validUserId) {
                $sql .= " AND user_id = :user_id";
            }

            $stmt = $this->pdo->prepare($sql);
            $stmt->bindParam(':group_id', $values['group_id']);
            if ($validUserId) {
                $stmt->bindParam(':user_id', $values['user_id']);
            }
            $stmt->execute();
            $a = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($a)
                return $a;
            else
                return array();
        }
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

//
// NOTE: There is no update function for group member because this is just 
//       a linking of group and member. We add a new record if we want to
//       add a user to a group and we delete the record if we are to remove
//       a user from a group. We just return true here.
//
    public function update($id, $values)
    {
        return true;
    }

    public function delete($values)
    {
        if (!isset($values['group_id']) || !is_numeric($values['group_id']) || $values['group_id'] <= 0)
            throw new DAOException('Invalid group id value - it should be numeric and greater than zero!');

        try {
            $sql = $this->deleteSql;
            $validUserId = (isset($values['user_id']) 
                                && is_numeric($values['user_id']) 
                                && $values['user_id'] > 0);

            if ($validUserId) {
                $sql .= " AND user_id = :user_id";
            }

            $stmt = $this->pdo->prepare($sql);
            $stmt->bindParam(':group_id', $values['group_id']);
            if ($validUserId) {
                $stmt->bindParam(':user_id', $values['user_id']);
            }

            return $stmt->execute();
        }
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

    public function count()
    {
        try {
            $stmt = $this->pdo->prepare($this->countSql);
            $stmt->execute();
            $a = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($a)
                return $a['cnt'];
            else
                return 0;
        }
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

    public function selectList($page, $pageSize)
    {
        if (!isset($page))
            throw new DAOException('Page is empty!');

        if (!is_numeric($page))
            throw new DAOException('Page is not numeric!');
 
        if (!isset($pageSize))
            throw new DAOException('Pagesize is empty!');

        if (!is_numeric($pageSize))
            throw new DAOException('Pagesize is not numeric!');

        try {
            $stmt = $this->pdo->prepare($this->listSql);
            $stmt->bindParam(':rows', $pageSize);
            $offset = ($page - 1) * $pageSize;
            $stmt->bindParam(':offset', $offset);
            $stmt->execute();
            $a = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($a)
                return $a;
            else
                return array();
        }
        catch (PDOException $e) {
            throw new DAOException('Error in the database!', 0, $e);
        }
    }

    // insert list is intentionally disabled in group
    public function insertList($list)
    {
        return array();      
    }
}

?>
