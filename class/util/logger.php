<?php

class Logger 
{
    const ERROR = LOG_ERROR;
    const WARNING = LOG_WARNING;
    const NOTICE = LOG_NOTICE;
    const INFO = LOG_INFO;
    const DEBUG = LOG_DEBUG;

    public static function log($message, $type = LOG_INFO) 
    {
        openlog(APP_NAME, LOG_PID | LOG_NDELAY, LOG_USER);
        syslog($type, $message);
    }
} 

?>
