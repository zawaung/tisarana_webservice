<?php

/*
 * All files are hashed using SHA1 and stored by their hashed value in hexadecimal 
 * as name under the folder named by the first 2 characters of the name. E.g. 
 * if hexdecimal value of the hash of a file called 'example.gif' is 
 * 'f43703511415436d15a72af118b29672e4edb35d', this file will be stored under a direcotry 
 * called 'f4'.  So the file full path will look like below.
 *
 * [storage path]/f4/f43703511415436d15a72af118b29672e4edb35d
 */
class FileStorageManager
{
    const CHUNK_SIZE = 8192;
    const TMP_NAME_PREFIX = 'ws';
    const HASH_LENGTH = 40; // the length of SHA1 hash in Hex format 

    // return the sha1 hash of the file after success
    // if the file is already in the storage by checking the hash false is return
    public static function processInbound($fileName)
    {
        if (!file_exists($fileName) || filesize($fileName) <= 0) {
            throw new Exception('Invalid or size zero uploaded file!');
        }

        $hash = sha1_file($fileName);
        $storagePath = self::getPath($hash); 
        if (!is_dir($storagePath)) {
            mkdir($storagePath, 0700, true);
        }

        $nameInStorage = $storagePath . $hash;

        if (!file_exists($nameInStorage)) {
            $if = fopen($fileName, 'r');
            $of = fopen($nameInStorage, 'w');

            while ($chunk = fread($if, self::CHUNK_SIZE)) {
                fwrite($of, $chunk, strlen($chunk));
            }

            fclose($if);
            fclose($of);

            chmod($nameInStorage, 0600);
        }
        else {
            $hash = false;
        }

        return $hash;
    }

    public static function processOutbound($hash)
    {
        $outFile = CACHE_STORAGE_PATH . $hash;

        if (file_exists($outFile) && filesize($outFile) > 0) {
            return $outFile; 
        }
        else {
            $name = self::getPath($hash) . $hash;
            if (file_exists($name)) {
                $if = fopen($name, 'r');
                $of = fopen($outFile, 'w');
                while ($chunk = fread($if, self::CHUNK_SIZE)) {
                    fwrite($of, $chunk, strlen($chunk));
                }

                fclose($if);
                fclose($of);

                return $outFile;
            }
            else {
                return false;
            }
        }
    }

    // get the path to storage of uploade file by its' hash
    public static function getPath($hash)
    {
        return TEMP_STORAGE_PATH . substr($hash, 0, 2) . DIRECTORY_SEPARATOR;
    }
}

?>
