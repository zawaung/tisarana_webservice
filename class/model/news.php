<?php

include_once(BASE_PATH . 'class/model/model.php');

/* 
 * To model news.
 */

class News extends Model  
{
    protected $id = 0;
    protected $createDate;
    protected $updateDate;
    protected $creator;
    protected $title;
    protected $details;
    protected $publishDate;

    public function validate()
    {
        $valid = true;
        $this->clearError();

        if ($this->state != Model::STATE_NEW) {
            if (is_null($this->id)) {
                $valid = false;
                $this->addError('id', 'ID is null');
            } 

            if (!is_numeric($this->id)) {
                $valid = false;
                $this->addError('id', 'ID is not numeric');
            }

            if (is_numeric($this->id) && $this->id <=0) {
                $valid = false;
                $this->addError('id', 'ID is not greater than zero');
            }
        }

        if (is_null($this->title)) {
            $valid = false;
            $this->addError('title', 'Title is null');
        }

        if (!is_null($this->title) && strlen(trim($this->title)) <= 0) {
            $valid = false;
            $this->addError('title', 'Title is blank');
        }

        if (is_null($this->details)) {
            $valid = false;
            $this->addError('details', 'Details is null');
        }

        if (!is_null($this->details) && strlen(trim($this->details)) <= 0) {
            $valid = false;
            $this->addError('details', 'Details is blank');
        }

        return $valid;
    }

    public function read()
    {
        $ret = false;

        if ($this->pm->read($this)) {
            $this->state = Model::STATE_NORMAL;
            $ret = true;
        }  

        return $ret;
    }

    public function save()
    {
        $ret = false;

        if ($this->validate()) {
            if (Model::STATE_NEW == $this->state) {
                $this->id = $this->pm->add($this);
        
                if ($this->id > 0) {
                    $this->state = Model::STATE_NORMAL;
                    $ret = true;
                }
            }
            elseif (Model::STATE_DIRTY == $this->state) {
                $this->pm->update($this);
                $ret = true;
            }
        }

        return $ret;
    }

    public function delete()
    {
        $ret = false;

        if ($this->pm->delete($this)) {
            $this-> id = 0;
            $this->createDate = '';
            $this->updateDate = '';
            $this->creator = 0;
            $this->title = '';
            $this->details = '';

            $ret = true;
        }

        return $ret;
    }

    public function toArray()
    {
        return array('id' => (is_null($this->id)?"":$this->id),
                        'createDate' => (is_null($this->createDate)?"":$this->createDate),
                        'updateDate' => (is_null($this->updateDate)?"":$this->updateDate),
                        'creator' => (is_null($this->creator)?"":$this->creator),
                        'title' => (is_null($this->title)?"":$this->title),
                        'details' => (is_null($this->details)?"":$this->details),
                        'publishDate' => (is_null($this->publishDate)?"":$this->publishDate));    
    }

    public function fromArray($a)
    {
        if (is_array($a) && count($a) > 0) {
            $this->id = $a['id'];
            $this->createDate = $a['createDate'];
            $this->updateDate = $a['updateDate'];
            $this->creator = $a['creator'];
            $this->title = $a['title'];
            $this->details = $a['details'];
            $this->publishDate = $a['publishDate'];
        }
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public function fromJson($json)
    {
        $this->fromArray(json_decode($json, true));
    }

    public function equals($model)
    {
        return ($model->id == $this->id &&
                $model->createDate == $this->createDate &&
                $model->updateDate == $this->updateDate &&
                $model->creator == $this->creator &&
                $model->title == $this->title &&
                $model->details == $this->details &&
                $model->publishDate == $this->publishDate);
    }

    public function equalsArray($a)
    {
        return ($a['id'] == $this->id &&
                $a['createDate'] == $this->createDate &&
                $a['updateDate'] == $this->updateDate &&
                $a['creator'] == $this->creator &&
                $a['title'] == $this->title &&
                $a['details'] == $this->details &&
                $a['publishDate'] == $this->publishDate);
    }
}

?>
