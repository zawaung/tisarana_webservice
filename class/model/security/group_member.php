<?php

include_once(BASE_PATH . 'class/model/model.php');

/* 
 * To model album.
 */

class GroupMember extends Model  
{
    protected $group_id = 0;
    protected $user_id = 0;
    protected $createDate;
    protected $creator;

    public function validate()
    {
        $valid = true;
        $this->clearError();

        if (is_null($this->group_id)) {
            $valid = false;
            $this->addError('group_id', 'GroupMember ID is null');
        } 

        if (!is_numeric($this->group_id)) {
            $valid = false;
            $this->addError('group_id', 'GroupMember ID is not numeric');
        }

        if (is_numeric($this->group_id) && $this->group_id <=0) {
            $valid = false;
            $this->addError('group_id', 'GroupMember ID is not greater than zero');
        }

        if (is_null($this->user_id)) {
            $valid = false;
            $this->addError('user_id', 'User ID is null');
        } 

        if (!is_numeric($this->user_id)) {
            $valid = false;
            $this->addError('user_id', 'User ID is not numeric');
        }

        if (is_numeric($this->user_id) && $this->user_id <=0) {
            $valid = false;
            $this->addError('user_id', 'User ID is not greater than zero');
        }

        return $valid;
    }

    public function read()
    {
        $ret = false;

        if ($this->pm->read($this)) {
            $this->state = Model::STATE_NORMAL;
            $ret = true;
        }  

        return $ret;
    }

    public function save()
    {
        $ret = false;

        if ($this->validate()) {
            if (Model::STATE_NEW == $this->state) {
                if ($this->pm->add($this)) {
                    $this->state = Model::STATE_NORMAL;
                    $ret = true;
                }
            }
            elseif (Model::STATE_DIRTY == $this->state) {
                $this->pm->update($this);
                $ret = true;
            }
        }

        return $ret;
    }

    public function delete()
    {
        $ret = false;

        if ($this->pm->delete($this)) {
            $this->createDate = '';
            $this->creator = 0;
            $this->group_id = 0;
            $this->user_id = 0;

            $ret = true;
        }

        return $ret;
    }

    public function toArray()
    {
        return array('createDate' => (is_null($this->createDate)?"":$this->createDate),
                    'creator' => (is_null($this->creator)?"":$this->creator),
                    'group_id' => (is_null($this->group_id)?"":$this->group_id),
                    'user_id' => (is_null($this->user_id)?"":$this->user_id));
    }

    public function fromArray($a)
    {
        if (is_array($a) && count($a) > 0) {
            $this->createDate = $a['createDate'];
            $this->creator = $a['creator'];
     		$this->group_id = $a['group_id'];
     		$this->user_id = $a['user_id'];
        }
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public function fromJson($json)
    {
        $this->fromArray(json_decode($json, true));
    }

    public function equals($model)
    {
        return ($model->createDate == $this->createDate &&
                $model->creator == $this->creator &&
                $model->group_id == $this->group_id &&
                $model->user_id == $this->user_id);
    }

    public function equalsArray($a)
    {
        $model = new GroupMember($this->pm);
        $model->fromArray($a);
        return $this->equals($model);
    }
}

?>
