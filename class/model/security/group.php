<?php

include_once(BASE_PATH . 'class/model/model.php');

/* 
 * To model album.
 */

class Group extends Model  
{
    protected $id = 0;
    protected $createDate;
    protected $updateDate;
    protected $creator;
    protected $name;

    public function validate()
    {
        $valid = true;
        $this->clearError();

        if ($this->state != Model::STATE_NEW) {
            if (is_null($this->id)) {
                $valid = false;
                $this->addError('id', 'ID is null');
            } 

            if (!is_numeric($this->id)) {
                $valid = false;
                $this->addError('id', 'ID is not numeric');
            }

            if (is_numeric($this->id) && $this->id <=0) {
                $valid = false;
                $this->addError('id', 'ID is not greater than zero');
            }
        }

        if (is_null($this->name)) {
            $valid = false;
            $this->addError('name', 'Name is null');
        }

        if (!is_null($this->name) && strlen(trim($this->name)) <= 0) {
            $valid = false;
            $this->addError('name', 'Name is blank');
        }

        return $valid;
    }

    public function read()
    {
        $ret = false;

        if ($this->pm->read($this)) {
            $this->state = Model::STATE_NORMAL;
            $ret = true;
        }  

        return $ret;
    }

    public function save()
    {
        $ret = false;

        if ($this->validate()) {
            if (Model::STATE_NEW == $this->state) {
                $this->id = $this->pm->add($this);
        
                if ($this->id > 0) {
                    $this->state = Model::STATE_NORMAL;
                    $ret = true;
                }
            }
            elseif (Model::STATE_DIRTY == $this->state) {
                $this->pm->update($this);
                $ret = true;
            }
        }

        return $ret;
    }

    public function delete()
    {
        $ret = false;

        if ($this->pm->delete($this)) {
            $this->id = 0;
            $this->createDate = '';
            $this->updateDate = '';
            $this->creator = 0;
     		$this->name = '';

            $ret = true;
        }

        return $ret;
    }

    public function toArray()
    {
        return array('id' => (is_null($this->id)?"":$this->id),
                    'createDate' => (is_null($this->createDate)?"":$this->createDate),
                    'updateDate' => (is_null($this->updateDate)?"":$this->updateDate),
                    'creator' => (is_null($this->creator)?"":$this->creator),
                    'name' => (is_null($this->name)?"":$this->name));
    }

    public function fromArray($a)
    {
        if (is_array($a) && count($a) > 0) {
            $this->id = $a['id'];
            $this->createDate = $a['createDate'];
            $this->updateDate = $a['updateDate'];
            $this->creator = $a['creator'];
     		$this->name = $a['name'];
        }
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public function fromJson($json)
    {
        $this->fromArray(json_decode($json, true));
    }

    public function equals($model)
    {
        return ($model->id == $this->id &&
                $model->createDate == $this->createDate &&
                $model->updateDate == $this->updateDate &&
                $model->creator == $this->creator &&
                $model->name == $this->name);
    }

    public function equalsArray($a)
    {
        $model = new Group($this->pm);
        $model->fromArray($a);
        return $this->equals($model);
    }
}

?>
