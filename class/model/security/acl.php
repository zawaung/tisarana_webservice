<?php

include_once(BASE_PATH . 'class/model/model.php');

/* 
 * To model album.
 */

class Acl extends Model  
{
    protected $principal = 0;
    protected $action = 0;
    protected $createDate;
    protected $creator;
    protected $principal_type;

    public function validate()
    {
        $valid = true;
        $this->clearError();

        if (is_null($this->principal)) {
            $valid = false;
            $this->addError('principal', 'Acl ID is null');
        } 

        if (!is_numeric($this->principal)) {
            $valid = false;
            $this->addError('principal', 'Acl ID is not numeric');
        }

        if (is_numeric($this->principal) && $this->principal <=0) {
            $valid = false;
            $this->addError('principal', 'Acl ID is not greater than zero');
        }

        if (is_null($this->action)) {
            $valid = false;
            $this->addError('action', 'User ID is null');
        } 

        if (!is_numeric($this->action)) {
            $valid = false;
            $this->addError('action', 'User ID is not numeric');
        }

        if (is_numeric($this->action) && $this->action <=0) {
            $valid = false;
            $this->addError('action', 'User ID is not greater than zero');
        }

        return $valid;
    }

    public function read()
    {
        $ret = false;

        if ($this->pm->read($this)) {
            $this->state = Model::STATE_NORMAL;
            $ret = true;
        }  

        return $ret;
    }

    public function save()
    {
        $ret = false;

        if ($this->validate()) {
            if (Model::STATE_NEW == $this->state) {
                if ($this->pm->add($this)) {
                    $this->state = Model::STATE_NORMAL;
                    $ret = true;
                }
            }
            elseif (Model::STATE_DIRTY == $this->state) {
                $this->pm->update($this);
                $ret = true;
            }
        }

        return $ret;
    }

    public function delete()
    {
        $ret = false;

        if ($this->pm->delete($this)) {
            $this->createDate = '';
            $this->creator = 0;
            $this->principal = 0;
            $this->action = 0;
            $this->principal_type = '';

            $ret = true;
        }

        return $ret;
    }

    public function toArray()
    {
        return array('createDate' => (is_null($this->createDate)?"":$this->createDate),
                    'creator' => (is_null($this->creator)?"":$this->creator),
                    'principal' => (is_null($this->principal)?"":$this->principal),
                    'action' => (is_null($this->action)?"":$this->action),
                    'principal_type' => (is_null($this->principal_type)?"":$this->principal_type));
    }

    public function fromArray($a)
    {
        if (is_array($a) && count($a) > 0) {
            $this->createDate = $a['createDate'];
            $this->creator = $a['creator'];
     		$this->principal = $a['principal'];
     		$this->action = $a['action'];
     		$this->principal_type = $a['principal_type'];
        }
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public function fromJson($json)
    {
        $this->fromArray(json_decode($json, true));
    }

    public function equals($model)
    {
        return ($model->createDate == $this->createDate &&
                $model->creator == $this->creator &&
                $model->principal == $this->principal &&
                $model->action == $this->action && 
                $model->principal_type == $this->principal_type);
    }

    public function equalsArray($a)
    {
        $model = new Acl($this->pm);
        $model->fromArray($a);
        return $this->equals($model);
    }
}

?>
