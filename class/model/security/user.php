<?php

include_once(BASE_PATH . 'class/model/model.php');

/* 
 * To model album.
 */

class User extends Model  
{
    protected $id = 0;
    protected $createDate;
    protected $updateDate;
    protected $creator;
    protected $firstName;
    protected $lastName;
    protected $email;
    protected $loginName;
    protected $password;
    protected $suspended;
    protected $suspendedDate;

    public function validate()
    {
        $valid = true;
        $this->clearError();

        if ($this->state != Model::STATE_NEW) {
            if (is_null($this->id)) {
                $valid = false;
                $this->addError('id', 'ID is null');
            } 

            if (!is_numeric($this->id)) {
                $valid = false;
                $this->addError('id', 'ID is not numeric');
            }

            if (is_numeric($this->id) && $this->id <=0) {
                $valid = false;
                $this->addError('id', 'ID is not greater than zero');
            }
        }

        if (is_null($this->firstName)) {
            $valid = false;
            $this->addError('firstName', 'First Name is null');
        }

        if (!is_null($this->firstName) && strlen(trim($this->firstName)) <= 0) {
            $valid = false;
            $this->addError('firstName', 'First Name is blank');
        }

        if (is_null($this->lastName)) {
            $valid = false;
            $this->addError('lastName', 'Last Name is null');
        }

        if (!is_null($this->lastName) && strlen(trim($this->lastName)) <= 0) {
            $valid = false;
            $this->addError('lastName', 'Last Name is blank');
        }

        if (is_null($this->loginName)) {
            $valid = false;
            $this->addError('loginName', 'Login Name is null');
        }

        if (!is_null($this->loginName) && strlen(trim($this->loginName)) <= 0) {
            $valid = false;
            $this->addError('loginName', 'Login Name is blank');
        }

        if (is_null($this->password)) {
            $valid = false;
            $this->addError('password', 'Password is null');
        }

        if (!is_null($this->password) && strlen(trim($this->password)) <= 0) {
            $valid = false;
            $this->addError('password', 'Password is blank');
        }

        return $valid;
    }

    public function read()
    {
        $ret = false;

        if ($this->pm->read($this)) {
            $this->state = Model::STATE_NORMAL;
            $ret = true;
        }  

        return $ret;
    }

    public function save()
    {
        $ret = false;

        if ($this->validate()) {
            if (Model::STATE_NEW == $this->state) {
                $this->id = $this->pm->add($this);
        
                if ($this->id > 0) {
                    $this->state = Model::STATE_NORMAL;
                    $ret = true;
                }
            }
            elseif (Model::STATE_DIRTY == $this->state) {
                $this->pm->update($this);
                $ret = true;
            }
        }

        return $ret;
    }

    public function delete()
    {
        $ret = false;

        if ($this->pm->delete($this)) {
            $this->id = 0;
            $this->createDate = '';
            $this->updateDate = '';
            $this->creator = 0;
     		$this->firstName = '';
     		$this->lastName = '';
     		$this->email = '';
     		$this->loginName = '';
     		$this->password = '';
     		$this->suspended = '';
     		$this->suspendedDate = '';

            $ret = true;
        }

        return $ret;
    }

    public function toArray()
    {
        return array('id' => (is_null($this->id)?"":$this->id),
                    'createDate' => (is_null($this->createDate)?"":$this->createDate),
                    'updateDate' => (is_null($this->updateDate)?"":$this->updateDate),
                    'creator' => (is_null($this->creator)?"":$this->creator),
                    'firstName' => (is_null($this->firstName)?"":$this->firstName),
                    'lastName' => (is_null($this->lastName)?"":$this->lastName),
                    'email' => (is_null($this->email)?"":$this->email),
                    'loginName' => (is_null($this->loginName)?"":$this->loginName),
                    'password' => (is_null($this->password)?"":$this->password),
                    'suspended' => (is_null($this->suspended)?"":$this->suspended),
                    'suspendedDate' => (is_null($this->suspendedDate)?"":$this->suspendedDate));
    }

    public function fromArray($a)
    {
        if (is_array($a) && count($a) > 0) {
            $this->id = $a['id'];
            $this->createDate = $a['createDate'];
            $this->updateDate = $a['updateDate'];
            $this->creator = $a['creator'];
     		$this->firstName = $a['firstName'];
     		$this->lastName = $a['lastName'];
     		$this->email = $a['email'];
     		$this->loginName = $a['loginName'];
     		$this->password = $a['password'];
     		$this->suspended = $a['suspended'];
     		$this->suspendedDate = $a['suspendedDate'];
        }
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public function fromJson($json)
    {
        $this->fromArray(json_decode($json, true));
    }

    public function equals($model)
    {
        return ($model->id == $this->id &&
                $model->createDate == $this->createDate &&
                $model->updateDate == $this->updateDate &&
                $model->creator == $this->creator &&
                $model->firstName == $this->firstName &&
                $model->lastName == $this->lastName &&
                $model->email == $this->email &&
                $model->loginName == $this->loginName &&
                $model->password == $this->password &&
                $model->suspended == $this->suspended &&
                $model->suspendedDate == $this->suspendedDate);
    }

    public function equalsArray($a)
    {
        $model = new User($this->pm);
        $model->fromArray($a);
        return $this->equals($model);
    }
}

?>
