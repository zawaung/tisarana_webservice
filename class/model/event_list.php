<?php

include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/model_list.php');

class EventList extends ModelList
{
    public function getCount()
    {
        return $this->pm->getCount('Event');
    }

    public function getList()
    {
        return $this->pm->getList($this->currentPage, $this->pageSize, 'Event');
    }

    public function addList($list)
    {
        return $this->pm->addList($list, 'Event');
    }
}

?>
