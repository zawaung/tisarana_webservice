<?php

include_once(BASE_PATH . 'class/model/model.php');
include_once(BASE_PATH . 'class/model/album.php');

/* 
 * To model photo.
 */

class Photo extends Model  
{
    protected $id = 0;
    protected $createDate;
    protected $updateDate;
    protected $creator;
    protected $album;
    protected $description;
    protected $file;

    public function validate()
    {
        $valid = true;
        $this->clearError();

        if ($this->state != Model::STATE_NEW) {
            if (is_null($this->id)) {
                $valid = false;
                $this->addError('id', 'ID is null');
            } 

            if (!is_numeric($this->id)) {
                $valid = false;
                $this->addError('id', 'ID is not numeric');
            }

            if (is_numeric($this->id) && $this->id <=0) {
                $valid = false;
                $this->addError('id', 'ID is not greater than zero');
            }
        }

        if (is_null($this->album)) {
            $valid = false;
            $this->addError('album', 'Album is null');
        }

        if (!is_object($this->album)) {
            $valid = false;
            $this->addError('album', 'Album is not an object');
        }

        if (is_object($this->album) && get_class($this->album) != 'Album') {
            $valid = false;
            $this->addError('album', 'Object type is not Album');
        }

        if (is_null($this->description)) {
            $valid = false;
            $this->adderror('description', 'Description is null');
        }

        if (!is_null($this->description) && strlen(trim($this->description)) <= 0) {
            $valid = false;
            $this->adderror('description', 'Description is blank');
        }

        if (is_null($this->file)) {
            $valid = false;
            $this->adderror('file', 'File is null');
        }

        if (!is_null($this->file) && strlen(trim($this->file)) <= 0) {
            $valid = false;
            $this->adderror('file', 'File is blank');
        }

        return $valid;
    }

    public function read()
    {
        $ret = false;

        if ($this->pm->read($this)) {
            $this->state = Model::STATE_NORMAL;
            $ret = true;
        }  

        return $ret;
    }

    public function save()
    {
        $ret = false;

        if ($this->validate()) {
            if (Model::STATE_NEW == $this->state) {
                $this->id = $this->pm->add($this);
        
                if ($this->id > 0) {
                    $this->state = Model::STATE_NORMAL;
                    $ret = true;
                }
            }
            elseif (Model::STATE_DIRTY == $this->state) {
                $this->pm->update($this);
                $this->state = Model::STATE_NORMAL;
                $ret = true;
            }
        }

        return $ret;
    }

    public function delete()
    {
        $ret = false;

        if ($this->pm->delete($this)) {
            $this-> id = 0;
            $this->createDate = '';
            $this->updateDate = '';
            $this->creator = 0;
            $this->album = NULL;
            $this->description = '';
            $this->file = '';

            $ret = true;
        }

        return $ret;
    }

    public function toArray()
    {
        $album = (new Album($this->pm))->toArray();
        return array('id' => (is_null($this->id)?"":$this->id),
                        'createDate' => (is_null($this->createDate)?"":$this->createDate),
                        'updateDate' => (is_null($this->updateDate)?"":$this->updateDate),
                        'creator' => (is_null($this->creator)?"":$this->creator),
                        'album' => (is_null($this->album)?$album:$this->album->toArray()),
                        'description' => (is_null($this->description)?"":$this->description),
                        'file' => (is_null($this->file)?"":$this->file));
    }

    public function fromArray($a)
    {
        if (is_array($a) && count($a) > 0) {
            $this->id = $a['id'];
            $this->createDate = $a['createDate'];
            $this->updateDate = $a['updateDate'];
            $this->creator = $a['creator'];

            $album = new Album($this->pm);

            if (is_array($a['album']))  
                $album->fromArray($a['album']);
            
            $this->album = $album;

            $this->description = $a['description'];
            $this->file = $a['file'];
        }
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public function fromJson($json)
    {
        $this->fromArray(json_decode($json, true));
    }

    public function equals($model)
    {
        return ($model->id == $this->id &&
                $model->createDate == $this->createDate &&
                $model->updateDate == $this->updateDate &&
                $model->creator == $this->creator &&
                $model->album->equals($this->album) &&
                $model->description == $this->description && 
                $model->file == $this->file);
    }

    public function equalsArray($a)
    {
        return ($a['id'] == $this->id &&
                $a['createDate'] == $this->createDate &&
                $a['updateDate'] == $this->updateDate &&
                $a['creator'] == $this->creator &&
                $this->album->equalsArray($a['album']) &&
                $a['description'] == $this->description && 
                $a['file'] == $this->file);
    }
}

?>
