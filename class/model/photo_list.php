<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'conf.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/model_list.php');

class PhotoList extends ModelList
{
    public function getCount()
    {
        return $this->pm->getCount('Photo');
    }

    public function getList()
    {
        return $this->pm->getList($this->currentPage, $this->pageSize, 'Photo');
    }

    public function addList($list)
    {
        return $this->pm->addList($list, 'Photo');
    }
}

?>
