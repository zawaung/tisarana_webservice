<?php

abstract class ModelList 
{
    const DEFAULT_PAGE_SIZE = 20;

    protected $pm = null;
    protected $pageSize = self::DEFAULT_PAGE_SIZE;
    protected $currentPage = 0;
    protected $totalPage = 0;

    public function __construct($persistentManager)
    {
        if (get_class($persistentManager) == 'PersistentManager') 
            $this->pm = $persistentManager;
        else
            throw new ModelException('The input value is not of type Persistent Manager!');
    }

    public function setPageSize($size)
    {
        if (!is_null($size) && is_numeric($size) && $size > 0) 
            $this->pageSize = $size;
        else
            $this->pageSize = self::DEFAULT_PAGE_SIZE;

        $this->alignPage();
    }

    public function setCurrentPage($page)
    {
        if (!is_null($page) && is_numeric($page) && $page > 0) 
            $this->currentPage = $page;
        else
            $this->currentPage = 0;

        $this->alignPage();
    }

    public function getPageSize()
    {
        return $this->pageSize;
    }

    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    public function getTotalPage()
    {

        $this->alignPage();

        return $this->totalPage;
    }

    private function alignPage()
    {
        $cnt = $this->getCount();

        if ($this->getCount() > 0)
            $this->totalPage = ceil($cnt / $this->pageSize);
        else
            $this->totalPage = 0;

        if ($this->currentPage > $this->totalPage)
            $this->currentPage = $this->totalPage;
    }

    abstract public function getCount();
    abstract public function getList();
    abstract public function addList($list);
}

?>
