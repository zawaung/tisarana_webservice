<?php

abstract class Model 
{
    const STATE_NEW = 0;
    const STATE_DIRTY = 1;
    const STATE_NORMAL = 2;

    protected $pm = null;
    protected $errors = array();
    protected $state = self::STATE_NEW;

    public function __construct($persistentManager)
    {
        if (get_class($persistentManager) == 'PersistentManager') 
            $this->pm = $persistentManager;
        else
            throw new ModelException('The input value is not of type Persistent Manager!');
    }

    public function __set($name, $value)
    {
        $this->{$name} = $value;
        if (self::STATE_NEW != $this->state)
            $this->state = self::STATE_DIRTY;
    }

    public function __get($name)
    {
        return $this->{$name};
    }

    public function getError()
    {
        return $this->errors;
    }

    public function hasError()
    {
        return (count($this->errors) > 0);
    }

    protected function addError($item, $error)
    {
        $this->errors[] = array('item'=>$item, 'error'=>$error);
    }

    protected function clearError()
    {
        $this->errors = null;
        $this->errors = array();
    }

    abstract public function validate();
    abstract public function read();
    abstract public function save();
    abstract public function delete();
    abstract public function toArray();
    abstract public function fromArray($a);
    abstract public function toJson();
    abstract public function fromJson($json);
    abstract public function equals($model);
    abstract public function equalsArray($a);
}

?>
