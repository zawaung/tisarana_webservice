<?php

include_once(BASE_PATH . 'class/model/model.php');

/* 
 * To model album.
 */

class Album extends Model  
{
    protected $id = 0;
    protected $createDate;
    protected $updateDate;
    protected $creator;
    protected $title;
    protected $description;
    protected $photos;

    public function validate()
    {
        $valid = true;
        $this->clearError();

        if ($this->state != Model::STATE_NEW) {
            if (is_null($this->id)) {
                $valid = false;
                $this->addError('id', 'ID is null');
            } 

            if (!is_numeric($this->id)) {
                $valid = false;
                $this->addError('id', 'ID is not numeric');
            }

            if (is_numeric($this->id) && $this->id <=0) {
                $valid = false;
                $this->addError('id', 'ID is not greater than zero');
            }
        }

        if (is_null($this->title)) {
            $valid = false;
            $this->addError('title', 'Title is null');
        }

        if (!is_null($this->title) && strlen(trim($this->title)) <= 0) {
            $valid = false;
            $this->addError('title', 'Title is blank');
        }

        if (is_null($this->description)) {
            $valid = false;
            $this->addError('description', 'Description is null');
        }

        if (!is_null($this->description) && strlen(trim($this->description)) <= 0) {
            $valid = false;
            $this->addError('description', 'Description is blank');
        }

        return $valid;
    }

    public function read()
    {
        $ret = false;

        if ($this->pm->read($this)) {
            $this->state = Model::STATE_NORMAL;
            $ret = true;
        }  

        return $ret;
    }

    public function save()
    {
        $ret = false;

        if ($this->validate()) {
            if (Model::STATE_NEW == $this->state) {
                $this->id = $this->pm->add($this);
        
                if ($this->id > 0) {
                    $this->state = Model::STATE_NORMAL;
                    $ret = true;
                }
            }
            elseif (Model::STATE_DIRTY == $this->state) {
                $this->pm->update($this);
                $ret = true;
            }
        }

        return $ret;
    }

    public function delete()
    {
        $ret = false;

        if ($this->pm->delete($this)) {
            $this-> id = 0;
            $this->createDate = '';
            $this->updateDate = '';
            $this->creator = 0;
            $this->title = '';
            $this->description = '';

            $ret = true;
        }

        return $ret;
    }

    public function toArray()
    {
        return array('id' => (is_null($this->id)?"":$this->id),
                        'createDate' => (is_null($this->createDate)?"":$this->createDate),
                        'updateDate' => (is_null($this->updateDate)?"":$this->updateDate),
                        'creator' => (is_null($this->creator)?"":$this->creator),
                        'title' => (is_null($this->title)?"":$this->title),
                        'description' => (is_null($this->description)?"":$this->description));
    }

    public function fromArray($a)
    {
        if (is_array($a) && count($a) > 0) {
            $this->id = $a['id'];
            $this->createDate = $a['createDate'];
            $this->updateDate = $a['updateDate'];
            $this->creator = $a['creator'];
            $this->title = $a['title'];
            $this->description = $a['description'];
        }
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

    public function fromJson($json)
    {
        $this->fromArray(json_decode($json, true));
    }

    public function equals($model)
    {
        return ($model->id == $this->id &&
                $model->createDate == $this->createDate &&
                $model->updateDate == $this->updateDate &&
                $model->creator == $this->creator &&
                $model->title == $this->title &&
                $model->description == $this->description);
    }

    public function equalsArray($a)
    {
        return ($a['id'] == $this->id &&
                $a['createDate'] == $this->createDate &&
                $a['updateDate'] == $this->updateDate &&
                $a['creator'] == $this->creator &&
                $a['title'] == $this->title &&
                $a['description'] == $this->description);
    }
}

?>
