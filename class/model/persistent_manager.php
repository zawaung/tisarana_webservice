<?php

define(BASE_PATH, '');

include_once(BASE_PATH . 'class/model/model_exception.php');
include_once(BASE_PATH . 'class/model/news.php');
include_once(BASE_PATH . 'class/data/news_dao.php');
include_once(BASE_PATH . 'class/model/event.php');
include_once(BASE_PATH . 'class/data/event_dao.php');
include_once(BASE_PATH . 'class/model/album.php');
include_once(BASE_PATH . 'class/data/album_dao.php');
include_once(BASE_PATH . 'class/model/photo.php');
include_once(BASE_PATH . 'class/data/photo_dao.php');
include_once(BASE_PATH . 'class/data/video_dao.php');
include_once(BASE_PATH . 'class/data/document_dao.php');
include_once(BASE_PATH . 'class/data/security/user_dao.php');
include_once(BASE_PATH . 'class/data/security/group_dao.php');
include_once(BASE_PATH . 'class/data/security/group_member_dao.php');
include_once(BASE_PATH . 'class/data/security/object_dao.php');
include_once(BASE_PATH . 'class/data/security/action_dao.php');
include_once(BASE_PATH . 'class/data/security/acl_dao.php');

class PersistentManager 
{
    protected $pdo = null;

    public function __descturct() 
    {
        $this->pdo = null;
    }

    public function getConnection()
    {
        return $this->pdo;
    }

    public function connect($dsn, $user, $password)
    {
        try {
            $this->pdo = new PDO($dsn, $user, $password);
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); 
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
        }
        catch (PDOException $e) {
            echo "Error connecting to database " . $e->getMessage();
        }
    }

//
// ADD FUNCTIONS
//
    //
    // ADD 
    //

    public function add($obj)
    {
        if (!isset($obj))
            throw new ModelException('Input should not be Null value!');

        $c = get_class($obj);

        try {
            $ans = NULL;

            $this->pdo->beginTransaction();

            switch ($c) {
                case 'News': $ans = $this->addNews($obj); break;
                case 'Event': $ans = $this->addEvent($obj); break; 
                case 'Album': $ans = $this->addAlbum($obj); break;
                case 'Photo': $ans = $this->addPhoto($obj); break;
                case 'Video': $ans = $this->addVideo($obj); break;
                case 'Document': $ans = $this->addDocument($obj); break;

            // SECURITY RELATED               
                case 'User': $ans = $this->addUser($obj); break;               
                case 'Group': $ans = $this->addGroup($obj); break;               
                case 'GroupMember': $ans = $this->addGroupMember($obj); break;               
                case 'Object': $ans = $this->addObject($obj); break;               
                case 'Action': $ans = $this->addAction($obj); break;               
                case 'Acl': $ans = $this->addAcl($obj); break;               
            }

            $this->pdo->commit();

            return $ans;
        } 
        catch (Exception $e) {
            $this->pdo->rollBack();
            throw new ModelException('Error in database layer.', 0, $e);
        }
    }

    private function addNews($obj)
    {
        $dao = new NewsDao($this->pdo);
        return $dao->insert(array('creator' => $obj->creator, 
                                    'title' => $obj->title, 
                                    'details' => $obj->details));
    }

    private function addEvent($obj)
    {
        $dao = new EventDao($this->pdo);
        return $dao->insert(array('creator' => $obj->creator, 
                                    'title' => $obj->title, 
                                    'details' => $obj->details));
    }

    private function addAlbum($obj)
    {
        $dao = new AlbumDao($this->pdo);
        return $dao->insert(array('creator' => $obj->creator, 
                                    'title' => $obj->title, 
                                    'description' => $obj->description));
    }

    private function addPhoto($obj)
    {
        $dao = new PhotoDao($this->pdo);
        return $dao->insert(array('creator' => $obj->creator,
                                    'album' => $obj->album->id,
                                    'description' => $obj->description,
                                    'file' => $obj->file));
    }

    private function addVideo($obj)
    {
        $dao = new VideoDao($this->pdo);
        return $dao->insert(array('creator' => $obj->creator, 
                                    'file' => $obj->file, 
                                    'description' => $obj->description));
    }

    private function addDocument($obj)
    {
        $dao = new DocumentDao($this->pdo);
        return $dao->insert(array('creator' => $obj->creator, 
                                    'file' => $obj->file, 
                                    'description' => $obj->description));
    }

    private function addUser($obj)
    {
        $dao = new UserDao($this->pdo);
        return $dao->insert(array('creator' => $obj->creator, 
                                    'first_name' => $obj->firstName, 
                                    'last_name' => $obj->lastName, 
                                    'email' => $obj->email, 
                                    'login_name' => $obj->loginName, 
                                    'password' => $obj->password)); 
    }

    private function addGroup($obj)
    {
        $dao = new GroupDao($this->pdo);
        return $dao->insert(array('creator' => $obj->creator, 
                                    'name' => $obj->name)); 
    }

    private function addGroupMember($obj)
    {
        $dao = new GroupMemberDao($this->pdo);
        return $dao->insert(array('creator' => $obj->creator,
                                    'group_id' => $obj->group_id, 
                                    'user_id' => $obj->user_id)); 
    }

    private function addObject($obj)
    {
        $dao = new ObjectDao($this->pdo);
        return $dao->insert(array('creator' => $obj->creator, 
                                    'name' => $obj->name)); 
    }

    private function addAction($obj)
    {
        $dao = new ActionDao($this->pdo);
        return $dao->insert(array('creator' => $obj->creator, 
                                    'name' => $obj->name, 
                                    'object' => $obj->object)); 
    }

    private function addAcl($obj)
    {
        $dao = new AclDao($this->pdo);
        return $dao->insert(array('creator' => $obj->creator,
                                    'principal' => $obj->principal, 
                                    'action' => $obj->action, 
                                    'principal_type' => $obj->principal_type)); 
    }

    //
    // ADD LIST
    //

    public function addList($arr, $type)
    {
        if (!isset($arr))
            throw new ModelException('Input array should not be Null value!');

        if (!is_array($arr))
            throw new ModelException('Input array should be type of Array!');

        foreach ($arr as $obj) {
            $t = (is_object($obj)?get_class($obj):gettype($obj));

            if ($t != $type) 
                throw new ModelException('Expected type "' . $type. '" but get "' . $t . '"!');
        }

        try {
            $ans = NULL;

            $this->pdo->beginTransaction();

            switch ($type) {
                case 'News': $ans = $this->addNewsList($arr); break;
                case 'Event': $ans = $this->addEventList($arr); break; 
                case 'Album': $ans = $this->addAlbumList($arr); break;
                case 'Photo': $ans = $this->addPhotoList($arr); break;
                case 'Video': $ans = $this->addVideoList($arr); break;
                case 'Document': $ans = $this->addDocumentList($arr); break;               
            }

            $this->pdo->commit();

            return $ans;
        } 
        catch (Exception $e) {
            $this->pdo->rollBack();
            throw new ModelException('Error in database layer.', 0, $e);
        }
    }

    private function addNewsList($arr)
    {
        $l = array();

        foreach ($arr as $news) {
            $l[] = array('creator' => $news->creator, 
                            'title' => $news->title, 
                            'details' => $news->details);
        }

        $dao = new NewsDao($this->pdo);
        return $dao->insertList($l); 
    }

    private function addEventList($arr)
    {
        $l = array();

        foreach ($arr as $event) {
            $l[] = array('creator' => $event->creator, 
                            'title' => $event->title, 
                            'details' => $event->details);
        }

        $dao = new EventDao($this->pdo);
        return $dao->insertList($l); 
    }

    private function addAlbumList($arr)
    {
        $l = array();

        foreach ($arr as $album) {
            $l[] = array('creator' => $album->creator, 
                            'title' => $album->title, 
                            'description' => $album->description);
        }

        $dao = new AlbumDao($this->pdo);
        return $dao->insertList($l); 
    }

    private function addPhotoList($arr)
    {
        $l = array();

        foreach ($arr as $photo) {
            $l[] = array('creator' => $photo->creator, 
                            'album' => $photo->album->id, 
                            'description' => $photo->description,
                            'file' => $photo->file);
        }

        $dao = new PhotoDao($this->pdo);
        return $dao->insertList($l); 
    }

    private function addVideoList($arr)
    {
        $l = array();

        foreach ($arr as $video) {
            $l[] = array('creator' => $video->creator, 
                            'file' => $video->file, 
                            'description' => $video->description);
        }

        $dao = new VideoDao($this->pdo);
        return $dao->insertList($l); 
    }

    private function addDocumentList($arr)
    {
        $l = array();

        foreach ($arr as $document) {
            $l[] = array('creator' => $document->creator, 
                            'file' => $document->file, 
                            'description' => $document->description);
        }

        $dao = new DocumentDao($this->pdo);
        return $dao->insertList($l); 
    }

//
// READ FUNCTIONS
//
    public function read($obj) 
    {
        if (!isset($obj))
            throw new ModelException('Object must be non Null value!');

        // for model with compound keys making as id such as link table
        if (property_exists($obj, 'id')) {
            if (is_null($obj->id))
                throw new ModelException('ID must be non Null value!');

            if (!is_numeric($obj->id))
                throw new ModelException('ID must be numeric!');

            if ($obj->id <= 0)
                throw new ModelException('ID must be greater than 0!');
        }

        $c = get_class($obj);

        switch ($c) {
            case 'News': return $this->readNews($obj); break;
            case 'Event': return $this->readEvent($obj); break;
            case 'Album': return $this->readAlbum($obj); break;
            case 'Photo' : return $this->readPhoto($obj); break;
            case 'Video' : return $this->readVideo($obj); break;
            case 'Document' : return $this->readDocument($obj); break;           

        // SECURITY RELATED
            case 'User' : return $this->readUser($obj); break;           
            case 'Group' : return $this->readGroup($obj); break;           
            case 'GroupMember' : return $this->readGroupMember($obj); break;           
            case 'Object' : return $this->readObject($obj); break;           
            case 'Action' : return $this->readAction($obj); break;           
            case 'Acl' : return $this->readAcl($obj); break;           
        }
    }

    private function readNews($obj) 
    {
        $ret = false;
        $dao = new NewsDao($this->pdo);
        $a = $dao->select($obj->id);
        
        if (count($a) > 0) {
            $obj->id = $a['id'];
            $obj->createDate = $a['create_date'];
            $obj->updateDate = $a['update_date'];
            $obj->creator = $a['creator'];
            $obj->title = $a['title'];
            $obj->details = $a['details'];

            $ret = true;
        } 

        return $ret;
    }

    private function readEvent($obj) 
    {
        $ret = false;
        $dao = new EventDao($this->pdo);
        $a = $dao->select($obj->id);
        
        if (count($a) > 0) {
            $obj->id = $a['id'];
            $obj->createDate = $a['create_date'];
            $obj->creator = $a['creator'];
            $obj->title = $a['title'];
            $obj->details = $a['details'];

            $ret = true;
        } 

        return $ret;
    }

    private function readAlbum($obj) 
    {
        $ret = false;
        $dao = new AlbumDao($this->pdo);
        $a = $dao->select($obj->id);
        
        if (count($a) > 0) {
            $obj->id = $a['id'];
            $obj->createDate = $a['create_date'];
            $obj->updateDate = $a['update_date'];
            $obj->creator = $a['creator'];
            $obj->title = $a['title'];
            $obj->description = $a['description'];

            $ret = true;
        } 

        return $ret;
    }

    private function readPhoto($obj) 
    {
        $ret = false;
        $dao = new PhotoDao($this->pdo);
        $a = $dao->select($obj->id);
        
        if (count($a) > 0) {
            $obj->id = $a['id'];
            $obj->createDate = $a['create_date'];
            $obj->updateDate = $a['update_date'];
            $obj->creator = $a['creator'];
            $obj->description = $a['description'];
            $obj->file = $a['file'];

            if (is_null($obj->album) || !is_object($obj->album) || get_class($obj->album) != 'Album') {
                $album = new Album($this);
                $album->id = $a['album'];
                $this->readAlbum($album);
                $obj->album = $album;
            }

            $ret = true;
        } 

        return $ret;
    }

    private function readVideo($obj) 
    {
        $ret = false;
        $dao = new VideoDao($this->pdo);
        $a = $dao->select($obj->id);
        
        if (count($a) > 0) {
            $obj->id = $a['id'];
            $obj->createDate = $a['create_date'];
            $obj->updateDate = $a['update_date'];
            $obj->creator = $a['creator'];
            $obj->file = $a['file'];
            $obj->description = $a['description'];

            $ret = true;
        } 

        return $ret;
    }

    private function readDocument($obj) 
    {
        $ret = false;
        $dao = new DocumentDao($this->pdo);
        $a = $dao->select($obj->id);
        
        if (count($a) > 0) {
            $obj->id = $a['id'];
            $obj->createDate = $a['create_date'];
            $obj->updateDate = $a['update_date'];
            $obj->creator = $a['creator'];
            $obj->file = $a['file'];
            $obj->description = $a['description'];

            $ret = true;
        } 

        return $ret;
    }

    private function readUser($obj) 
    {
        $ret = false;
        $dao = new UserDao($this->pdo);
        $a = $dao->select($obj->id);
        
        if (count($a) > 0) {
            $obj->id = $a['id'];
            $obj->createDate = $a['create_date'];
            $obj->updateDate = $a['update_date'];
            $obj->creator = $a['creator'];
            $obj->firstName = $a['first_name'];
            $obj->lastName = $a['last_name'];
            $obj->email = $a['email'];
            $obj->loginName = $a['login_name'];
            $obj->password = $a['password'];
            $obj->suspended = $a['suspended'];
            $obj->suspendedDate = $a['suspended_date'];

            $ret = true;
        } 

        return $ret;
    }

    private function readGroup($obj) 
    {
        $ret = false;
        $dao = new GroupDao($this->pdo);
        $a = $dao->select($obj->id);
        
        if (count($a) > 0) {
            $obj->id = $a['id'];
            $obj->createDate = $a['create_date'];
            $obj->updateDate = $a['update_date'];
            $obj->creator = $a['creator'];
            $obj->name = $a['name'];

            $ret = true;
        } 

        return $ret;
    }

    private function readGroupMember($obj) 
    {
        $ret = false;
        $dao = new GroupMemberDao($this->pdo);
        $a = $dao->select(array('group_id' => $obj->group_id, 'user_id' => $obj->user_id));
        
        if (count($a) > 0) {
            $obj->createDate = $a['create_date'];
            $obj->creator = $a['creator'];
            $obj->group_id = $a['group_id'];
            $obj->user_id = $a['user_id'];

            $ret = true;
        } 

        return $ret;
    } 
 
    private function readObject($obj) 
    {
        $ret = false;
        $dao = new ObjectDao($this->pdo);
        $a = $dao->select($obj->id);
        
        if (count($a) > 0) {
            $obj->id = $a['id'];
            $obj->createDate = $a['create_date'];
            $obj->updateDate = $a['update_date'];
            $obj->creator = $a['creator'];
            $obj->name = $a['name'];

            $ret = true;
        } 

        return $ret;
    }

    private function readAction($obj) 
    {
        $ret = false;
        $dao = new ActionDao($this->pdo);
        $a = $dao->select($obj->id);
        
        if (count($a) > 0) {
            $obj->id = $a['id'];
            $obj->createDate = $a['create_date'];
            $obj->updateDate = $a['update_date'];
            $obj->creator = $a['creator'];
            $obj->name = $a['name'];
            $obj->object = $a['object'];

            $ret = true;
        } 

        return $ret;
    }

    private function readAcl($obj) 
    {
        $ret = false;
        $dao = new AclDao($this->pdo);
        $a = $dao->select(array('principal' => $obj->principal, 
                                'action' => $obj->action, 
                                'principal_type' => $obj->principal_type));
        
        if (count($a) > 0) {
            $obj->createDate = $a['create_date'];
            $obj->creator = $a['creator'];
            $obj->principal = $a['principal'];
            $obj->action = $a['action'];
            $obj->principal_type = $a['principal_type'];

            $ret = true;
        } 

        return $ret;
    } 

//
// DELETE FUNCTIONS
//
    public function delete($obj)
    {
        if (!isset($obj))
            throw new ModelException('Object must be non Null value!');

        // for model with compound keys making as id such as link table
        if (property_exists($obj, 'id')) {
            if (is_null($obj->id))
                throw new ModelException('ID must be non Null value!');

            if (!is_numeric($obj->id))
                throw new ModelException('ID must be numeric value!');

            if ($obj->id <= 0)
                throw new ModelException('ID must be greater than zero!');
        }

        $c = get_class($obj);

        try {
            $ans = NULL;

            $this->pdo->beginTransaction();

            switch ($c) {
                case 'News': $ans = $this->deleteNews($obj); break;
                case 'Event': $ans = $this->deleteEvent($obj); break;
                case 'Album': $ans = $this->deleteAlbum($obj); break;
                case 'Photo': $ans = $this->deletePhoto($obj); break;
                case 'Video': $ans = $this->deleteVideo($obj); break;
                case 'Document': $ans = $this->deleteDocument($obj); break;               

            // SECURITY RELATED
                case 'User': $ans = $this->deleteUser($obj); break;               
                case 'Group': $ans = $this->deleteGroup($obj); break;               
                case 'GroupMember': $ans = $this->deleteGroupMember($obj); break;               
                case 'Object': $ans = $this->deleteObject($obj); break;               
                case 'Action': $ans = $this->deleteAction($obj); break;               
                case 'Acl': $ans = $this->deleteAcl($obj); break;               
            }

            $this->pdo->commit();

            return $ans;
        } 
        catch (Exception $e) {
            $this->pdo->rollBack();
            throw new ModelException('Error in database layer.', 0, $e);
        }
    }

    private function deleteNews($obj)
    {
        $dao = new NewsDao($this->pdo);
        return $dao->delete($obj->id);
    }

    private function deleteEvent($obj)
    {
        $dao = new EventDao($this->pdo);
        return $dao->delete($obj->id);
    }

    private function deleteAlbum($obj)
    {
        $dao = new AlbumDao($this->pdo);
        return $dao->delete($obj->id);
    }
 
    private function deletePhoto($obj)
    {
        $dao = new PhotoDao($this->pdo);
        return $dao->delete($obj->id);
    }
 
    private function deleteVideo($obj)
    {
        $dao = new VideoDao($this->pdo);
        return $dao->delete($obj->id);
    }

    private function deleteDocument($obj)
    {
        $dao = new DocumentDao($this->pdo);
        return $dao->delete($obj->id);
    }

    private function deleteUser($obj)
    {
        $dao = new UserDao($this->pdo);
        return $dao->delete($obj->id);
    }

    private function deleteGroup($obj)
    {
        $dao = new GroupDao($this->pdo);
        return $dao->delete($obj->id);
    }

    private function deleteGroupMember($obj)
    {
        $dao = new GroupMemberDao($this->pdo);
        return $dao->delete(array('group_id' => $obj->group_id, 'user_id' => $obj->user_id));
    } 
 
    private function deleteObject($obj)
    {
        $dao = new ObjectDao($this->pdo);
        return $dao->delete($obj->id);
    }

    private function deleteAction($obj)
    {
        $dao = new ActionDao($this->pdo);
        return $dao->delete($obj->id);
    }

    private function deleteAcl($obj)
    {
        $dao = new AclDao($this->pdo);
        return $dao->delete(array('principal' => $obj->principal, 
                                    'action' => $obj->action, 
                                    'principal_type' => $obj->principal_type));
    } 
//
// UPDATE FUNCTIONS
//
    public function update($obj) 
    {
        if (!isset($obj))
            throw new ModelException('Object must be non Null value!');

        // for model with compound keys making as id such as link table
        if (property_exists($obj, 'id')) {
            if (is_null($obj->id))
                throw new ModelException('ID must be non Null value!');

            if (!is_numeric($obj->id))
                throw new ModelException('ID must be numeric!');

            if ($obj->id <= 0)
                throw new ModelException('ID must be greater than 0!');
        }

        $c = get_class($obj);

        try {
            $ans = NULL;

            $this->pdo->beginTransaction();

            switch ($c) {
                case 'News': $ans = $this->updateNews($obj); break;
                case 'Event': $ans = $this->updateEvent($obj); break;
                case 'Album': $ans = $this->updateAlbum($obj); break;
                case 'Photo': $ans = $this->updatePhoto($obj); break;
                case 'Video': $ans = $this->updateVideo($obj); break;
                case 'Document': $ans = $this->updateDocument($obj); break;               

            // SECURITY RELATED
                case 'User': $ans = $this->updateUser($obj); break;               
                case 'Group': $ans = $this->updateGroup($obj); break;               
                case 'Object': $ans = $this->updateObject($obj); break;               
                case 'Action': $ans = $this->updateAction($obj); break;               
            }

            $this->pdo->commit();

            return $ans;
        } 
        catch (Exception $e) {
            $this->pdo->rollBack();
            throw new ModelException('Error in database layer.', 0, $e);
        }
    }

    private function updateNews($obj)
    {
        $dao = new NewsDao($this->pdo);
        return $dao->update($obj->id, array('creator' => $obj->creator, 
                                            'title' => $obj->title, 
                                            'details' => $obj->details));
    }

    private function updateEvent($obj)
    {
        $dao = new EventDao($this->pdo);
        return $dao->update($obj->id, array('creator' => $obj->creator, 
                                            'title' => $obj->title, 
                                            'details' => $obj->details));
    }

    private function updateAlbum($obj)
    {
        $dao = new AlbumDao($this->pdo);
        return $dao->update($obj->id, array('creator' => $obj->creator, 
                                            'title' => $obj->title, 
                                            'description' => $obj->description));
    }

    private function updatePhoto($obj)
    {
        $dao = new PhotoDao($this->pdo);
        return $dao->update($obj->id, array('creator' => $obj->creator, 
                                            'album' => $obj->album->id, 
                                            'description' => $obj->description,
                                            'file' => $obj->file));
    }

    private function updateVideo($obj)
    {
        $dao = new VideoDao($this->pdo);
        return $dao->update($obj->id, array('creator' => $obj->creator, 
                                            'file' => $obj->file, 
                                            'description' => $obj->description));
    }

    private function updateDocument($obj)
    {
        $dao = new DocumentDao($this->pdo);
        return $dao->update($obj->id, array('creator' => $obj->creator, 
                                            'file' => $obj->file, 
                                            'description' => $obj->description));
    }

    private function updateUser($obj)
    {
        $dao = new UserDao($this->pdo);
        return $dao->update($obj->id, array('creator' => $obj->creator, 
                                            'first_name' => $obj->firstName, 
                                            'last_name' => $obj->lastName, 
                                            'email' => $obj->email, 
                                            'login_name' => $obj->loginName, 
                                            'password' => $obj->password,
                                            'suspended' => $obj->suspended,
                                            'suspended_date' => $obj->suspendedDate)); 
    }

    private function updateGroup($obj)
    {
        $dao = new GroupDao($this->pdo);
        return $dao->update($obj->id, array('creator' => $obj->creator, 
                                            'name' => $obj->name)); 
    }

    private function updateObject($obj)
    {
        $dao = new ObjectDao($this->pdo);
        return $dao->update($obj->id, array('creator' => $obj->creator, 
                                            'name' => $obj->name)); 
    }

    private function updateAction($obj)
    {
        $dao = new ActionDao($this->pdo);
        return $dao->update($obj->id, array('creator' => $obj->creator, 
                                            'name' => $obj->name,
                                            'object' => $obj->object));
    }

//
// COUNT FUNCTIONS
//
    public function getCount($type)
    {
         if (!isset($type))
            throw new ModelException('Input must be non Null value!');
  
        switch ($type) {
            case 'News': return $this->getNewsCount(); break;
            case 'Event': return $this->getEventCount(); break;
            case 'Album': return $this->getAlbumCount(); break;
            case 'Photo': return $this->getPhotoCount(); break;
            case 'Video': return $this->getVideoCount(); break;           
            case 'Document': return $this->getDocumentCount(); break;           
        }
    }

    private function getNewsCount()
    {
        $dao = new NewsDao($this->pdo);
        return $dao->count();
    }

    private function getEventCount()
    {
        $dao = new EventDao($this->pdo);
        return $dao->count();
    }

    private function getAlbumCount()
    {
        $dao = new AlbumDao($this->pdo);
        return $dao->count();
    }

    private function getPhotoCount()
    {
        $dao = new PhotoDao($this->pdo);
        return $dao->count();
    }

    private function getVideoCount()
    {
        $dao = new VideoDao($this->pdo);
        return $dao->count();
    }

    private function getDocumentCount()
    {
        $dao = new DocumentDao($this->pdo);
        return $dao->count();
    }

// 
// LIST FUNCTIONS
//
    public function getList($page, $pageSize, $type)
    {
        if (!isset($page) || !is_numeric($page) || $page < 0) 
            throw new ModelException('Invalid page. Page should be numeric and not less than zero!');

        if (!isset($pageSize) || !is_numeric($pageSize) || $pageSize < 0) 
            throw new ModelException('Invalid page size. Page size should be numeric and not less than zero!');

        switch ($type) {
            case 'News': return $this->listNews($page, $pageSize); break;
            case 'Event': return $this->listEvent($page, $pageSize); break;
            case 'Album': return $this->listAlbum($page, $pageSize); break;
            case 'Photo': return $this->listPhoto($page, $pageSize); break;
            case 'Video': return $this->listVideo($page, $pageSize); break;            
            case 'Document': return $this->listDocument($page, $pageSize); break;           
        }
    }

    private function listNews($page, $pageSize)
    {
        $dao = new NewsDao($this->pdo);
        $list = $dao->selectList($page, $pageSize);
        $l = array();

        foreach ($list as $item) {
            $news = new News($this);
            $news->fromArray(array('id' => $item['id'], 
                                    'createDate' => $item['create_date'],
                                    'updateDate' => $item['update_date'],
                                    'creator' => $item['creator'],
                                    'title' => $item['title'],
                                    'details' => $item['details'],
                                    'publishDate' => $item['publish_date']));
            $l[] = $news;
            $news = NULL;
        }

        return $l;
    }

    private function listEvent($page, $pageSize)
    {
        $dao = new EventDao($this->pdo);
        $list = $dao->selectList($page, $pageSize);
        $l = array();

        foreach ($list as $item) {
            $event = new Event($this);
            $event->fromArray(array('id' => $item['id'], 
                                    'createDate' => $item['create_date'],
                                    'creator' => $item['creator'],
                                    'title' => $item['title'],
                                    'details' => $item['details']));
            $l[] = $event;
            $event = NULL;
        }

        return $l;
    }

    private function listAlbum($page, $pageSize)
    {
        $dao = new AlbumDao($this->pdo);
        $list = $dao->selectList($page, $pageSize);
        $l = array();

        foreach ($list as $item) {
            $album = new Album($this);
            $album->fromArray(array('id' => $item['id'], 
                                    'createDate' => $item['create_date'],
                                    'updateDate' => $item['update_date'],
                                    'creator' => $item['creator'],
                                    'title' => $item['title'],
                                    'description' => $item['description']));
            $l[] = $album;
            $album = NULL;
        }

        return $l;
    }

    private function listPhoto($page, $pageSize)
    {
        $dao = new PhotoDao($this->pdo);
        $list = $dao->selectList($page, $pageSize);
        $l = array();

        foreach ($list as $item) {
            $photo = new Photo($this);
            $photo->id = $item['id'];
            $photo->createDate = $item['create_date'];
            $photo->updateDate = $item['update_date'];
            $photo->creator = $item['creator'];
            $photo->description = $item['description'];
            $photo->file = $item['file'];

            if (is_null($photo->album) || !is_photoect($photo->album) || get_class($photo->album) != 'Album') {
                $itemlbum = new Album($this);
                $itemlbum->id = $item['album'];
                $this->readAlbum($itemlbum);
                $photo->album = $itemlbum;
                $itemlbum = NULL;
            }
            $l[] = $photo;
            $photo = NULL;
        }

        return $l;
    }

    private function listVideo($page, $pageSize)
    {
        $dao = new VideoDao($this->pdo);
        $list = $dao->selectList($page, $pageSize);
        $l = array();

        foreach ($list as $item) {
            $video = new Video($this);
            $video->fromArray(array('id' => $item['id'], 
                                    'createDate' => $item['create_date'],
                                    'updateDate' => $item['update_date'],
                                    'creator' => $item['creator'],
                                    'file' => $item['file'],
                                    'description' => $item['description']));
            $l[] = $video;
            $video = NULL;
        }

        return $l;
    }

    private function listDocument($page, $pageSize)
    {
        $dao = new DocumentDao($this->pdo);
        $list = $dao->selectList($page, $pageSize);
        $l = array();

        foreach ($list as $item) {
            $document = new Document($this);
            $document->fromArray(array('id' => $item['id'], 
                                    'createDate' => $item['create_date'],
                                    'updateDate' => $item['update_date'],
                                    'creator' => $item['creator'],
                                    'file' => $item['file'],
                                    'description' => $item['description']));
            $l[] = $document;
            $document = NULL;
        }

        return $l;
    }

}

?>
