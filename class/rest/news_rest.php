<?php

include_once(BASE_PATH . 'conf.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/news.php');
include_once(BASE_PATH . 'class/model/news_list.php');
include_once(BASE_PATH . 'class/rest/rest.php');

class NewsRest extends Rest 
{
    private $pm = null;

    public function __construct()
    {
        parent::__construct();

        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

    public function add()
    {
        $creator = filter_var($this->params->creator, FILTER_VALIDATE_INT);
        $title = filter_var($this->params->title, FILTER_SANITIZE_STRING);
        $details = filter_var($this->params->details, FILTER_SANITIZE_STRING);

        try {
            $news = new News($this->pm);
            $news->creator = $creator;
            $news->title = $title;
            $news->details = $details;

            if ($news->save()) {
                $n = new News($this->pm);
                $n->id = $news->id;
                $n->read(); 

                $this->created($n->id);
                $this->result = $n->toJson();
            } 
            else {
                $s = NULL;

                if ($news->hasError()) {
                    $errors = $news->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function read()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        
        try {
            $news = new News($this->pm);
            $news->id = $id;

            if ($news->read()) { 
                $this->ok();
                $this->result = $news->toJson();
            }
            else {
                $s = NULL;

                if ($news->hasError()) {
                    $errors = $news->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function readList() 
    {
        $page = filter_var($this->params->page, FILTER_VALIDATE_INT);
        $pageSize = filter_var($this->params->pageSize, FILTER_VALIDATE_INT);
        
        try {
            $nl = new NewsList($this->pm);
            if ($page > 0)
                $page -= 1;

            $nl->setPageSize($pageSize);
            $nl->setCurrentPage($page);
            
            $list = $nl->getList();
            $a = array();
            foreach ($list as $news) {
                $a[] = $news->toJson();      
            }

            $this->ok();
            $this->result = '{' .
                                '"page": ' . $nl->getCurrentPage() . ', ' .
                                '"totalPage": ' . $nl->getTotalPage() . ', ' .
                                '"result": [' . implode(',', $a) . ']' . 
                            '}';
            $a = NULL;
        }
        catch (Exception $e) {
            $this->error(e);
        }
    }

    public function update()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        $title = filter_var($this->params->title, FILTER_SANITIZE_STRING);
        $details = filter_var($this->params->details, FILTER_SANITIZE_STRING);

        try {
            $news = new News($this->pm);
            $news->id = $id;
            $news->read();

            $news->title = $title;
            $news->details = $details;

            if ($news->save()) {
                $n = new News($this->pm);
                $n->id = $news->id;
                $n->read(); 

                $this->ok();
                $this->result = $n->toJson();
            } 
            else {
                $s = NULL;

                if ($news->hasError()) {
                    $errors = $news->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function remove()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        
        try {
            $news = new News($this->pm);
            $news->id = $id;

            if ($news->delete()) {
                $this->code = Rest::RC_NOCONTENT;
            }
            else {
                $s = NULL;

                if ($news->hasError()) {
                    $errors = $news->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->stackTrace = true;
            $this->error($e);
        }
    }
}

?>
