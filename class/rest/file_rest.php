<?php

define('BASE_PATH', '../../');

include_once(BASE_PATH . 'conf.php');
include_once(BASE_PATH . 'class/rest/rest.php');
include_once(BASE_PATH . 'class/util/file_storage_manager.php');

class FileRest extends Rest 
{
    public function add() 
    { 
        $this->code = Rest::RC_NOT_IMPLEMENTED; 
    }

    public function read()
    {
        $hash = filter_var($this->params->hash, FILTER_SANITIZE_STRING);

        try {
            $file = FileStorageManager::processOutbound($hash);
            if ($file) {
                $this->toSendFile = true;
                $this->fileToSend = $file;
            }
            else {
                $this->code = Rest::RC_NOT_FOUND;
                $this->result = '{ "message" : "File not found in storage ' . 
                                    'for hash (' . $hash. ')" }';
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function readList() 
    { 
        $this->code = Rest::RC_NOT_IMPLEMENTED; 
    }

    public function update()
    {
        $putfile = filter_var($this->params->putfile, FILTER_SANITIZE_STRING);

        try {
            $hash = FileStorageManager::processInbound($putfile);
            if ($hash) {
                $this->code = Rest::RC_CREATED;
                $this->result = '{"hash" : "' . $hash . '"}';
            }
            else {
                $this->code = Rest::RC_CONFLICT;
                $this->result = '{"hash" : "File exists in the storage."}';
            }

            unlink($putfile);
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }
    
    public function remove() 
    { 
        $this->code = Rest::RC_NOT_IMPLEMENTED; 
    }
}

?> 
