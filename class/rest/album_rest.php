<?php

define('BASE_PATH', '../../');

include_once(BASE_PATH . 'conf.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/album.php');
include_once(BASE_PATH . 'class/model/album_list.php');
include_once(BASE_PATH . 'class/rest/rest.php');

class AlbumRest extends Rest 
{
    private $pm = null;

    public function __construct()
    {
        parent::__construct();

        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

    public function add()
    {
        $creator = filter_var($this->params->creator, FILTER_VALIDATE_INT);
        $title = filter_var($this->params->title, FILTER_SANITIZE_STRING);
        $description = filter_var($this->params->description, FILTER_SANITIZE_STRING);

        try {
            $album = new Album($this->pm);
            $album->creator = $creator;
            $album->title = $title;
            $album->description = $description;

            if ($album->save()) {
                $n = new Album($this->pm);
                $n->id = $album->id;
                $n->read(); 

                $this->created($n->id);
                $this->result = $n->toJson();
            } 
            else {
                $s = NULL;

                if ($album->hasError()) {
                    $errors = $album->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function read()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        
        try {
            $album = new Album($this->pm);
            $album->id = $id;

            if ($album->read()) { 
                $this->ok();
                $this->result = $album->toJson();
            }
            else {
                $s = NULL;

                if ($album->hasError()) {
                    $errors = $album->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function readList() 
    {
        $page = filter_var($this->params->page, FILTER_VALIDATE_INT);
        $pageSize = filter_var($this->params->pageSize, FILTER_VALIDATE_INT);
        
        try {
            $nl = new AlbumList($this->pm);
            if ($page > 0)
                $page -= 1;

            $nl->setPageSize($pageSize);
            $nl->setCurrentPage($page);
            
            $list = $nl->getList();
            $a = array();
            foreach ($list as $album) {
                $a[] = $album->toJson();      
            }

            $this->ok();
            $this->result = '{' .
                                '"page": ' . $nl->getCurrentPage() . ', ' .
                                '"totalPage": ' . $nl->getTotalPage() . ', ' .
                                '"result": [' . implode(',', $a) . ']' . 
                            '}';
            $a = NULL;
        }
        catch (Exception $e) {
            $this->error(e);
        }
    }

    public function update()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        $title = filter_var($this->params->title, FILTER_SANITIZE_STRING);
        $description = filter_var($this->params->description, FILTER_SANITIZE_STRING);

        try {
            $album = new Album($this->pm);
            $album->id = $id;
            $album->read();
            
            $album->title = $title;
            $album->description = $description;

            if ($album->save()) {
                $n = new Album($this->pm);
                $n->id = $album->id;
                $n->read(); 

                $this->ok();
                $this->result = $n->toJson();
            } 
            else {
                $s = NULL;

                if ($album->hasError()) {
                    $errors = $album->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function remove()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        
        try {
            $n = new Album($this->pm);        
            $n->id = $id;
            $n->read();

            $album = new Album($this->pm);
            $album->id = $id;

            if ($album->delete()) {
                $this->ok();
                $this->result = $n->toJson();
            }
            else {
                $s = NULL;

                if ($album->hasError()) {
                    $errors = $album->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->stackTrace = true;
            $this->error($e);
        }
    }
}

?>
