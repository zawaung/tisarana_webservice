<?php

define('BASE_PATH', '../../');

include_once(BASE_PATH . 'conf.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/document.php');
include_once(BASE_PATH . 'class/model/document_list.php');
include_once(BASE_PATH . 'class/rest/rest.php');

class DocumentRest extends Rest 
{
    private $pm = null;

    public function __construct()
    {
        parent::__construct();

        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

    public function add()
    {
        $creator = filter_var($this->params->creator, FILTER_VALIDATE_INT);
        $file = filter_var($this->params->file, FILTER_SANITIZE_STRING);
        $description = filter_var($this->params->description, FILTER_SANITIZE_STRING);

        try {
            $document = new Document($this->pm);
            $document->creator = $creator;
            $document->file = $file;
            $document->description = $description;

            if ($document->save()) {
                $n = new Document($this->pm);
                $n->id = $document->id;
                $n->read(); 

                $this->created($n->id);
                $this->result = $n->toJson();
            } 
            else {
                $s = NULL;

                if ($document->hasError()) {
                    $errors = $document->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function read()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        
        try {
            $document = new Document($this->pm);
            $document->id = $id;

            if ($document->read()) { 
                $this->ok();
                $this->result = $document->toJson();
            }
            else {
                $s = NULL;

                if ($document->hasError()) {
                    $errors = $document->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function readList() 
    {
        $page = filter_var($this->params->page, FILTER_VALIDATE_INT);
        $pageSize = filter_var($this->params->pageSize, FILTER_VALIDATE_INT);
        
        try {
            $nl = new DocumentList($this->pm);
            if ($page > 0)
                $page -= 1;

            $nl->setPageSize($pageSize);
            $nl->setCurrentPage($page);
            
            $list = $nl->getList();
            $a = array();
            foreach ($list as $document) {
                $a[] = $document->toJson();      
            }

            $this->ok();
            $this->result = '{' .
                                '"page": ' . $nl->getCurrentPage() . ', ' .
                                '"totalPage": ' . $nl->getTotalPage() . ', ' .
                                '"result": [' . implode(',', $a) . ']' . 
                            '}';
            $a = NULL;
        }
        catch (Exception $e) {
            $this->error(e);
        }
    }

    public function update()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        $file = filter_var($this->params->file, FILTER_SANITIZE_STRING);
        $description = filter_var($this->params->description, FILTER_SANITIZE_STRING);

        try {
            $document = new Document($this->pm);
            $document->id = $id;
            $document->read();

            $document->file = $file;
            $document->description = $description;

            if ($document->save()) {
                $n = new Document($this->pm);
                $n->id = $document->id;
                $n->read(); 

                $this->ok();
                $this->result = $n->toJson();
            } 
            else {
                $s = NULL;

                if ($document->hasError()) {
                    $errors = $document->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function remove()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        
        try {
            $n = new Document($this->pm);        
            $n->id = $id;
            $n->read();

            $document = new Document($this->pm);
            $document->id = $id;

            if ($document->delete()) {
                $this->ok();
                $this->result = $n->toJson();
            }
            else {
                $s = NULL;

                if ($document->hasError()) {
                    $errors = $document->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->stackTrace = true;
            $this->error($e);
        }
    }
}

?>
