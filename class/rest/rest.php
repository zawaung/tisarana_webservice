<?php

include_once('params.php');

abstract class Rest
{
    // HTTP RESPONSE CODE
    const RC_OK                 = 200;
    const RC_CREATED            = 201;
    const RC_NOCONTENT          = 204;
    const RC_AUTH_FAILED        = 401;
    const RC_NOT_FOUND          = 404;
    const RC_CONFLICT           = 409;
    const RC_ERROR              = 500;
    const RC_NOT_IMPLEMENTED    = 501;

    // CONTENT TYPE
    const CT_JSON = 'Content-Type : application/json';
    const CT_GZIP = 'Content-Type : application/gzip';

    protected $params = null;
    protected $code = self::RC_OK;
    protected $result = '{}';
    protected $createdReouceId = 0;
    protected $stackTrace = false;
    protected $contentType = self::CT_JSON;
    protected $toSendFile = false;
    protected $fileToSend = '';

    public function __construct()
    {
        $this->params = new Params();
    }

    public function run()
    {
        /*
        ob_start();
        var_dump($this->params);
        $this->result = ob_get_contents();
        ob_end_clean();
        */

        switch($_SERVER['REQUEST_METHOD']) {
            case 'GET': $this->handleGet(); break; 
            case 'POST': $this->add(); break;
            case 'PUT': $this->update(); break;
            case 'DELETE': $this->remove(); break;
        }
   
        http_response_code($this->code);
        header($contentType);

        if ($this->toSendFile) {
            $this->sendFile();
        }
        else {
            echo $this->result;
        }
    }
    
    protected function handleGet()
    {
        if ($this->params->id || $this->params->hash) 
            $this->read();
        else
            $this->readList();
    }

    protected function ok()
    {
        $this->code = self::RC_OK;
    }

    protected function error($e)
    {
        $this->code = self::RC_ERROR;


        if (isset($e) && is_object($e) && is_subclass_of('Exception')) {
            $st = '';

            if ($this->stackTrace) {
                ob_start();
                var_dump($e->getTrace());
                $st = ob_get_contents();
                ob_end_clean();
            }
            
            $this->result = '{"error": 1, "message": "Error processing your request!", ' . 
                                '"debug": "' . $e->getMessage() . '", ' . 
                                '"trace": "' . $st . '"}';  
        } 
        elseif (isset($e)) {
            $this->result = '{"error": "YES", "debug": "' . $e . '"}';
        } 
        else {
            $this->result = '{"error": "YES"}';
        }
    }

    protected function created($id)
    {
        $this->code = self::RC_CREATED;
        $this->createdResourceId = $id;
    }

    protected function sendFile($file = false)
    {
        if(!$file) {
            $file = $this->fileToSend;
        }

        if(file_exists($file)) {
            readfile($file);
        }
    }

    abstract public function add();
    abstract public function read();
    abstract public function readList();
    abstract public function update();
    abstract public function remove();    
}

?>
