<?php

define('BASE_PATH', '../../');

include_once(BASE_PATH . 'conf.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/photo.php');
include_once(BASE_PATH . 'class/model/photo_list.php');
include_once(BASE_PATH . 'class/rest/rest.php');
include_once(BASE_PATH . 'class/model/album.php');

class PhotoRest extends Rest 
{
    private $pm = null;

    public function __construct()
    {
        parent::__construct();

        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

    public function add()
    {
        $creator = filter_var($this->params->creator, FILTER_VALIDATE_INT);
        $album = filter_var($this->params->album, FILTER_VALIDATE_INT);
        $description = filter_var($this->params->description, FILTER_SANITIZE_STRING);
        $file = filter_var($this->params->file, FILTER_SANITIZE_STRING);

        try {
            $a = new Album($this->pm);
            $a->id = $album;
            $a->read();

            $photo = new Photo($this->pm);
            $photo->creator = $creator;
            $photo->album = $a;
            $photo->description = $description;
            $photo->file = $file;

            if ($photo->save()) {
                $n = new Photo($this->pm);
                $n->id = $photo->id;
                $n->read(); 

                $this->created($n->id);
                $this->result = $n->toJson();
            } 
            else {
                $s = NULL;

                if ($photo->hasError()) {
                    $errors = $photo->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function read()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        
        try {
            $photo = new Photo($this->pm);
            $photo->id = $id;

            if ($photo->read()) { 
                $this->ok();
                $this->result = $photo->toJson();
            }
            else {
                $s = NULL;

                if ($photo->hasError()) {
                    $errors = $photo->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function readList() 
    {
        $page = filter_var($this->params->page, FILTER_VALIDATE_INT);
        $pageSize = filter_var($this->params->pageSize, FILTER_VALIDATE_INT);
        
        try {
            $nl = new PhotoList($this->pm);
            if ($page > 0)
                $page -= 1;

            $nl->setPageSize($pageSize);
            $nl->setCurrentPage($page);
            
            $list = $nl->getList();
            $a = array();
            foreach ($list as $photo) {
                $a[] = $photo->toJson();      
            }

            $this->ok();
            $this->result = '{' .
                                '"page": ' . $nl->getCurrentPage() . ', ' .
                                '"totalPage": ' . $nl->getTotalPage() . ', ' .
                                '"result": [' . implode(',', $a) . ']' . 
                            '}';
            $a = NULL;
        }
        catch (Exception $e) {
            $this->error(e);
        }
    }

    public function update()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        $album = filter_var($this->params->album, FILTER_VALIDATE_INT);
        $description = filter_var($this->params->description, FILTER_SANITIZE_STRING);
        $file = filter_var($this->params->file, FILTER_SANITIZE_STRING);

        try {
            $a = new Album($this->pm);
            $a->id = $album;
            $a->read();
           
            $photo = new Photo($this->pm);
            $photo->id = $id;
            $photo->read();

            $photo->album = $a;
            $photo->description = $description;
            $photo->file = $file;

            if ($photo->save()) {
                $n = new Photo($this->pm);
                $n->id = $photo->id;
                $n->read(); 

                $this->ok();
                $this->result = $n->toJson();
            } 
            else {
                $s = NULL;

                if ($photo->hasError()) {
                    $errors = $photo->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function remove()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        
        try {
            $n = new Photo($this->pm);        
            $n->id = $id;
            $n->read();

            $photo = new Photo($this->pm);
            $photo->id = $id;

            if ($photo->delete()) {
                $this->ok();
                $this->result = $n->toJson();
            }
            else {
                $s = NULL;

                if ($photo->hasError()) {
                    $errors = $photo->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->stackTrace = true;
            $this->error($e);
        }
    }
}

?>
