<?php

/**
 *
 * This is the request parameter processing php class by user 
 * Bryan Drewery (http://stackoverflow.com/users/285734/bryan-drewery) 
 * mentioned in this post (http://stackoverflow.com/a/5932067) on stackoverflow.
 *
 */
class Params {
    private $params = Array();

    public function __construct() 
    {
        $this->_parseParams();
    }

    /**
     * @brief Lookup request params
     * @param string $name Name of the argument to lookup
     * @returns The value from the GET/POST/PUT/DELETE value, or NULL if not set
     */
    public function __get($name) 
    {
        if (isset($this->params[$name])) 
            return $this->params[$name];
        else 
            return NULL;
    }

    private function _parseParams() 
    {
        $method = $_SERVER['REQUEST_METHOD'];

        if ($method == 'PUT') {
            $type = $_SERVER['HTTP_CONTENT_TYPE'];
            if (empty($type)) {
                $type = $_SERVER['CONTENT_TYPE'];
            }

            if ('multipart/form-data' == $type || 'x-www-form-urlencoded' == $type) {
                parse_str(file_get_contents('php://input'), $this->params);
            }
            else {
                $in = fopen('php://input', 'r');
                $tmpfile = tempnam(sys_get_temp_dir(), 'wsfile');
                $out = fopen($tmpfile, 'w');

                if ($in && $out) {
                    while ($data = fread($in, 8192)) {
                        fwrite($out, $data);
                    }
                    fclose($in);
                    fclose($out);
                }

                $this->params['putfile'] = $tmpfile;
            }

            $GLOBALS["_{$method}"] = $this->params;
            // Add these request vars into _REQUEST, mimicing default behavior 
            // PUT will override existing COOKIE/GET vars
            $_REQUEST = $this->params + $_REQUEST;
        } 
        else if ($method == 'DELETE') {
            parse_str(file_get_contents('php://input'), $this->params);
            $GLOBALS["_{$method}"] = $this->params;
            // Add these request vars into _REQUEST, mimicing default behavior, 
            // DELETE will override existing COOKIE/GET vars
            $_REQUEST = $this->params + $_REQUEST;
        } 
        else if ($method == 'GET') {
            $this->params = $_GET;
        } 
        else if ($method == 'POST') {
            $this->params = $_POST;
        }
    }
}

?>
