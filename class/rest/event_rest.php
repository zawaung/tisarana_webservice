<?php

include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/event.php');
include_once(BASE_PATH . 'class/model/event_list.php');
include_once(BASE_PATH . 'class/rest/rest.php');

class EventRest extends Rest 
{
    private $pm = null;

    public function __construct()
    {
        parent::__construct();

        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

    public function add()
    {
        $creator = filter_var($this->params->creator, FILTER_VALIDATE_INT);
        $title = filter_var($this->params->title, FILTER_SANITIZE_STRING);
        $details = filter_var($this->params->details, FILTER_SANITIZE_STRING);

        try {
            $event = new Event($this->pm);
            $event->creator = $creator;
            $event->title = $title;
            $event->details = $details;

            if ($event->save()) {
                $n = new Event($this->pm);
                $n->id = $event->id;
                $n->read(); 

                $this->created($n->id);
                $this->result = $n->toJson();
            } 
            else {
                $s = NULL;

                if ($event->hasError()) {
                    $errors = $event->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function read()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        
        try {
            $event = new Event($this->pm);
            $event->id = $id;

            if ($event->read()) { 
                $this->ok();
                $this->result = $event->toJson();
            }
            else {
                $s = NULL;

                if ($event->hasError()) {
                    $errors = $event->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function readList() 
    {
        $page = filter_var($this->params->page, FILTER_VALIDATE_INT);
        $pageSize = filter_var($this->params->pageSize, FILTER_VALIDATE_INT);
        
        try {
            $nl = new EventList($this->pm);
            if ($page > 0)
                $page -= 1;

            $nl->setPageSize($pageSize);
            $nl->setCurrentPage($page);
            
            $list = $nl->getList();
            $a = array();
            foreach ($list as $event) {
                $a[] = $event->toJson();      
            }

            $this->ok();
            $this->result = '{' .
                                '"page": ' . $nl->getCurrentPage() . ', ' .
                                '"totalPage": ' . $nl->getTotalPage() . ', ' .
                                '"result": [' . implode(',', $a) . ']' . 
                            '}';
            $a = NULL;
        }
        catch (Exception $e) {
            $this->error(e);
        }
    }

    public function update()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        $title = filter_var($this->params->title, FILTER_SANITIZE_STRING);
        $details = filter_var($this->params->details, FILTER_SANITIZE_STRING);

        try {
            $event = new Event($this->pm);
            $event->id = $id;
            $event->read();

            $event->title = $title;
            $event->details = $details;

            if ($event->save()) {
                $n = new Event($this->pm);
                $n->id = $event->id;
                $n->read(); 

                $this->ok();
                $this->result = $n->toJson();
            } 
            else {
                $s = NULL;

                if ($event->hasError()) {
                    $errors = $event->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->error($e);
        }
    }

    public function remove()
    {
        $id = filter_var($this->params->id, FILTER_VALIDATE_INT);
        
        try {
            $event = new Event($this->pm);
            $event->id = $id;

            if ($event->delete()) {
                $this->code = Rest::RC_NOCONTENT;
            }
            else {
                $s = NULL;

                if ($event->hasError()) {
                    $errors = $event->getError();
                    foreach ($errors as $e) {
                        $s = $s . $e['item'] . ' -> ' . $e['error']; 
                    }
                }

                $this->error($s);
            }
        }
        catch (Exception $e) {
            $this->stackTrace = true;
            $this->error($e);
        }
    }
}

?>
