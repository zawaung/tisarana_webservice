START TRANSACTION;

CREATE DATABASE  IF NOT EXISTS `tisarana` DEFAULT CHARACTER SET utf8;

USE `tisarana`;

--
-- Table structure for table `album`
--
CREATE TABLE `album` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `create_date` datetime DEFAULT NULL,
	  `creator` int(10) unsigned DEFAULT NULL,
	  `title` varchar(50) DEFAULT NULL,
	  `description` text,
	  `update_date` datetime DEFAULT NULL,
	  PRIMARY KEY (`id`),
	  KEY `create_date` (`create_date`),
	  KEY `update_date` (`update_date`),
	  KEY `title` (`title`),
	  KEY `creator` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `comment`
--
CREATE TABLE `comment` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `create_date` datetime DEFAULT NULL,
	  `creator` int(10) unsigned DEFAULT NULL,
	  `type` tinyint(3) unsigned DEFAULT NULL,
	  `link` int(10) unsigned DEFAULT NULL,
	  `ip` varchar(30) DEFAULT NULL,
	  `comment` text,
	  PRIMARY KEY (`id`),
	  KEY `comment_create_date_idx` (`create_date`),
	  KEY `comment_creator_idx` (`creator`),
	  KEY `comment_type_idx` (`type`),
	  KEY `comment_link_idx` (`link`),
	  KEY `comment_ip_idx` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `document`
--
CREATE TABLE `document` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `create_date` datetime DEFAULT NULL,
	  `creator` int(10) unsigned DEFAULT NULL,
	  `description` text,
	  `file` varchar(30) DEFAULT NULL,
	  PRIMARY KEY (`id`),
	  KEY `document_create_date_idx` (`create_date`),
	  KEY `document_creator_idx` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `event`
--
CREATE TABLE `event` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `title` varchar(100) NOT NULL,
	  `details` text NOT NULL,
	  `creator` int(10) unsigned NOT NULL,
	  `create_date` datetime DEFAULT NULL,
	  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `news`
--
CREATE TABLE `news` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `create_date` datetime DEFAULT NULL,
	  `update_date` datetime DEFAULT NULL,
	  `creator` int(10) unsigned DEFAULT NULL,
	  `title` varchar(50) DEFAULT NULL,
	  `details` text,
	  `publish_date` datetime DEFAULT NULL,
	  PRIMARY KEY (`id`),
	  KEY `news_create_date_idx` (`create_date`),
	  KEY `news_creator_idx` (`creator`),
	  KEY `news_title_idx` (`title`),
	  KEY `news_publish_date_idx` (`publish_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `photo`
--
CREATE TABLE `photo` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `create_date` datetime DEFAULT NULL,
	  `update_date` datetime DEFAULT NULL,
	  `creator` int(10) unsigned DEFAULT NULL,
	  `album` int(10) unsigned DEFAULT NULL,
	  `description` text,
	  `file` varchar(30) DEFAULT NULL,
	  PRIMARY KEY (`id`),
	  KEY `photo_create_date_idx` (`create_date`),
	  KEY `photo_creator_idx` (`creator`),
	  KEY `photo_album_idx` (`album`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sec_acl`
--
CREATE TABLE `sec_acl` (
	  `principal` int(11) unsigned NOT NULL,
	  `principal_type` enum('U','G') DEFAULT 'U' COMMENT 'U - User; G - Group',
	  `action` int(11) unsigned NOT NULL,
	  `create_date` datetime NOT NULL,
	  `creator` int(11) unsigned NOT NULL,
	  UNIQUE KEY `pta_idx` (`principal`,`principal_type`,`action`),
	  KEY `creator` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sec_action`
--
CREATE TABLE `sec_action` (
	  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	  `name` varchar(50) NOT NULL,
	  `create_date` datetime NOT NULL,
	  `update_date` datetime NOT NULL,
	  `creator` int(11) NOT NULL,
	  `object` int(11) unsigned NOT NULL,
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `on_idx` (`object`,`name`),
	  KEY `create_date` (`create_date`),
	  KEY `update_date` (`update_date`),
	  KEY `creator` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sec_group`
--
CREATE TABLE `sec_group` (
	  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	  `name` varchar(50) NOT NULL,
	  `create_date` datetime NOT NULL,
	  `update_date` datetime NOT NULL,
	  `creator` int(11) unsigned NOT NULL,
	  PRIMARY KEY (`id`),
	  KEY `name` (`name`),
	  KEY `create_date` (`create_date`),
	  KEY `update_date` (`update_date`),
	  KEY `creator` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sec_group_member`
--
CREATE TABLE `sec_group_member` (
	  `group_id` int(11) NOT NULL DEFAULT '0',
	  `user_id` int(11) NOT NULL DEFAULT '0',
	  `create_date` datetime DEFAULT NULL,
	  `creator` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sec_object`
--
CREATE TABLE `sec_object` (
	  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	  `name` varchar(50) DEFAULT NULL,
	  `create_date` datetime DEFAULT NULL,
	  `update_date` datetime DEFAULT NULL,
	  `creator` int(11) unsigned DEFAULT '0',
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `name` (`name`),
	  KEY `create_date` (`create_date`),
	  KEY `update_date` (`update_date`),
	  KEY `creator` (`creator`),
	  KEY `update_date_2` (`update_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `sec_user`
--
CREATE TABLE `sec_user` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `first_name` varchar(50) NOT NULL,
	  `last_name` varchar(50) DEFAULT NULL,
	  `login_name` varchar(50) NOT NULL,
	  `password` varchar(60) NOT NULL,
	  `email` varchar(100) NOT NULL,
	  `suspended` tinyint(3) unsigned DEFAULT '0',
	  `suspended_date` datetime DEFAULT NULL,
	  `create_date` datetime NOT NULL,
	  `update_date` datetime NOT NULL,
	  `creator` int(11) unsigned NOT NULL,
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `login_name` (`login_name`),
	  UNIQUE KEY `email` (`email`),
	  KEY `first_name` (`first_name`),
	  KEY `last_name` (`last_name`),
	  KEY `suspended_date` (`suspended_date`),
	  KEY `create_date` (`create_date`),
	  KEY `update_date` (`update_date`),
	  KEY `creator` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `video`
--
CREATE TABLE `video` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `update_date` datetime DEFAULT NULL,
	  `create_date` datetime DEFAULT NULL,
	  `creator` int(10) unsigned DEFAULT NULL,
	  `description` text,
	  `file` varchar(30) NOT NULL,
	  PRIMARY KEY (`id`),
	  KEY `video_create_date_idx` (`create_date`),
	  KEY `video_creator_idx` (`creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

COMMIT;
