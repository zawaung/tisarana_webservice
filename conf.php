<?php

if(!defined('CONF')) {
    define('CONF', 1);
    define('DB_DSN', 'mysql:dbname=tisarana;host=localhost');
    define('DB_USER', 'taintaman');
    define('DB_PWD', 'pianochitswe');

    define('APP_NAME', 'tisarana_webservice');
    define('BASE_PATH', dirname(__FILE__) . DIRECTORY_SEPARATOR);

    // storage for photos, videos and documents
    // must be absolute path with separator at the end
    define('FILE_STORAGE_PATH', 
            dir(__FILE__) . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR);

    // temporary storage for intermediate file such as deflated files etc.
    define('TEMP_STORAGE_PATH', DIRECTORY_SEPARATOR . 'tmp/storage' . DIRECTORY_SEPARATOR);

    define('CACHE_STORAGE_PATH', DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR);
}

?>
