<?php

include_once(BASE_PATH . 'test/config.php');

class TestDAO extends PHPUnit_Framework_TestCase
{
    protected $pdo = null;

    public function __construct() 
    {
        try {
            $this->pdo = new PDO(DB_DSN, DB_USER, DB_PWD);
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); 
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
        }
        catch (PDOException $e) {
            echo "Error connecting to database " . $e->getMessage();
        }
    }   

    public function __destruct()
    {
        $this->pdo = null;
    } 
}

?>
