<?php

include_once('test/config.php');

class NewsWebServiceTest extends PHPUnit_Framework_TestCase
{
    public function testAdd()
    {
        $creator = 1;
        $title = 'TEST';
        $details = 'WEB SERVICE TEST ADD NEWS';

        $post_values = http_build_query(array('creator' => $creator, 'title' => $title, 'details' => $details));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/news/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));

        $this->assertFalse(array_key_exists('error', $a));

        $this->assertTrue(array_key_exists('id', $a));
        $this->assertTrue(array_key_exists('createDate', $a));
        $this->assertTrue(array_key_exists('updateDate', $a));
        $this->assertTrue(array_key_exists('creator', $a));
        $this->assertTrue(array_key_exists('title', $a));
        $this->assertTrue(array_key_exists('details', $a));
        $this->assertTrue(array_key_exists('publishDate', $a));

        $this->assertTrue(is_numeric($a['id']) && $a['id'] > 0);

        $this->assertEquals($creator, $a['creator']);       
        $this->assertEquals($title, $a['title']);
        $this->assertEquals($details, $a['details']);
    }

    public function testList()
    {
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . '/news/?page=1&pageSize=10', 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $this->assertEquals($code, 200);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));

        $this->assertFalse(array_key_exists('error', $a));

        $this->assertTrue(array_key_exists('page', $a));
        $this->assertTrue(array_key_exists('totalPage', $a));
        $this->assertTrue(array_key_exists('result', $a));

        $this->assertTrue(is_array($a['result']));
        $this->assertEquals(count($a['result']), 10);
    }

    public function testGet()
    {
        // add a test record
        $creator = 1;
        $title = 'TEST';
        $details = 'WEB SERVICE TEST GET NEWS';

        $post_values = http_build_query(array('creator' => $creator, 'title' => $title, 'details' => $details));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/news/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertFalse(array_key_exists('error', $a));

        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . '/news/?id=' . $a['id'], 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $this->assertEquals($code, 200);

        $news = json_decode($response, true);

        $this->assertTrue(is_array($news));

        $this->assertFalse(array_key_exists('error', $news));

        $this->assertTrue(array_key_exists('id', $news));
        $this->assertTrue(array_key_exists('createDate', $news));
        $this->assertTrue(array_key_exists('updateDate', $news));
        $this->assertTrue(array_key_exists('creator', $news));
        $this->assertTrue(array_key_exists('title', $news));
        $this->assertTrue(array_key_exists('details', $news));
        $this->assertTrue(array_key_exists('publishDate', $news));

        $this->assertTrue(is_numeric($news['id']) && $news['id'] > 0);

        $this->assertEquals($creator, $news['creator']);      
        $this->assertEquals($title, $news['title']);
        $this->assertEquals($details, $news['details']);
    }

    public function testUpdate()
    {
        // add a test record
        $creator = 1;
        $title = 'TEST';
        $details = 'WEB SERVICE TEST UPDATE NEWS';

        $post_values = http_build_query(array('creator' => $creator, 'title' => $title, 'details' => $details));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/news/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertFalse(array_key_exists('error', $a));
        
        $title = "$title 1";
        $details = "$details 1";

        $post_values = http_build_query(array('id' => $a['id'], 'title' => $title, 'details' => $details));
    
        $options = array(CURLOPT_URL => WS_URL . "/news/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 200);

        $news = json_decode($response, true);

        $this->assertTrue(is_array($news));

        $this->assertFalse(array_key_exists('error', $news));

        $this->assertTrue(array_key_exists('id', $news));
        $this->assertTrue(array_key_exists('createDate', $news));
        $this->assertTrue(array_key_exists('updateDate', $news));
        $this->assertTrue(array_key_exists('creator', $news));
        $this->assertTrue(array_key_exists('title', $news));
        $this->assertTrue(array_key_exists('details', $news));
        $this->assertTrue(array_key_exists('publishDate', $news));

        $this->assertTrue(is_numeric($news['id']) && $news['id'] > 0);

        $this->assertEquals($creator, $news['creator']);
        $this->assertEquals($title, $news['title']);
        $this->assertEquals($details, $news['details']);
    }

    public function testDelete()
    {
        // add a test record
        $creator = 1;
        $title = 'TEST';
        $details = 'WEB SERVICE TEST DELETE NEWS';

        $post_values = http_build_query(array('creator' => $creator, 'title' => $title, 'details' => $details));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/news/" , 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertTrue(array_key_exists('id', $a));

        $post_values = http_build_query(array('id' => $a['id']));

        $options = array(CURLOPT_URL => WS_URL . '/news/', 
                            CURLOPT_POST => true,
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_CUSTOMREQUEST => 'DELETE', 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 200);

        $news = json_decode($response, true);

        $this->assertTrue(is_array($news));
        $this->assertFalse(array_key_exists('error', $news));

        $this->assertEquals(count(array_diff_assoc($a, $news)), 0);
    }
}

?>
