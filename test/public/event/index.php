<?php

include_once('test/config.php');

class EventWebServiceTest extends PHPUnit_Framework_TestCase
{
    public function testAdd()
    {
        $creator = 1;
        $title = 'TEST';
        $details = 'WEB SERVICE TEST ADD EVENT';

        $post_values = http_build_query(array('creator' => $creator, 'title' => $title, 'details' => $details));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/event/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));

        $this->assertFalse(array_key_exists('error', $a));

        $this->assertTrue(array_key_exists('id', $a));
        $this->assertTrue(array_key_exists('createDate', $a));
        $this->assertTrue(array_key_exists('creator', $a));
        $this->assertTrue(array_key_exists('title', $a));
        $this->assertTrue(array_key_exists('details', $a));

        $this->assertTrue(is_numeric($a['id']) && $a['id'] > 0);
        $this->assertEquals($creator, $a['creator']);       
        $this->assertEquals($title, $a['title']);
        $this->assertEquals($details, $a['details']);
    }

    public function testList()
    {
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . '/event/?page=1&pageSize=10', 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $this->assertEquals($code, 200);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));

        $this->assertFalse(array_key_exists('error', $a));

        $this->assertTrue(array_key_exists('page', $a));
        $this->assertTrue(array_key_exists('totalPage', $a));
        $this->assertTrue(array_key_exists('result', $a));

        $this->assertTrue(is_array($a['result']));
        $this->assertEquals(count($a['result']), 10);
    }

    public function testGet()
    {
        // add a test record
        $creator = 1;
        $title = 'TEST';
        $details = 'WEB SERVICE TEST GET EVENT';

        $post_values = http_build_query(array('creator' => 1, 'title' => $title, 'details' => $details));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/event/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertFalse(array_key_exists('error', $a));

        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . '/event/?id='. $a['id'], 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $this->assertEquals($code, 200);

        $event = json_decode($response, true);

        $this->assertTrue(is_array($event));

        $this->assertFalse(array_key_exists('error', $event));

        $this->assertTrue(array_key_exists('id', $event));
        $this->assertTrue(array_key_exists('createDate', $event));
        $this->assertTrue(array_key_exists('creator', $event));
        $this->assertTrue(array_key_exists('title', $event));
        $this->assertTrue(array_key_exists('details', $event));

        $this->assertTrue(is_numeric($event['id']) && $event['id'] > 0);

        $this->assertEquals($creator, $event['creator']);      
        $this->assertEquals($title, $event['title']);
        $this->assertEquals($details, $event['details']);
    }

    public function testUpdate()
    {
        // add a test record
        $creator = 1;
        $title = 'TEST';
        $details = 'WEB SERVICE TEST UPDATE EVENT';

        $post_values = http_build_query(array('creator' => $creator, 'title' => $title, 'details' => $details));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/event/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertFalse(array_key_exists('error', $a));
        
        $title = "$title 1";
        $details = "$details 1";

        $post_values = http_build_query(array('id' => $a['id'], 'title' => $title, 'details' => $details));
    
        $options = array(CURLOPT_URL => WS_URL . "/event/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 200);

        $event = json_decode($response, true);

        $this->assertTrue(is_array($event));

        $this->assertFalse(array_key_exists('error', $event));

        $this->assertTrue(array_key_exists('id', $event));
        $this->assertTrue(array_key_exists('createDate', $event));
        $this->assertTrue(array_key_exists('creator', $event));
        $this->assertTrue(array_key_exists('title', $event));
        $this->assertTrue(array_key_exists('details', $event));

        $this->assertTrue(is_numeric($event['id']) && $event['id'] > 0);

        $this->assertEquals($creator, $event['creator']);      
        $this->assertEquals($title, $event['title']);
        $this->assertEquals($details, $event['details']);
    }

    public function testDelete()
    {
        // add a test record
        $creator = 1;
        $title = 'TEST';
        $details = 'WEB SERVICE TEST DELETE EVENT';

        $post_values = http_build_query(array('creator' => $creator, 'title' => $title, 'details' => $details));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/event/" , 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertTrue(array_key_exists('id', $a));

        $post_values = http_build_query(array('id' => $a['id']));

        $options = array(CURLOPT_URL => WS_URL . '/event/', 
                            CURLOPT_POST => true,
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_CUSTOMREQUEST => 'DELETE', 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 200);

        $event = json_decode($response, true);

        $this->assertTrue(is_array($event));
        $this->assertFalse(array_key_exists('error', $event));

        $this->assertEquals(count(array_diff_assoc($a, $event)), 0);
    }
}

?>
