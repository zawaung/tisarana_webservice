<?php

include_once('test/config.php');

class PhotoWebServiceTest extends PHPUnit_Framework_TestCase
{
    public function testAdd()
    {
        $creator = 1;
        $title = 'TEST';
        $description = 'WEB SERVICE TEST ADD PHOTO';

        $post_values = http_build_query(array('creator' => $creator, 
                                                'title' => $title, 
                                                'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/album/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));

        $this->assertFalse(array_key_exists('error', $a));

        $this->assertTrue(array_key_exists('id', $a));
        $this->assertTrue(array_key_exists('createDate', $a));
        $this->assertTrue(array_key_exists('updateDate', $a));
        $this->assertTrue(array_key_exists('creator', $a));
        $this->assertTrue(array_key_exists('title', $a));
        $this->assertTrue(array_key_exists('description', $a));

        $this->assertTrue(is_numeric($a['id']) && $a['id'] > 0);
        $this->assertEquals($creator, $a['creator']);
        $this->assertEquals($title, $a['title']);
        $this->assertEquals($description, $a['description']);

        $description = 'TEST WEB SERVICE ADD';
        $file = 'test.png';

        $post_values = http_build_query(array('creator' => 1,
                                                'album' => $a['id'], 
                                                'description' => $description,
                                                'file' => $file));

        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/photo/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $b = json_decode($response, true);

        $this->assertTrue(is_array($b));

        $this->assertFalse(array_key_exists('error', $b));

        $this->assertTrue(array_key_exists('id', $b));
        $this->assertTrue(array_key_exists('createDate', $b));
        $this->assertTrue(array_key_exists('updateDate', $b));
        $this->assertTrue(array_key_exists('creator', $b));
        $this->assertTrue(array_key_exists('album', $b));
        $this->assertTrue(array_key_exists('description', $b));
        $this->assertTrue(array_key_exists('file', $b));

        $this->assertTrue(is_numeric($b['id']) && $b['id'] > 0);
        $this->assertEquals($creator, $b['creator']);
        $this->assertEquals($file, $b['file']);
        $this->assertEquals($description, $b['description']);
        $this->assertEquals($a['id'], $b['album']['id']);
    }

    public function testList()
    {
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . '/photo/?page=1&pageSize=10', 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $this->assertEquals($code, 200);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));

        $this->assertFalse(array_key_exists('error', $a));

        $this->assertTrue(array_key_exists('page', $a));
        $this->assertTrue(array_key_exists('totalPage', $a));
        $this->assertTrue(array_key_exists('result', $a));

        $this->assertTrue(is_array($a['result']));
        $this->assertEquals(count($a['result']), 10);
    }

    public function testGet()
    {
    // ADD NEW ALBUM
        $creator = 1;
        $title = 'TEST';
        $description = 'WEB SERVICE TEST GET PHOTO';

        $post_values = http_build_query(array('creator' => $creator, 
                                                'title' => $title, 
                                                'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/album/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

    // ADD A PHOTO
        $description = 'TEST WEB SERVICE GET';
        $file = 'test.png';

        $post_values = http_build_query(array('creator' => 1,
                                                'album' => $a['id'], 
                                                'description' => $description,
                                                'file' => $file));

        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/photo/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $b = json_decode($response, true);

        $this->assertTrue(is_array($b));

        $this->assertFalse(array_key_exists('error', $b));
    
    // READ NEWLY ADDED PHTO
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . '/photo/?id=' . $b['id'], 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $this->assertEquals($code, 200);

        $photo = json_decode($response, true);

        $this->assertTrue(is_array($photo));

        $this->assertFalse(array_key_exists('error', $photo));

        $this->assertTrue(array_key_exists('id', $photo));
        $this->assertTrue(array_key_exists('createDate', $photo));
        $this->assertTrue(array_key_exists('updateDate', $photo));
        $this->assertTrue(array_key_exists('creator', $photo));
        $this->assertTrue(array_key_exists('album', $photo));
        $this->assertTrue(array_key_exists('description', $photo));
        $this->assertTrue(array_key_exists('file', $photo));

        $this->assertTrue(is_numeric($photo['id']) && $photo['id'] > 0);
        $this->assertEquals($creator, $photo['creator']);
        $this->assertEquals($file, $photo['file']);
        $this->assertEquals($description, $photo['description']);
        $this->assertEquals($a['id'], $photo['album']['id']);
    }

    public function testUpdate()
    {
    // ADD NEW ALBUM
        $creator = 1;
        $title = 'TEST';
        $description = 'WEB SERVICE TEST UPDATE PHOTO';

        $post_values = http_build_query(array('creator' => $creator, 
                                                'title' => $title, 
                                                'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/album/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $a1 = json_decode($response, true);

        $title = 'TEST';
        $description = 'WEB SERVICE TEST UPDATE PHOTO';

        $post_values = http_build_query(array('creator' => $creator, 
                                                'title' => $title, 
                                                'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/album/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $a2 = json_decode($response, true);

    // ADD A PHOTO
        $description = 'TEST WEB SERVICE UPDATE';
        $file = 'test.png';

        $post_values = http_build_query(array('creator' => 1,
                                                'album' => $a1['id'], 
                                                'description' => $description,
                                                'file' => $file));

        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/photo/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $b = json_decode($response, true);

        $this->assertTrue(is_array($b));

        $this->assertFalse(array_key_exists('error', $b));
    
    // READ NEWLY ADDED PHTO
        $ch = curl_init();
        
        $file = "test1.jpg";
        $description = "$description 1";

        $post_values = http_build_query(array('id' => $b['id'],
                                                'album' => $a2['id'], 
                                                'description' => $description,
                                                'file' => $file));
    
        $options = array(CURLOPT_URL => WS_URL . "/photo/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 200);

        $photo = json_decode($response, true);

        $this->assertTrue(is_array($photo));

        $this->assertFalse(array_key_exists('error', $photo));

        $this->assertTrue(array_key_exists('id', $photo));
        $this->assertTrue(array_key_exists('createDate', $photo));
        $this->assertTrue(array_key_exists('updateDate', $photo));
        $this->assertTrue(array_key_exists('creator', $photo));
        $this->assertTrue(array_key_exists('album', $photo));
        $this->assertTrue(array_key_exists('description', $photo));
        $this->assertTrue(array_key_exists('file', $photo));

        $this->assertEquals($b['id'], $photo['id']);
        $this->assertEquals($file, $photo['file']);
        $this->assertEquals($description, $photo['description']);
        $this->assertEquals($a2['id'], $photo['album']['id']);
    }

    public function testDelete()
    {
    // ADD NEW ALBUM
        $creator = 1;
        $title = 'TEST';
        $description = 'WEB SERVICE TEST DELETE PHOTO';

        $post_values = http_build_query(array('creator' => $creator, 
                                                'title' => $title, 
                                                'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/album/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

    // ADD A PHOTO
        $description = 'TEST WEB SERVICE DELETE';
        $file = 'test.png';

        $post_values = http_build_query(array('creator' => $creator,
                                                'album' => $a['id'], 
                                                'description' => $description,
                                                'file' => $file));

        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/photo/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $b = json_decode($response, true);

        $this->assertTrue(is_array($b));

        $this->assertFalse(array_key_exists('error', $b));

    // DELETE RECENTLY ADDED PHOTO
        $ch = curl_init();
        $post_values = http_build_query(array('id' => $b['id']));

        $options = array(CURLOPT_URL => WS_URL . '/photo/', 
                            CURLOPT_POST => true,
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_CUSTOMREQUEST => 'DELETE', 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 200);

        $photo = json_decode($response, true);

        $this->assertTrue(is_array($photo));
        $this->assertFalse(array_key_exists('error', $photo));
    }
}

?>
