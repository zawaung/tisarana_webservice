<?php

include_once('test/config.php');

class VideoWebServiceTest extends PHPUnit_Framework_TestCase
{
    public function testAdd()
    {
        $creator = 1;
        $file = 'test.jpg';
        $description = 'WEB SERVICE TEST ADD VIDEO';

        $post_values = http_build_query(array('creator' => 1, 'file' => $file, 'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/video/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));

        $this->assertFalse(array_key_exists('error', $a));

        $this->assertTrue(array_key_exists('id', $a));
        $this->assertTrue(array_key_exists('createDate', $a));
        $this->assertTrue(array_key_exists('updateDate', $a));
        $this->assertTrue(array_key_exists('creator', $a));
        $this->assertTrue(array_key_exists('file', $a));
        $this->assertTrue(array_key_exists('description', $a));

        $this->assertTrue(is_numeric($a['id']) && $a['id'] > 0);
        $this->assertEquals($file, $a['file']);
        $this->assertEquals($description, $a['description']);
    }

    public function testList()
    {
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . '/video/?page=1&pageSize=10', 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 200);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));

        $this->assertFalse(array_key_exists('error', $a));

        $this->assertTrue(array_key_exists('page', $a));
        $this->assertTrue(array_key_exists('totalPage', $a));
        $this->assertTrue(array_key_exists('result', $a));

        $this->assertTrue(is_array($a['result']));
        $this->assertEquals(count($a['result']), 10);
    }

    public function testGet()
    {
        // add a test record
        $creator = 1;
        $file = 'test.jpg';
        $description = 'WEB SERVICE TEST GET VIDEO';

        $post_values = http_build_query(array('creator' => 1, 'file' => $file, 'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/video/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertFalse(array_key_exists('error', $a));

        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . '/video/?id=' . $a['id'], 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $this->assertEquals($code, 200);

        $video = json_decode($response, true);

        $this->assertTrue(is_array($video));

        $this->assertFalse(array_key_exists('error', $video));

        $this->assertTrue(array_key_exists('id', $video));
        $this->assertTrue(array_key_exists('createDate', $video));
        $this->assertTrue(array_key_exists('updateDate', $video));
        $this->assertTrue(array_key_exists('creator', $video));
        $this->assertTrue(array_key_exists('file', $video));
        $this->assertTrue(array_key_exists('description', $video));

        $this->assertTrue(is_numeric($video['id']) && $video['id'] > 0);
       
        $this->assertEquals($file, $video['file']);
        $this->assertEquals($description, $video['description']);
    }

    public function testUpdate()
    {
        // add a test record
        $creator = 1;
        $file = 'test.jpg';
        $description = 'WEB SERVICE TEST UPDATE VIDEO';

        $post_values = http_build_query(array('creator' => $creator, 'file' => $file, 'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/video/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertFalse(array_key_exists('error', $a));
        
        $file = "test1.jpg";
        $description = "$description 1";

        $post_values = http_build_query(array('id' => $a['id'], 'file' => $file, 'description' => $description));
    
        $options = array(CURLOPT_URL => WS_URL . "/video/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 200);

        $video = json_decode($response, true);

        $this->assertTrue(is_array($video));

        $this->assertFalse(array_key_exists('error', $video));

        $this->assertTrue(array_key_exists('id', $video));
        $this->assertTrue(array_key_exists('createDate', $video));
        $this->assertTrue(array_key_exists('updateDate', $video));
        $this->assertTrue(array_key_exists('creator', $video));
        $this->assertTrue(array_key_exists('file', $video));
        $this->assertTrue(array_key_exists('description', $video));

        $this->assertTrue(is_numeric($video['id']) && $video['id'] > 0);
       
        $this->assertEquals($file, $video['file']);
        $this->assertEquals($description, $video['description']);
    }

    public function testDelete()
    {
        // add a test record
        $creator = 1;
        $file = 'test.jpg';
        $description = 'WEB SERVICE TEST DELETE VIDEO';

        $post_values = http_build_query(array('creator' => 1, 'file' => $file, 'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/video/" , 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertTrue(array_key_exists('id', $a));

        $post_values = http_build_query(array('id' => $a['id']));

        $options = array(CURLOPT_URL => WS_URL . '/video/', 
                            CURLOPT_POST => true,
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_CUSTOMREQUEST => 'DELETE', 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 200);

        $video = json_decode($response, true);

        $this->assertTrue(is_array($video));
        $this->assertFalse(array_key_exists('error', $video));

        $this->assertEquals(count(array_diff_assoc($a, $video)), 0);
    }
}

?>
