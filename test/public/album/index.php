<?php

include_once('test/config.php');

class AlbumWebServiceTest extends PHPUnit_Framework_TestCase
{
    public function testAdd()
    {
        $creator = 1;
        $title = 'TEST';
        $description = 'WEB SERVICE TEST ADD ALBUM';

        $post_values = http_build_query(array('creator' => $creator, 
                                                'title' => $title, 
                                                'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/album/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));

        $this->assertFalse(array_key_exists('error', $a));

        $this->assertTrue(array_key_exists('id', $a));
        $this->assertTrue(array_key_exists('createDate', $a));
        $this->assertTrue(array_key_exists('updateDate', $a));
        $this->assertTrue(array_key_exists('creator', $a));
        $this->assertTrue(array_key_exists('title', $a));
        $this->assertTrue(array_key_exists('description', $a));

        $this->assertTrue(is_numeric($a['id']) && $a['id'] > 0);
        $this->assertEquals($creator, $a['creator']);
        $this->assertEquals($title, $a['title']);
        $this->assertEquals($description, $a['description']);
    }

    public function testList()
    {
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . '/album/?page=1&pageSize=10', 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $this->assertEquals($code, 200);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));

        $this->assertFalse(array_key_exists('error', $a));

        $this->assertTrue(array_key_exists('page', $a));
        $this->assertTrue(array_key_exists('totalPage', $a));
        $this->assertTrue(array_key_exists('result', $a));

        $this->assertTrue(is_array($a['result']));
        $this->assertEquals(count($a['result']), 10);
    }

    public function testGet()
    {
        // add a test record
        $creator = 1;
        $title = 'TEST';
        $description = 'WEB SERVICE TEST GET ALBUM';

        $post_values = http_build_query(array('creator' => $creator, 
                                                'title' => $title, 
                                                'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/album/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertFalse(array_key_exists('error', $a));

        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . '/album/?id=' . $a['id'], 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $this->assertEquals($code, 200);

        $album = json_decode($response, true);

        $this->assertTrue(is_array($album));

        $this->assertFalse(array_key_exists('error', $album));

        $this->assertTrue(array_key_exists('id', $album));
        $this->assertTrue(array_key_exists('createDate', $album));
        $this->assertTrue(array_key_exists('updateDate', $album));
        $this->assertTrue(array_key_exists('creator', $album));
        $this->assertTrue(array_key_exists('title', $album));
        $this->assertTrue(array_key_exists('description', $album));

        $this->assertTrue(is_numeric($album['id']) && $album['id'] > 0);

        $this->assertEquals($creator, $album['creator']);
        $this->assertEquals($title, $album['title']);
        $this->assertEquals($description, $album['description']);
    }

    public function testUpdate()
    {
        // add a test record
        $creator = 1;
        $title = 'TEST';
        $description = 'WEB SERVICE TEST UPDATE ALBUM';

        $post_values = http_build_query(array('creator' => 1, 'title' => $title, 'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/album/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertFalse(array_key_exists('error', $a));
        
        $title = "$title 1";
        $description = "$description 1";

        $post_values = http_build_query(array('id' => $a['id'], 'title' => $title, 'description' => $description));
    
        $options = array(CURLOPT_URL => WS_URL . "/album/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 200);

        $album = json_decode($response, true);

        $this->assertTrue(is_array($album));

        $this->assertFalse(array_key_exists('error', $album));

        $this->assertTrue(array_key_exists('id', $album));
        $this->assertTrue(array_key_exists('createDate', $album));
        $this->assertTrue(array_key_exists('updateDate', $album));
        $this->assertTrue(array_key_exists('creator', $album));
        $this->assertTrue(array_key_exists('title', $album));
        $this->assertTrue(array_key_exists('description', $album));

        $this->assertTrue(is_numeric($album['id']) && $album['id'] > 0);

        $this->assertEquals($creator, $album['creator']);
        $this->assertEquals($title, $album['title']);
        $this->assertEquals($description, $album['description']);
    }

    public function testDelete()
    {
        // add a test record
        $creator = 1;
        $title = 'TEST';
        $description = 'WEB SERVICE TEST DELETE ALBUM';

        $post_values = http_build_query(array('creator' => $creator, 
                                                'title' => $title, 
                                                'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/album/" , 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertTrue(array_key_exists('id', $a));

        $post_values = http_build_query(array('id' => $a['id']));

        $options = array(CURLOPT_URL => WS_URL . '/album/', 
                            CURLOPT_POST => true,
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_CUSTOMREQUEST => 'DELETE', 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 200);

        $album = json_decode($response, true);

        $this->assertTrue(is_array($album));
        $this->assertFalse(array_key_exists('error', $album));

        $this->assertEquals(count(array_diff_assoc($a, $album)), 0);
    }
}

?>
