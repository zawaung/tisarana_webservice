<?php

include_once('test/config.php');

class DocumentWebServiceTest extends PHPUnit_Framework_TestCase
{
    public function testAdd()
    {
        $creator = 1;
        $file = 'test.pdf';
        $description = 'WEB SERVICE TEST ADD DOCUMENT';

        $post_values = http_build_query(array('creator' => 1, 'file' => $file, 'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/document/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));

        $this->assertFalse(array_key_exists('error', $a));

        $this->assertTrue(array_key_exists('id', $a));
        $this->assertTrue(array_key_exists('createDate', $a));
        $this->assertTrue(array_key_exists('updateDate', $a));
        $this->assertTrue(array_key_exists('creator', $a));
        $this->assertTrue(array_key_exists('file', $a));
        $this->assertTrue(array_key_exists('description', $a));

        $this->assertTrue(is_numeric($a['id']) && $a['id'] > 0);
        $this->assertEquals($file, $a['file']);
        $this->assertEquals($description, $a['description']);
    }

    public function testList()
    {
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . '/document/?page=1&pageSize=10', 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        $this->assertEquals($code, 200);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));

        $this->assertFalse(array_key_exists('error', $a));

        $this->assertTrue(array_key_exists('page', $a));
        $this->assertTrue(array_key_exists('totalPage', $a));
        $this->assertTrue(array_key_exists('result', $a));

        $this->assertTrue(is_array($a['result']));
        $this->assertEquals(count($a['result']), 10);
    }

    public function testGet()
    {
        // add a test record
        $creator = 1;
        $file = 'test.pdf';
        $description = 'WEB SERVICE TEST GET DOCUMENT';

        $post_values = http_build_query(array('creator' => 1, 'file' => $file, 'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/document/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertFalse(array_key_exists('error', $a));

        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . '/document/?id=' . $a['id'], 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $this->assertEquals($code, 200);

        $document = json_decode($response, true);

        $this->assertTrue(is_array($document));

        $this->assertFalse(array_key_exists('error', $document));

        $this->assertTrue(array_key_exists('id', $document));
        $this->assertTrue(array_key_exists('createDate', $document));
        $this->assertTrue(array_key_exists('updateDate', $document));
        $this->assertTrue(array_key_exists('creator', $document));
        $this->assertTrue(array_key_exists('file', $document));
        $this->assertTrue(array_key_exists('description', $document));

        $this->assertTrue(is_numeric($document['id']) && $document['id'] > 0);
       
        $this->assertEquals($file, $document['file']);
        $this->assertEquals($description, $document['description']);
    }

    public function testUpdate()
    {
        // add a test record
        $creator = 1;
        $file = 'test.pdf';
        $description = 'WEB SERVICE TEST UPDATE DOCUMENT';

        $post_values = http_build_query(array('creator' => $creator, 'file' => $file, 'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/document/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertFalse(array_key_exists('error', $a));
        
        $file = "test1.pdf";
        $description = "$description 1";

        $post_values = http_build_query(array('id' => $a['id'], 'file' => $file, 'description' => $description));
    
        $options = array(CURLOPT_URL => WS_URL . "/document/", 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 200);

        $document = json_decode($response, true);

        $this->assertTrue(is_array($document));

        $this->assertFalse(array_key_exists('error', $document));

        $this->assertTrue(array_key_exists('id', $document));
        $this->assertTrue(array_key_exists('createDate', $document));
        $this->assertTrue(array_key_exists('updateDate', $document));
        $this->assertTrue(array_key_exists('creator', $document));
        $this->assertTrue(array_key_exists('file', $document));
        $this->assertTrue(array_key_exists('description', $document));

        $this->assertTrue(is_numeric($document['id']) && $document['id'] > 0);
       
        $this->assertEquals($file, $document['file']);
        $this->assertEquals($description, $document['description']);
    }

    public function testDelete()
    {
        // add a test record
        $creator = 1;
        $file = 'test.pdf';
        $description = 'WEB SERVICE TEST DELETE DOCUMENT';

        $post_values = http_build_query(array('creator' => 1, 'file' => $file, 'description' => $description));
    
        $ch = curl_init();
        $options = array(CURLOPT_URL => WS_URL . "/document/" , 
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $this->assertEquals($code, 201);

        $a = json_decode($response, true);

        $this->assertTrue(is_array($a));
        $this->assertTrue(array_key_exists('id', $a));

        $post_values = http_build_query(array('id' => $a['id']));

        $options = array(CURLOPT_URL => WS_URL . '/document/', 
                            CURLOPT_POST => true,
                            CURLOPT_POSTFIELDS => $post_values, 
                            CURLOPT_CUSTOMREQUEST => 'DELETE', 
                            CURLOPT_RETURNTRANSFER => true);
       
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($code, 200);

        $document = json_decode($response, true);

        $this->assertTrue(is_array($document));
        $this->assertFalse(array_key_exists('error', $document));

        $this->assertEquals(count(array_diff_assoc($a, $document)), 0);
    }
}

?>
