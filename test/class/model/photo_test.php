<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/photo.php');

/*
 *  Unit testing the Photo class.
 */

class PhotoModelTest extends PHPUnit_Framework_TestCase
{
    protected $pm = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

//
// TESTING THE VALIDATION OF ID
//
    public function testValidateId()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST VALIDATE ID - PHOTO ';
        $album->description = 'TEST PHOTO MODEL VALIDATE ID';
        
        if ($album->save()) 
            $album->read();

        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->file = 'test.jpg';
        $photo->album = $album;
        $photo->description = 'TEST MODEL SAVE';
        $this->assertTrue($photo->save());

        $id = $photo->id;

        // CHECK UNDEFINED OR NULL 
            $photo->id = NULL;
            $photo->validate();
            $a = $photo->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NON NUMERIC 
            $photo->id = 'a';
            $photo->validate();
            $a = null;
            $a = $photo->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NOT GREATER THAN ZERO
            $photo->id = 0;
            $photo->validate();
            $a = null;
            $a = $photo->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NORMAL OR NOTHING WRONG CASE
            $photo->id = $id;
            $photo->validate();

            $a = null;
            $a = $photo->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertFalse($found);
    }

//
// TESTING THE VALIDATION OF ALBUM
//
    public function testValidateAlbum()
    {
    // CHECK UNDEFINED OR NULL 
        $photo = new Photo($this->pm);
        $photo->validate();
        $a = $photo->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'album') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK NON-OBJECT TYPE  
        $photo = new Photo($this->pm);
        $photo->album = 1;
        $photo->validate();
        $a = $photo->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'album') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
   
     // CHECK NOT TYPE ALBUM
        $photo = new Photo($this->pm);
        $photo->album = $photo;
        $photo->validate();
        $a = $photo->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'album') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $photo->album = new Album($this->pm);
        $photo->validate();
        $a = null;
        $a = $photo->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'album') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }

//
// TESTING THE VALIDATION OF FILE
//
    public function testValidateFile()
    {
    // CHECK UNDEFINED OR NULL 
        $photo = new Photo($this->pm);
        $photo->validate();
        $a = $photo->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'file') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK BLANK FILE 
        $photo = new Photo($this->pm);
        $photo->file = ' ';
        $photo->validate();
        $a = $photo->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'file') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $photo->file = 'HELLO';
        $photo->validate();
        $a = null;
        $a = $photo->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'file') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }
    
//
// TESTING THE VALIDATION OF DESCRIPTION
//
    public function testValidateDescription()
    {
    // CHECK UNDEFINED OR NULL 
        $photo = new Photo($this->pm);
        $photo->validate();
        $a = $photo->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'description') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK BLANK DESCRIPTION 
        $photo = new Photo($this->pm);
        $photo->description = ' ';
        $photo->validate();
        $a = $photo->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'description') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $photo->description = 'HELLO';
        $photo->validate();
        $a = null;
        $a = $photo->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'description') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }

//
// TEST SAVE
//
    public function testSavePhoto() 
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST SAVE - PHOTO ';
        $album->description = 'TEST PHOTO MODEL SAVE';
        
        if ($album->save()) 
            $album->read();

        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->album = $album;
        $photo->file = 'test,png';
        $photo->description = 'TEST MODEL SAVE';
        $this->assertTrue($photo->save());
        $this->assertTrue(!is_null($photo->id) && $photo->id > 0);
    }

    public function testRead() 
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST READ - PHOTO ';
        $album->description = 'TEST PHOTO MODEL READ';
        
        if ($album->save()) 
            $album->read();
       
        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->album = $album;
        $photo->file = 'test,png';
        $photo->description = 'TEST MODEL SAVE';
        $this->assertTrue($photo->save());
        $this->assertTrue(!is_null($photo->id) && $photo->id > 0);
        $photo->read();

        $test = new Photo($this->pm);
        $test->id = $photo->id;

        $this->assertTrue($test->read());
        $this->assertEquals($test->creator, $photo->creator);
        $this->assertEquals($test->file, $photo->file);
        $this->assertTrue($test->equals($photo));
        $this->assertEquals($test->description, $photo->description);
    }

    public function testSaveUpdate() 
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST UPDATE - PHOTO ';
        $album->description = 'TEST PHOTO MODEL UPDATE';
        
        if ($album->save()) 
            $album->read();
       
        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->album = $album;
        $photo->file = 'test,png';
        $photo->description = 'TEST MODEL UPDATE';
        $this->assertTrue($photo->save());
        $this->assertTrue(!is_null($photo->id) && $photo->id > 0);


        $photo->file = 'test1,png';
        $photo->description = 'TEST MODEL UPDATE';
        $photo->save();

        $test = new Photo($this->pm);
        $test->id = $photo->id;
        $test->read();

        $this->assertEquals($test->file, $photo->file);
        $this->assertEquals($test->description, $photo->description);
    }

    public function testDelete() 
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST DELETE - PHOTO ';
        $album->description = 'TEST PHOTO MODEL DELETE';
        
        if ($album->save()) 
            $album->read();
       
        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->album = $album;
        $photo->file = 'test,png';
        $photo->description = 'TEST MODEL DELETE';
        $this->assertTrue($photo->save());
        $this->assertTrue(!is_null($photo->id) && $photo->id > 0);

        $photo->delete();

        $this->assertTrue(is_null($photo->id) || $photo->id == 0);
        $this->assertTrue(is_null($photo->createDate) || strlen(trim($photo->createDate)) <= 0);
        $this->assertTrue(is_null($photo->updateDate) || strlen(trim($photo->updateDate)) <= 0);
        $this->assertTrue(is_null($photo->creator) || $photo->creator <= 0);
        $this->assertTrue(is_null($photo->file) || strlen(trim($photo->file)) <= 0);
        $this->assertTrue(is_null($photo->description) || strlen(trim($photo->description)) <= 0);
        $this->assertTrue(is_null($photo->album));
    }

    public function testEquals()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST EQUALS - PHOTO';
        $album->description = 'TEST PHOTO MODEL EQUALS';
        
        if ($album->save()) 
            $album->read();
       
        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->album = $album;
        $photo->file = 'test,png';
        $photo->description = 'TEST MODEL EQUALS';
        $this->assertTrue($photo->save());
        $this->assertTrue(!is_null($photo->id) && $photo->id > 0);

        $photo->read($photo->id);

        $test = clone $photo;

        $this->assertTrue($photo->equals($test));

        $test->id = 99943000;
        $this->assertFalse($photo->equals($test));

        $test->id = $photo->id;
        $test->createDate = 'djksadkdsadsa';
        $this->assertFalse($photo->equals($test));

        $test->createDate = $photo->createDate;
        $test->updateDate = 'dsafgdgfgf';
        $this->assertFalse($photo->equals($test));

        $test->updateDate = $photo->updateDate;
        $test->creator = 999999;
        $this->assertFalse($photo->equals($test));

        $test->creator = $photo->creator;
        $test->file = 'fkreree';
        $this->assertFalse($photo->equals($test));

        $test->file = $photo->file;
        $test->description = 'fksfdsfds';
        $this->assertFalse($photo->equals($test));

        $test->description = $photo->description;
        $test->album = new Album($this->pm);
        $this->assertFalse($photo->equals($test));

        $test->album = $photo->album;
        $this->assertTrue($photo->equals($test));
    }

    public function testEqualsArray()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST EQUALS ARRAY- PHOTO';
        $album->description = 'TEST PHOTO MODEL EQUALSARRAY';
        
        if ($album->save()) 
            $album->read();
       
        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->album = $album;
        $photo->file = 'test,png';
        $photo->description = 'TEST MODEL EQUALSARRAY';
        $this->assertTrue($photo->save());
        $this->assertTrue(!is_null($photo->id) && $photo->id > 0);

        $photo->read($photo->id);

        $a = $photo->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($photo->equalsArray($a));

        $a['id'] = 999999099;
        $this->assertFalse($photo->equalsArray($a));

        $a['id'] = $photo->id;
        $a['createDate'] = 'djksadkdsadsa';
        $this->assertFalse($photo->equalsArray($a));

        $a['createDate'] = $photo->createDate;
        $a['updateDate'] = 'dsafgdgfgf';
        $this->assertFalse($photo->equalsArray($a));

        $a['updateDate'] = $photo->updateDate;
        $a['creator'] = 999999;
        $this->assertFalse($photo->equalsArray($a));

        $a['creator'] = $photo->creator;
        $a['file'] = 'fkreree';
        $this->assertFalse($photo->equalsArray($a));

        $a['file'] = $photo->file;
        $a['description'] = 'fksfdsfds';
        $this->assertFalse($photo->equalsArray($a));

        $a['description'] = $photo->description;
        $a['album'] = (new Album($this->pm))->toArray();
        $this->assertFalse($photo->equalsArray($a));

        $a['album'] = $photo->album->toArray();
        $this->assertTrue($photo->equalsArray($a));
    }

    public function testToArray()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST TO ARRAY- PHOTO';
        $album->description = 'TEST PHOTO MODEL TOARRAY';
        
        if ($album->save()) 
            $album->read();
       
        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->album = $album;
        $photo->file = 'test,png';
        $photo->description = 'TEST MODEL TOARRAY';
        $this->assertTrue($photo->save());
        $this->assertTrue(!is_null($photo->id) && $photo->id > 0);

        $photo->read($photo->id);

        $a = $photo->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($photo->equalsArray($a));
    }

    public function testFromArray()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST FROM ARRAY- PHOTO';
        $album->description = 'TEST PHOTO MODEL FROMARRAY';
        
        if ($album->save()) 
            $album->read();

        $photo = new Photo($this->pm);

        $a = array('id' => 1, 
                    'createDate' => '2014-01-01 00:00:01', 
                    'updateDate' => '2001-01-01 00:00:01',
                    'creator' => 1,
                    'file' => 'test.png',
                    'description' => 'TEST MODEL FROMARRAY',
                    'album' => $album->toArray());

        $photo->fromArray($a);

        $this->assertTrue($photo->equalsArray($a));
    }

    public function testToJson()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST TO JSON - PHOTO';
        $album->description = 'TEST PHOTO MODEL TOJSON';
        
        if ($album->save()) 
            $album->read();
       
        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->album = $album;
        $photo->file = 'test,png';
        $photo->description = 'TEST MODEL TOJSON';
        $this->assertTrue($photo->save());
        $this->assertTrue(!is_null($photo->id) && $photo->id > 0);

        $photo->read($photo->id);

        $s = $photo->toJson();
        $this->assertTrue(strlen(trim($s)) > 0);
        $a = json_decode($s, true);
        $this->assertTrue($photo->equalsArray($a));
    }

    public function testFromJson()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST FROM JSON - PHOTO';
        $album->description = 'TEST PHOTO MODEL FROMJSON';
        
        if ($album->save()) 
            $album->read();
       
        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->album = $album;
        $photo->file = 'test,png';
        $photo->description = 'TEST MODEL FROMJSON';
        $this->assertTrue($photo->save());
        $this->assertTrue(!is_null($photo->id) && $photo->id > 0);

        $photo->read($photo->id);

        $s = $photo->toJson();

        $test = new Photo($this->pm);
        $test->fromJson($s);
        $this->assertTrue($photo->equals($test));
    }
}

?>
