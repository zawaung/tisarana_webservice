<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/news.php');

/*
 *  Unit testing the News class.
 */

class NewsModelTest extends PHPUnit_Framework_TestCase
{
    protected $pm = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

//
// TESTING THE VALIDATION OF ID
//
    public function testValidateId()
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST VALIDATE ID';
        $news->details = 'TEST NEWS MODEL VALIDATE ID';
        
        if ($news->save()) {
            $news->read();
            $id = $news->id;

        // CHECK UNDEFINED OR NULL 
            $news->id = NULL;
            $news->validate();
            $a = $news->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NON NUMERIC 
            $news->id = 'a';
            $news->validate();
            $a = null;
            $a = $news->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NOT GREATER THAN ZERO
            $news->id = 0;
            $news->validate();
            $a = null;
            $a = $news->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NORMAL OR NOTHING WRONG CASE
            $news->id = $id;
            $news->validate();
            $a = null;
            $a = $news->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertFalse($found);
        }
    }

//
// TESTING THE VALIDATION OF TITLE
//
    public function testValidateTitle()
    {
    // CHECK UNDEFINED OR NULL 
        $news = new News($this->pm);
        $news->validate();
        $a = $news->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'title') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK BLANK TITLE 
        $news = new News($this->pm);
        $news->title = ' ';
        $news->validate();
        $a = $news->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'title') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $news->title = 'HELLO';
        $news->validate();
        $a = null;
        $a = $news->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'title') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }
    
//
// TESTING THE VALIDATION OF DETAILS
//
    public function testValidateDetails()
    {
    // CHECK UNDEFINED OR NULL 
        $news = new News($this->pm);
        $news->validate();
        $a = $news->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'details') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK BLANK DETAILS 
        $news = new News($this->pm);
        $news->details = ' ';
        $news->validate();
        $a = $news->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'details') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $news->details = 'HELLO';
        $news->validate();
        $a = null;
        $a = $news->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'details') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }

//
// TEST SAVE
//
    public function testSaveNews() 
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST';
        $news->details = 'TEST NEWS MODEL SAVE';
        $this->assertTrue($news->save());
        $this->assertTrue(!is_null($news->id) && $news->id > 0);
    }

    public function testRead() 
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST';
        $news->details = 'TEST NEWS MODEL READ';
        $this->assertTrue($news->save());
        $this->assertTrue(!is_null($news->id) && $news->id > 0);

        $test = new News($this->pm);
        $test->id = $news->id;

        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $news->creator);
        $this->assertTrue($test->title == $news->title);
        $this->assertTrue($test->details == $news->details);
    }

    public function testSaveUpdate() 
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST UPDATE';
        $news->details = 'TEST NEWS MODEL';
        $this->assertTrue($news->save());
        $this->assertTrue(!is_null($news->id) && $news->id > 0);


        $news->title = 'TEST UPDATE - 1';
        $news->details = 'TEST NEWS MODEL UPDATE';
        $news->save();

        $test = new News($this->pm);
        $test->id = $news->id;
        $test->read();

        $this->assertTrue($test->title == $news->title);
        $this->assertTrue($test->details == $news->details);

        $test1 = new News($this->pm);
        $test1->id = $news->id;
        $test1->read();

        $test1->title = 'TEST UPDATE - 2';
        $test1->details = 'TEST NEWS MODEL UPDATE';
        $test1->save();

        $test = null;
        $test = new News($this->pm);
        $test->id = $news->id;
        $test->read();

        $this->assertTrue($test->title == $test1->title);
        $this->assertTrue($test->details == $test1->details);

    }

    public function testDelete() 
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST DELETE';
        $news->details = 'TEST NEWS MODEL DELETE';
        $this->assertTrue($news->save());
        $this->assertTrue(!is_null($news->id) && $news->id > 0);

        $news->delete();

        $this->assertTrue(is_null($news->id) || $news->id == 0);
        $this->assertTrue(is_null($news->createDate) || strlen(trim($news->createDate)) <= 0);
        $this->assertTrue(is_null($news->updateDate) || strlen(trim($news->updateDate)) <= 0);
        $this->assertTrue(is_null($news->creator) || $news->creator <= 0);
        $this->assertTrue(is_null($news->title) || strlen(trim($news->title)) <= 0);
        $this->assertTrue(is_null($news->details) || strlen(trim($news->details)) <= 0);
    }

    public function testEquals()
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST EQUALS ARRAY';
        $news->details = 'TEST NEWS MODEL EQUALS ARRAY';
        $this->assertTrue($news->save());
        $this->assertTrue(!is_null($news->id) && $news->id > 0);

        $news->read($news->id);

        $test = clone $news;

        $this->assertTrue($news->equals($test));

        $test->id = 99943000;
        $this->assertFalse($news->equals($test));

        $test->id = $news->id;
        $test->createDate = 'djksadkdsadsa';
        $this->assertFalse($news->equals($test));

        $test->createDate = $news->createDate;
        $test->updateDate = 'dsafgdgfgf';
        $this->assertFalse($news->equals($test));

        $test->updateDate = $news->updateDate;
        $test->creator = 999999;
        $this->assertFalse($news->equals($test));

        $test->creator = $news->creator;
        $test->title = 'fkreree';
        $this->assertFalse($news->equals($test));

        $test->title = $news->title;
        $test->details = 'fksfdsfds';
        $this->assertFalse($news->equals($test));

        $test->details = $news->details;
        $test->publishDate = 'dsadsadsa';
        $this->assertFalse($news->equals($test));

        $test->publishDate = $news->publishDate;
        $this->assertTrue($news->equals($test));
    }

    public function testEqualsArray()
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST EQUALS ARRAY';
        $news->details = 'TEST NEWS MODEL EQUALS ARRAY';
        $this->assertTrue($news->save());
        $this->assertTrue(!is_null($news->id) && $news->id > 0);

        $news->read($news->id);

        $a = $news->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($news->equalsArray($a));

        $a['id'] = 999999099;
        $this->assertFalse($news->equalsArray($a));

        $a['id'] = $news->id;
        $a['createDate'] = 'djksadkdsadsa';
        $this->assertFalse($news->equalsArray($a));

        $a['createDate'] = $news->createDate;
        $a['updateDate'] = 'dsafgdgfgf';
        $this->assertFalse($news->equalsArray($a));

        $a['updateDate'] = $news->updateDate;
        $a['creator'] = 999999;
        $this->assertFalse($news->equalsArray($a));

        $a['creator'] = $news->creator;
        $a['title'] = 'fkreree';
        $this->assertFalse($news->equalsArray($a));

        $a['title'] = $news->title;
        $a['details'] = 'fksfdsfds';
        $this->assertFalse($news->equalsArray($a));

        $a['details'] = $news->details;
        $a['publishDate'] = 'dsadsadsa';
        $this->assertFalse($news->equalsArray($a));

        $a['publishDate'] = $news->publishDate;
        $this->assertTrue($news->equalsArray($a));
    }

    public function testToArray()
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST TO ARRAY';
        $news->details = 'TEST NEWS MODEL TO ARRAY';
        $this->assertTrue($news->save());
        $this->assertTrue(!is_null($news->id) && $news->id > 0);

        $news->read($news->id);

        $a = $news->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($news->equalsArray($a));
    }

    public function testFromArray()
    {
        $news = new News($this->pm);

        $a = array('id' => 1, 
                    'createDate' => '2014-01-01 00:00:01', 
                    'updateDate' => '2001-01-01 00:00:01',
                    'creator' => 1,
                    'title' => 'TEST TO ARRAY',
                    'details' => 'TEST NEWS MODEL TO ARRAY',
                    'publishDate' => NULL);

        $news->fromArray($a);

        $this->assertTrue($news->equalsArray($a));
    }

    public function testToJson()
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST TO JSON';
        $news->details = 'TEST NEWS MODEL TO JSON';
        $this->assertTrue($news->save());
        $this->assertTrue(!is_null($news->id) && $news->id > 0);

        $news->read($news->id);

        $s = $news->toJson();
        $this->assertTrue(strlen(trim($s)) > 0);
        $a = json_decode($s, true);
        $this->assertTrue($news->equalsArray($a));
    }

    public function testFromJson()
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST FROM JSON';
        $news->details = 'TEST NEWS MODEL FROM JSON';
        $this->assertTrue($news->save());
        $this->assertTrue(!is_null($news->id) && $news->id > 0);

        $news->read($news->id);

        $s = $news->toJson();

        $test = new News($this->pm);
        $test->fromJson($s);
        $this->assertTrue($news->equals($test));
    }

}

?>
