<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/event.php');

/*
 *  Unit testing the Event class.
 */

class EventModelTest extends PHPUnit_Framework_TestCase
{
    protected $pm = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

//
// TESTING THE VALIDATION OF ID
//
    public function testValidateId()
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST VALIDATE ID';
        $event->details = 'TEST NEWS MODEL VALIDATE ID';
        
        if ($event->save()) {
            $event->read();
            $id = $event->id;

        // CHECK UNDEFINED OR NULL 
            $event->id = NULL;
            $event->validate();
            $a = $event->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NON NUMERIC 
            $event->id = 'a';
            $event->validate();
            $a = null;
            $a = $event->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NOT GREATER THAN ZERO
            $event->id = 0;
            $event->validate();
            $a = null;
            $a = $event->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NORMAL OR NOTHING WRONG CASE
            $event->id = $id;
            $event->validate();
            $a = null;
            $a = $event->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertFalse($found);
        }
    }

//
// TESTING THE VALIDATION OF TITLE
//
    public function testValidateTitle()
    {
    // CHECK UNDEFINED OR NULL 
        $event = new Event($this->pm);
        $event->validate();
        $a = $event->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'title') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK BLANK TITLE 
        $event = new Event($this->pm);
        $event->title = ' ';
        $event->validate();
        $a = $event->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'title') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $event->title = 'HELLO';
        $event->validate();
        $a = null;
        $a = $event->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'title') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }
    
//
// TESTING THE VALIDATION OF DETAILS
//
    public function testValidateDetails()
    {
    // CHECK UNDEFINED OR NULL 
        $event = new Event($this->pm);
        $event->validate();
        $a = $event->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'details') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK BLANK DETAILS 
        $event = new Event($this->pm);
        $event->details = ' ';
        $event->validate();
        $a = $event->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'details') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $event->details = 'HELLO';
        $event->validate();
        $a = null;
        $a = $event->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'details') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }

//
// TEST SAVE
//
    public function testSaveEvent() 
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST';
        $event->details = 'TEST NEWS MODEL SAVE';
        $this->assertTrue($event->save());
        $this->assertTrue(!is_null($event->id) && $event->id > 0);
    }

    public function testRead() 
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST';
        $event->details = 'TEST NEWS MODEL READ';
        $this->assertTrue($event->save());
        $this->assertTrue(!is_null($event->id) && $event->id > 0);

        $test = new Event($this->pm);
        $test->id = $event->id;

        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $event->creator);
        $this->assertTrue($test->title == $event->title);
        $this->assertTrue($test->details == $event->details);
    }

    public function testSaveUpdate() 
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST UPDATE';
        $event->details = 'TEST NEWS MODEL';
        $this->assertTrue($event->save());
        $this->assertTrue(!is_null($event->id) && $event->id > 0);


        $event->title = 'TEST UPDATE - 1';
        $event->details = 'TEST NEWS MODEL UPDATE';
        $event->save();

        $test = new Event($this->pm);
        $test->id = $event->id;
        $test->read();

        $this->assertTrue($test->title == $event->title);
        $this->assertTrue($test->details == $event->details);

        $test1 = new Event($this->pm);
        $test1->id = $event->id;
        $test1->read();

        $test1->title = 'TEST UPDATE - 2';
        $test1->details = 'TEST NEWS MODEL UPDATE';
        $test1->save();

        $test = null;
        $test = new Event($this->pm);
        $test->id = $event->id;
        $test->read();

        $this->assertTrue($test->title == $test1->title);
        $this->assertTrue($test->details == $test1->details);

    }

    public function testDelete() 
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST DELETE';
        $event->details = 'TEST NEWS MODEL DELETE';
        $this->assertTrue($event->save());
        $this->assertTrue(!is_null($event->id) && $event->id > 0);

        $event->delete();

        $this->assertTrue(is_null($event->id) || $event->id == 0);
        $this->assertTrue(is_null($event->createDate) || strlen(trim($event->createDate)) <= 0);
        //$this->assertTrue(is_null($event->updateDate) || strlen(trim($event->updateDate)) <= 0);
        $this->assertTrue(is_null($event->creator) || $event->creator <= 0);
        $this->assertTrue(is_null($event->title) || strlen(trim($event->title)) <= 0);
        $this->assertTrue(is_null($event->details) || strlen(trim($event->details)) <= 0);
    }

    public function testEquals()
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST EQUALS ARRAY';
        $event->details = 'TEST NEWS MODEL EQUALS ARRAY';
        $this->assertTrue($event->save());
        $this->assertTrue(!is_null($event->id) && $event->id > 0);

        $event->read($event->id);

        $test = clone $event;

        $this->assertTrue($event->equals($test));

        $test->id = 99943000;
        $this->assertFalse($event->equals($test));

        $test->id = $event->id;
        $test->createDate = 'djksadkdsadsa';
        $this->assertFalse($event->equals($test));
/*
        $test->createDate = $event->createDate;
        $test->updateDate = 'dsafgdgfgf';
        $this->assertFalse($event->equals($test));

        $test->updateDate = $event->updateDate;
        $test->creator = 999999;
        $this->assertFalse($event->equals($test));
 */
        $test->creator = $event->creator;
        $test->title = 'fkreree';
        $this->assertFalse($event->equals($test));

        $test->title = $event->title;
        $test->details = 'fksfdsfds';
        $this->assertFalse($event->equals($test));
    }

    public function testEqualsArray()
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST EQUALS ARRAY';
        $event->details = 'TEST NEWS MODEL EQUALS ARRAY';
        $this->assertTrue($event->save());
        $this->assertTrue(!is_null($event->id) && $event->id > 0);

        $event->read($event->id);

        $a = $event->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($event->equalsArray($a));

        $a['id'] = 999999099;
        $this->assertFalse($event->equalsArray($a));

        $a['id'] = $event->id;
        $a['createDate'] = 'djksadkdsadsa';
        $this->assertFalse($event->equalsArray($a));
/*
        $a['createDate'] = $event->createDate;
        $a['updateDate'] = 'dsafgdgfgf';
        $this->assertFalse($event->equalsArray($a));

        $a['updateDate'] = $event->updateDate;
        $a['creator'] = 999999;
        $this->assertFalse($event->equalsArray($a));
 */
        $a['creator'] = $event->creator;
        $a['title'] = 'fkreree';
        $this->assertFalse($event->equalsArray($a));

        $a['title'] = $event->title;
        $a['details'] = 'fksfdsfds';
        $this->assertFalse($event->equalsArray($a));
    }

    public function testToArray()
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST TO ARRAY';
        $event->details = 'TEST NEWS MODEL TO ARRAY';
        $this->assertTrue($event->save());
        $this->assertTrue(!is_null($event->id) && $event->id > 0);

        $event->read($event->id);

        $a = $event->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($event->equalsArray($a));
    }

    public function testFromArray()
    {
        $event = new Event($this->pm);

        $a = array('id' => 1, 
                    'createDate' => '2014-01-01 00:00:01', 
//                    'updateDate' => '2001-01-01 00:00:01',
                    'creator' => 1,
                    'title' => 'TEST TO ARRAY',
                    'details' => 'TEST NEWS MODEL TO ARRAY');

        $event->fromArray($a);

        $this->assertTrue($event->equalsArray($a));
    }

    public function testToJson()
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST TO JSON';
        $event->details = 'TEST NEWS MODEL TO JSON';
        $this->assertTrue($event->save());
        $this->assertTrue(!is_null($event->id) && $event->id > 0);

        $event->read($event->id);

        $s = $event->toJson();
        $this->assertTrue(strlen(trim($s)) > 0);
        $a = json_decode($s, true);
        $this->assertTrue($event->equalsArray($a));
    }

    public function testFromJson()
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST FROM JSON';
        $event->details = 'TEST NEWS MODEL FROM JSON';
        $this->assertTrue($event->save());
        $this->assertTrue(!is_null($event->id) && $event->id > 0);

        $event->read($event->id);

        $s = $event->toJson();

        $test = new Event($this->pm);
        $test->fromJson($s);
        $this->assertTrue($event->equals($test));
    }

}

?>
