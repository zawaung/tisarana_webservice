<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/video.php');

/*
 *  Unit testing the Video class.
 */

class VideoModelTest extends PHPUnit_Framework_TestCase
{
    protected $pm = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

//
// TESTING THE VALIDATION OF ID
//
    public function testValidateId()
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'test.jpg';
        $video->description = 'TEST MODEL VALIDATE ID';
        
        if ($video->save()) {
            $video->read();
            $id = $video->id;

        // CHECK UNDEFINED OR NULL 
            $video->id = NULL;
            $video->validate();
            $a = $video->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NON NUMERIC 
            $video->id = 'a';
            $video->validate();
            $a = null;
            $a = $video->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NOT GREATER THAN ZERO
            $video->id = 0;
            $video->validate();
            $a = null;
            $a = $video->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NORMAL OR NOTHING WRONG CASE
            $video->id = $id;
            $video->validate();
            $a = null;
            $a = $video->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertFalse($found);
        }
    }

//
// TESTING THE VALIDATION OF TITLE
//
    public function testValidateTitle()
    {
    // CHECK UNDEFINED OR NULL 
        $video = new Video($this->pm);
        $video->validate();
        $a = $video->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'file') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK BLANK TITLE 
        $video = new Video($this->pm);
        $video->file = ' ';
        $video->validate();
        $a = $video->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'file') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $video->file = 'test.jpg';
        $video->validate();
        $a = null;
        $a = $video->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'file') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }
    
//
// TESTING THE VALIDATION OF DETAILS
//
    public function testValidateDescription()
    {
    // CHECK UNDEFINED OR NULL 
        $video = new Video($this->pm);
        $video->validate();
        $a = $video->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'description') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK BLANK DETAILS 
        $video = new Video($this->pm);
        $video->description = ' ';
        $video->validate();
        $a = $video->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'description') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $video->description = 'HELLO';
        $video->validate();
        $a = null;
        $a = $video->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'description') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }

//
// TEST SAVE
//
    public function testSaveVideo() 
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'test.png';
        $video->description = 'TEST NEWS MODEL SAVE';
        $this->assertTrue($video->save());
        $this->assertTrue(!is_null($video->id) && $video->id > 0);
    }

    public function testRead() 
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'test.jpg';
        $video->description = 'TEST NEWS MODEL READ';
        $this->assertTrue($video->save());
        $this->assertTrue(!is_null($video->id) && $video->id > 0);

        $test = new Video($this->pm);
        $test->id = $video->id;

        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $video->creator);
        $this->assertTrue($test->file == $video->file);
        $this->assertTrue($test->description == $video->description);
    }

    public function testSaveUpdate() 
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'test.jpg';
        $video->description = 'TEST NEWS MODEL';
        $this->assertTrue($video->save());
        $this->assertTrue(!is_null($video->id) && $video->id > 0);


        $video->file = 'test1.jpg';
        $video->description = 'TEST NEWS MODEL UPDATE';
        $video->save();

        $test = new Video($this->pm);
        $test->id = $video->id;
        $test->read();

        $this->assertTrue($test->file == $video->file);
        $this->assertTrue($test->description == $video->description);

        $test1 = new Video($this->pm);
        $test1->id = $video->id;
        $test1->read();

        $test1->file = 'test2.jpg';
        $test1->description = 'TEST NEWS MODEL UPDATE';
        $test1->save();

        $test = null;
        $test = new Video($this->pm);
        $test->id = $video->id;
        $test->read();

        $this->assertTrue($test->file == $test1->file);
        $this->assertTrue($test->description == $test1->description);

    }

    public function testDelete() 
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'test.jpg';
        $video->description = 'TEST NEWS MODEL DELETE';
        $this->assertTrue($video->save());
        $this->assertTrue(!is_null($video->id) && $video->id > 0);

        $video->delete();

        $this->assertTrue(is_null($video->id) || $video->id == 0);
        $this->assertTrue(is_null($video->createDate) || strlen(trim($video->createDate)) <= 0);
        $this->assertTrue(is_null($video->updateDate) || strlen(trim($video->updateDate)) <= 0);
        $this->assertTrue(is_null($video->creator) || $video->creator <= 0);
        $this->assertTrue(is_null($video->file) || strlen(trim($video->file)) <= 0);
        $this->assertTrue(is_null($video->description) || strlen(trim($video->description)) <= 0);
    }

    public function testEquals()
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'test.jpg';
        $video->description = 'TEST NEWS MODEL EQUALS ARRAY';
        $this->assertTrue($video->save());
        $this->assertTrue(!is_null($video->id) && $video->id > 0);

        $video->read($video->id);

        $test = clone $video;

        $this->assertTrue($video->equals($test));

        $test->id = 99943000;
        $this->assertFalse($video->equals($test));

        $test->id = $video->id;
        $test->createDate = 'djksadkdsadsa';
        $this->assertFalse($video->equals($test));

        $test->createDate = $video->createDate;
        $test->updateDate = 'dsafgdgfgf';
        $this->assertFalse($video->equals($test));

        $test->updateDate = $video->updateDate;
        $test->creator = 999999;
        $this->assertFalse($video->equals($test));

        $test->creator = $video->creator;
        $test->file = 'fkreree';
        $this->assertFalse($video->equals($test));

        $test->file = $video->file;
        $test->description = 'fksfdsfds';
        $this->assertFalse($video->equals($test));
    }

    public function testEqualsArray()
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'TEST EQUALS ARRAY';
        $video->description = 'TEST NEWS MODEL EQUALS ARRAY';
        $this->assertTrue($video->save());
        $this->assertTrue(!is_null($video->id) && $video->id > 0);

        $video->read($video->id);

        $a = $video->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($video->equalsArray($a));

        $a['id'] = 999999099;
        $this->assertFalse($video->equalsArray($a));

        $a['id'] = $video->id;
        $a['createDate'] = 'djksadkdsadsa';
        $this->assertFalse($video->equalsArray($a));

        $a['createDate'] = $video->createDate;
        $a['updateDate'] = 'dsafgdgfgf';
        $this->assertFalse($video->equalsArray($a));

        $a['updateDate'] = $video->updateDate;
        $a['creator'] = 999999;
        $this->assertFalse($video->equalsArray($a));

        $a['creator'] = $video->creator;
        $a['file'] = 'fkreree';
        $this->assertFalse($video->equalsArray($a));

        $a['file'] = $video->file;
        $a['description'] = 'fksfdsfds';
        $this->assertFalse($video->equalsArray($a));
    }

    public function testToArray()
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'test.png';
        $video->description = 'TEST NEWS MODEL TO ARRAY';
        $this->assertTrue($video->save());
        $this->assertTrue(!is_null($video->id) && $video->id > 0);

        $video->read($video->id);

        $a = $video->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($video->equalsArray($a));
    }

    public function testFromArray()
    {
        $video = new Video($this->pm);

        $a = array('id' => 1, 
                    'createDate' => '2014-01-01 00:00:01', 
                    'updateDate' => '2001-01-01 00:00:01',
                    'creator' => 1,
                    'file' => 'test.jpg',
                    'description' => 'TEST NEWS MODEL TO ARRAY');

        $video->fromArray($a);

        $this->assertTrue($video->equalsArray($a));
    }

    public function testToJson()
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'test.jpg';
        $video->description = 'TEST NEWS MODEL TO JSON';
        $this->assertTrue($video->save());
        $this->assertTrue(!is_null($video->id) && $video->id > 0);

        $video->read($video->id);

        $s = $video->toJson();
        $this->assertTrue(strlen(trim($s)) > 0);
        $a = json_decode($s, true);
        $this->assertTrue($video->equalsArray($a));
    }

    public function testFromJson()
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'test.jpg';
        $video->description = 'TEST NEWS MODEL FROM JSON';
        $this->assertTrue($video->save());
        $this->assertTrue(!is_null($video->id) && $video->id > 0);

        $video->read($video->id);

        $s = $video->toJson();

        $test = new Video($this->pm);
        $test->fromJson($s);
        $this->assertTrue($video->equals($test));
    }

}

?>
