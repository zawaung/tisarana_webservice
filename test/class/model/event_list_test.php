<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/event.php');
include_once(BASE_PATH . 'class/model/event_list.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');

class EventListTest extends PHPUnit_Framework_TestCase
{
    protected $pm = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

    public function testCount()
    {
        $nl = new EventList($this->pm);

        $na = array(); // event array

        for ($i=1; $i<=20; $i++) {
            $event = new Event($this->pm);
            $event->creator = 1;
            $event->title = "TEST NEWS LIST $i";
            $event->details = "TEST NEWS LIST COUNT $i";

            if ($event->save())
                $na[] = $event;

            $event = null;
        }

        $this->assertTrue(count($na) == 20);
        $this->assertTrue($nl->getCount() > 0);

        foreach($na as $a) {
            $a->delete();
        }
    }

    public function testGetList()
    {
        $nl = new EventList($this->pm);

        $na = array(); // event array

        for ($i=1; $i<=20; $i++) {
            $event = new Event($this->pm);
            $event->creator = 1;
            $event->title = "TEST NEWS LIST $i";
            $event->details = "TEST NEWS LIST COUNT $i";
    
            if ($event->save())
                $na[] = $event;

            $event = null;
        }

        $this->assertTrue(count($na) == 20);
        $this->assertTrue($nl->getList() >= 20);

        foreach($na as $a) {
            $a->delete();
        }
    }

    public function testAddList()
    {
        $nl = new EventList($this->pm);

        $na = array(20);

        for ($i=0; $i<20; $i++) {
            $na[$i] = new Event($this->pm);
            $na[$i]->creator = 1;
            $na[$i]->title = 'TEST ' . $i;
            $na[$i]->details = 'TEST NEWS LIST ADD LIST';
        }

        $id = $nl->addList($na);

        $this->assertTrue(is_array($id));
        $this->assertEquals(count($id), 20);

        for ($i=0; $i<20; $i++) {
            $test = new Event($this->pm);
            $test->id = $id[$i]; 
            $this->pm->read($test);

            $this->assertTrue($id[$i] == $test->id);
            $this->assertTrue($na[$i]->title == $test->title);
            $this->assertTrue($na[$i]->details == $test->details);

            $na[$i] = $test; // for deletion at the end
            $test = null;
        }

        foreach($na as $a) {
            $a->delete();
        }
    }
}

?>
