<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/news.php');
include_once(BASE_PATH . 'class/model/news_list.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');

class NewsListTest extends PHPUnit_Framework_TestCase
{
    protected $pm = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

    public function testCount()
    {
        $nl = new NewsList($this->pm);

        $na = array(); // news array

        for ($i=1; $i<=20; $i++) {
            $news = new News($this->pm);
            $news->creator = 1;
            $news->title = "TEST NEWS LIST $i";
            $news->details = "TEST NEWS LIST COUNT $i";

            if ($news->save())
                $na[] = $news;

            $news = null;
        }

        $this->assertTrue(count($na) == 20);
        $this->assertTrue($nl->getCount() > 0);

        foreach($na as $a) {
            $a->delete();
        }
    }

    public function testGetList()
    {
        $nl = new NewsList($this->pm);

        $na = array(); // news array

        for ($i=1; $i<=20; $i++) {
            $news = new News($this->pm);
            $news->creator = 1;
            $news->title = "TEST NEWS LIST $i";
            $news->details = "TEST NEWS LIST COUNT $i";
    
            if ($news->save())
                $na[] = $news;

            $news = null;
        }

        $this->assertTrue(count($na) == 20);
        $this->assertTrue($nl->getList() >= 20);

        foreach($na as $a) {
            $a->delete();
        }
    }

    public function testAddList()
    {
        $nl = new NewsList($this->pm);

        $na = array(20);

        for ($i=0; $i<20; $i++) {
            $na[$i] = new News($this->pm);
            $na[$i]->creator = 1;
            $na[$i]->title = 'TEST ' . $i;
            $na[$i]->details = 'TEST NEWS LIST ADD LIST';
        }

        $id = $nl->addList($na);

        $this->assertTrue(is_array($id));
        $this->assertEquals(count($id), 20);

        for ($i=0; $i<20; $i++) {
            $test = new News($this->pm);
            $test->id = $id[$i]; 
            $this->pm->read($test);

            $this->assertTrue($id[$i] == $test->id);
            $this->assertTrue($na[$i]->title == $test->title);
            $this->assertTrue($na[$i]->details == $test->details);

            $na[$i] = $test; // for deletion at the end
            $test = null;
        }

        foreach($na as $a) {
            $a->delete();
        }
    }
}

?>
