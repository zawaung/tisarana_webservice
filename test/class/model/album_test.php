<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/album.php');

/*
 *  Unit testing the Album class.
 */

class AlbumModelTest extends PHPUnit_Framework_TestCase
{
    protected $pm = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

//
// TESTING THE VALIDATION OF ID
//
    public function testValidateId()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST VALIDATE ID';
        $album->description = 'TEST MODEL VALIDATE ID';
        
        if ($album->save()) {
            $album->read();
            $id = $album->id;

        // CHECK UNDEFINED OR NULL 
            $album->id = NULL;
            $album->validate();
            $a = $album->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NON NUMERIC 
            $album->id = 'a';
            $album->validate();
            $a = null;
            $a = $album->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NOT GREATER THAN ZERO
            $album->id = 0;
            $album->validate();
            $a = null;
            $a = $album->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NORMAL OR NOTHING WRONG CASE
            $album->id = $id;
            $album->validate();
            $a = null;
            $a = $album->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertFalse($found);
        }
    }

//
// TESTING THE VALIDATION OF TITLE
//
    public function testValidateTitle()
    {
    // CHECK UNDEFINED OR NULL 
        $album = new Album($this->pm);
        $album->validate();
        $a = $album->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'title') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK BLANK TITLE 
        $album = new Album($this->pm);
        $album->title = ' ';
        $album->validate();
        $a = $album->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'title') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $album->title = 'HELLO';
        $album->validate();
        $a = null;
        $a = $album->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'title') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }
    
//
// TESTING THE VALIDATION OF DETAILS
//
    public function testValidateDescription()
    {
    // CHECK UNDEFINED OR NULL 
        $album = new Album($this->pm);
        $album->validate();
        $a = $album->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'description') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK BLANK DETAILS 
        $album = new Album($this->pm);
        $album->description = ' ';
        $album->validate();
        $a = $album->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'description') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $album->description = 'HELLO';
        $album->validate();
        $a = null;
        $a = $album->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'description') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }

//
// TEST SAVE
//
    public function testSaveAlbum() 
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST';
        $album->description = 'TEST NEWS MODEL SAVE';
        $this->assertTrue($album->save());
        $this->assertTrue(!is_null($album->id) && $album->id > 0);
    }

    public function testRead() 
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST';
        $album->description = 'TEST NEWS MODEL READ';
        $this->assertTrue($album->save());
        $this->assertTrue(!is_null($album->id) && $album->id > 0);

        $test = new Album($this->pm);
        $test->id = $album->id;

        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $album->creator);
        $this->assertTrue($test->title == $album->title);
        $this->assertTrue($test->description == $album->description);
    }

    public function testSaveUpdate() 
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST UPDATE';
        $album->description = 'TEST NEWS MODEL';
        $this->assertTrue($album->save());
        $this->assertTrue(!is_null($album->id) && $album->id > 0);


        $album->title = 'TEST UPDATE - 1';
        $album->description = 'TEST NEWS MODEL UPDATE';
        $album->save();

        $test = new Album($this->pm);
        $test->id = $album->id;
        $test->read();

        $this->assertTrue($test->title == $album->title);
        $this->assertTrue($test->description == $album->description);

        $test1 = new Album($this->pm);
        $test1->id = $album->id;
        $test1->read();

        $test1->title = 'TEST UPDATE - 2';
        $test1->description = 'TEST NEWS MODEL UPDATE';
        $test1->save();

        $test = null;
        $test = new Album($this->pm);
        $test->id = $album->id;
        $test->read();

        $this->assertTrue($test->title == $test1->title);
        $this->assertTrue($test->description == $test1->description);

    }

    public function testDelete() 
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST DELETE';
        $album->description = 'TEST NEWS MODEL DELETE';
        $this->assertTrue($album->save());
        $this->assertTrue(!is_null($album->id) && $album->id > 0);

        $album->delete();

        $this->assertTrue(is_null($album->id) || $album->id == 0);
        $this->assertTrue(is_null($album->createDate) || strlen(trim($album->createDate)) <= 0);
        $this->assertTrue(is_null($album->updateDate) || strlen(trim($album->updateDate)) <= 0);
        $this->assertTrue(is_null($album->creator) || $album->creator <= 0);
        $this->assertTrue(is_null($album->title) || strlen(trim($album->title)) <= 0);
        $this->assertTrue(is_null($album->description) || strlen(trim($album->description)) <= 0);
    }

    public function testEquals()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST EQUALS ARRAY';
        $album->description = 'TEST NEWS MODEL EQUALS ARRAY';
        $this->assertTrue($album->save());
        $this->assertTrue(!is_null($album->id) && $album->id > 0);

        $album->read($album->id);

        $test = clone $album;

        $this->assertTrue($album->equals($test));

        $test->id = 99943000;
        $this->assertFalse($album->equals($test));

        $test->id = $album->id;
        $test->createDate = 'djksadkdsadsa';
        $this->assertFalse($album->equals($test));

        $test->createDate = $album->createDate;
        $test->updateDate = 'dsafgdgfgf';
        $this->assertFalse($album->equals($test));

        $test->updateDate = $album->updateDate;
        $test->creator = 999999;
        $this->assertFalse($album->equals($test));

        $test->creator = $album->creator;
        $test->title = 'fkreree';
        $this->assertFalse($album->equals($test));

        $test->title = $album->title;
        $test->description = 'fksfdsfds';
        $this->assertFalse($album->equals($test));
    }

    public function testEqualsArray()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST EQUALS ARRAY';
        $album->description = 'TEST NEWS MODEL EQUALS ARRAY';
        $this->assertTrue($album->save());
        $this->assertTrue(!is_null($album->id) && $album->id > 0);

        $album->read($album->id);

        $a = $album->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($album->equalsArray($a));

        $a['id'] = 999999099;
        $this->assertFalse($album->equalsArray($a));

        $a['id'] = $album->id;
        $a['createDate'] = 'djksadkdsadsa';
        $this->assertFalse($album->equalsArray($a));

        $a['createDate'] = $album->createDate;
        $a['updateDate'] = 'dsafgdgfgf';
        $this->assertFalse($album->equalsArray($a));

        $a['updateDate'] = $album->updateDate;
        $a['creator'] = 999999;
        $this->assertFalse($album->equalsArray($a));

        $a['creator'] = $album->creator;
        $a['title'] = 'fkreree';
        $this->assertFalse($album->equalsArray($a));

        $a['title'] = $album->title;
        $a['description'] = 'fksfdsfds';
        $this->assertFalse($album->equalsArray($a));
    }

    public function testToArray()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST TO ARRAY';
        $album->description = 'TEST NEWS MODEL TO ARRAY';
        $this->assertTrue($album->save());
        $this->assertTrue(!is_null($album->id) && $album->id > 0);

        $album->read($album->id);

        $a = $album->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($album->equalsArray($a));
    }

    public function testFromArray()
    {
        $album = new Album($this->pm);

        $a = array('id' => 1, 
                    'createDate' => '2014-01-01 00:00:01', 
                    'updateDate' => '2001-01-01 00:00:01',
                    'creator' => 1,
                    'title' => 'TEST TO ARRAY',
                    'description' => 'TEST NEWS MODEL TO ARRAY');

        $album->fromArray($a);

        $this->assertTrue($album->equalsArray($a));
    }

    public function testToJson()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST TO JSON';
        $album->description = 'TEST NEWS MODEL TO JSON';
        $this->assertTrue($album->save());
        $this->assertTrue(!is_null($album->id) && $album->id > 0);

        $album->read($album->id);

        $s = $album->toJson();
        $this->assertTrue(strlen(trim($s)) > 0);
        $a = json_decode($s, true);
        $this->assertTrue($album->equalsArray($a));
    }

    public function testFromJson()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST FROM JSON';
        $album->description = 'TEST NEWS MODEL FROM JSON';
        $this->assertTrue($album->save());
        $this->assertTrue(!is_null($album->id) && $album->id > 0);

        $album->read($album->id);

        $s = $album->toJson();

        $test = new Album($this->pm);
        $test->fromJson($s);
        $this->assertTrue($album->equals($test));
    }

}

?>
