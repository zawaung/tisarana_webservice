<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/album.php');
include_once(BASE_PATH . 'class/model/album_list.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');

class AlbumListTest extends PHPUnit_Framework_TestCase
{
    protected $pm = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

    public function testCount()
    {
        $l = new AlbumList($this->pm);

        $na = array(); // album array

        for ($i=1; $i<=20; $i++) {
            $album = new Album($this->pm);
            $album->creator = 1;
            $album->title = "TEST LIST $i";
            $album->description = "TEST LIST COUNT $i";

            if ($album->save())
                $na[] = $album;

            $album = null;
        }

        $this->assertTrue(count($na) == 20);
        $this->assertTrue($l->getCount() > 0);

        foreach($na as $a) {
            $a->delete();
        }
    }

    public function testGetList()
    {
        $l = new AlbumList($this->pm);

        $na = array(); // album array

        for ($i=1; $i<=20; $i++) {
            $album = new Album($this->pm);
            $album->creator = 1;
            $album->title = "TEST LIST $i";
            $album->description = "TEST LIST COUNT $i";
    
            if ($album->save())
                $na[] = $album;

            $album = null;
        }

        $this->assertTrue(count($na) == 20);
        $this->assertTrue($l->getList() >= 20);

        foreach($na as $a) {
            $a->delete();
        }
    }

    public function testAddList()
    {
        $l = new AlbumList($this->pm);

        $na = array(20);

        for ($i=0; $i<20; $i++) {
            $na[$i] = new Album($this->pm);
            $na[$i]->creator = 1;
            $na[$i]->title = 'TEST ' . $i;
            $na[$i]->description = 'TEST LIST ADD LIST';
        }

        $id = $l->addList($na);

        $this->assertTrue(is_array($id));
        $this->assertEquals(count($id), 20);

        for ($i=0; $i<20; $i++) {
            $test = new Album($this->pm);
            $test->id = $id[$i]; 
            $this->pm->read($test);

            $this->assertTrue($id[$i] == $test->id);
            $this->assertTrue($na[$i]->title == $test->title);
            $this->assertTrue($na[$i]->description == $test->description);

            $na[$i] = $test; // for deletion at the end
            $test = null;
        }

        foreach($na as $a) {
            $a->delete();
        }
    }
}

?>
