<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/document.php');

/*
 *  Unit testing the Document class.
 */

class DocumentModelTest extends PHPUnit_Framework_TestCase
{
    protected $pm = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

//
// TESTING THE VALIDATION OF ID
//
    public function testValidateId()
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'test.pdf';
        $document->description = 'TEST MODEL VALIDATE ID';
        
        if ($document->save()) {
            $document->read();
            $id = $document->id;

        // CHECK UNDEFINED OR NULL 
            $document->id = NULL;
            $document->validate();
            $a = $document->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NON NUMERIC 
            $document->id = 'a';
            $document->validate();
            $a = null;
            $a = $document->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NOT GREATER THAN ZERO
            $document->id = 0;
            $document->validate();
            $a = null;
            $a = $document->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertTrue($found);

        // CHECK NORMAL OR NOTHING WRONG CASE
            $document->id = $id;
            $document->validate();
            $a = null;
            $a = $document->getError();
            $this->assertNotNull($a);

            $found = false;
            foreach ($a as $b) {
                if ($b['item'] == 'id') {
                    $found = true;
                    break;
                } 
            }

            $this->assertFalse($found);
        }
    }

//
// TESTING THE VALIDATION OF FILE
//
    public function testValidateFile()
    {
    // CHECK UNDEFINED OR NULL 
        $document = new Document($this->pm);
        $document->validate();
        $a = $document->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'file') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK BLANK TITLE 
        $document = new Document($this->pm);
        $document->file = ' ';
        $document->validate();
        $a = $document->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'file') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $document->file = 'test.pdf';
        $document->validate();
        $a = null;
        $a = $document->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'file') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }
    
//
// TESTING THE VALIDATION OF DESCRIPTION
//
    public function testValidateDescription()
    {
    // CHECK UNDEFINED OR NULL 
        $document = new Document($this->pm);
        $document->validate();
        $a = $document->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'description') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK BLANK DETAILS 
        $document = new Document($this->pm);
        $document->description = ' ';
        $document->validate();
        $a = $document->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'description') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $document->description = 'HELLO';
        $document->validate();
        $a = null;
        $a = $document->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'description') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }

//
// TEST SAVE
//
    public function testSaveDocument() 
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'test.pdf';
        $document->description = 'TEST NEWS MODEL SAVE';
        $this->assertTrue($document->save());
        $this->assertTrue(!is_null($document->id) && $document->id > 0);
    }

    public function testRead() 
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'test.pdf';
        $document->description = 'TEST NEWS MODEL READ';
        $this->assertTrue($document->save());
        $this->assertTrue(!is_null($document->id) && $document->id > 0);

        $test = new Document($this->pm);
        $test->id = $document->id;

        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $document->creator);
        $this->assertTrue($test->file == $document->file);
        $this->assertTrue($test->description == $document->description);
    }

    public function testSaveUpdate() 
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'test.pdf';
        $document->description = 'TEST NEWS MODEL';
        $this->assertTrue($document->save());
        $this->assertTrue(!is_null($document->id) && $document->id > 0);


        $document->file = 'test1.pdf';
        $document->description = 'TEST NEWS MODEL UPDATE';
        $document->save();

        $test = new Document($this->pm);
        $test->id = $document->id;
        $test->read();

        $this->assertTrue($test->file == $document->file);
        $this->assertTrue($test->description == $document->description);

        $test1 = new Document($this->pm);
        $test1->id = $document->id;
        $test1->read();

        $test1->file = 'test2.pdf';
        $test1->description = 'TEST NEWS MODEL UPDATE';
        $test1->save();

        $test = null;
        $test = new Document($this->pm);
        $test->id = $document->id;
        $test->read();

        $this->assertTrue($test->file == $test1->file);
        $this->assertTrue($test->description == $test1->description);

    }

    public function testDelete() 
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'test.pdf';
        $document->description = 'TEST NEWS MODEL DELETE';
        $this->assertTrue($document->save());
        $this->assertTrue(!is_null($document->id) && $document->id > 0);

        $document->delete();

        $this->assertTrue(is_null($document->id) || $document->id == 0);
        $this->assertTrue(is_null($document->createDate) || strlen(trim($document->createDate)) <= 0);
        $this->assertTrue(is_null($document->updateDate) || strlen(trim($document->updateDate)) <= 0);
        $this->assertTrue(is_null($document->creator) || $document->creator <= 0);
        $this->assertTrue(is_null($document->file) || strlen(trim($document->file)) <= 0);
        $this->assertTrue(is_null($document->description) || strlen(trim($document->description)) <= 0);
    }

    public function testEquals()
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'test.pdf';
        $document->description = 'TEST NEWS MODEL EQUALS ARRAY';
        $this->assertTrue($document->save());
        $this->assertTrue(!is_null($document->id) && $document->id > 0);

        $document->read($document->id);

        $test = clone $document;

        $this->assertTrue($document->equals($test));

        $test->id = 99943000;
        $this->assertFalse($document->equals($test));

        $test->id = $document->id;
        $test->createDate = 'djksadkdsadsa';
        $this->assertFalse($document->equals($test));

        $test->createDate = $document->createDate;
        $test->updateDate = 'dsafgdgfgf';
        $this->assertFalse($document->equals($test));

        $test->updateDate = $document->updateDate;
        $test->creator = 999999;
        $this->assertFalse($document->equals($test));

        $test->creator = $document->creator;
        $test->file = 'fkreree';
        $this->assertFalse($document->equals($test));

        $test->file = $document->file;
        $test->description = 'fksfdsfds';
        $this->assertFalse($document->equals($test));
    }

    public function testEqualsArray()
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'TEST EQUALS ARRAY';
        $document->description = 'TEST NEWS MODEL EQUALS ARRAY';
        $this->assertTrue($document->save());
        $this->assertTrue(!is_null($document->id) && $document->id > 0);

        $document->read($document->id);

        $a = $document->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($document->equalsArray($a));

        $a['id'] = 999999099;
        $this->assertFalse($document->equalsArray($a));

        $a['id'] = $document->id;
        $a['createDate'] = 'djksadkdsadsa';
        $this->assertFalse($document->equalsArray($a));

        $a['createDate'] = $document->createDate;
        $a['updateDate'] = 'dsafgdgfgf';
        $this->assertFalse($document->equalsArray($a));

        $a['updateDate'] = $document->updateDate;
        $a['creator'] = 999999;
        $this->assertFalse($document->equalsArray($a));

        $a['creator'] = $document->creator;
        $a['file'] = 'fkreree';
        $this->assertFalse($document->equalsArray($a));

        $a['file'] = $document->file;
        $a['description'] = 'fksfdsfds';
        $this->assertFalse($document->equalsArray($a));
    }

    public function testToArray()
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'test.pdf';
        $document->description = 'TEST NEWS MODEL TO ARRAY';
        $this->assertTrue($document->save());
        $this->assertTrue(!is_null($document->id) && $document->id > 0);

        $document->read($document->id);

        $a = $document->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($document->equalsArray($a));
    }

    public function testFromArray()
    {
        $document = new Document($this->pm);

        $a = array('id' => 1, 
                    'createDate' => '2014-01-01 00:00:01', 
                    'updateDate' => '2001-01-01 00:00:01',
                    'creator' => 1,
                    'file' => 'test.pdf',
                    'description' => 'TEST NEWS MODEL TO ARRAY');

        $document->fromArray($a);

        $this->assertTrue($document->equalsArray($a));
    }

    public function testToJson()
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'test.pdf';
        $document->description = 'TEST NEWS MODEL TO JSON';
        $this->assertTrue($document->save());
        $this->assertTrue(!is_null($document->id) && $document->id > 0);

        $document->read($document->id);

        $s = $document->toJson();
        $this->assertTrue(strlen(trim($s)) > 0);
        $a = json_decode($s, true);
        $this->assertTrue($document->equalsArray($a));
    }

    public function testFromJson()
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'test.pdf';
        $document->description = 'TEST NEWS MODEL FROM JSON';
        $this->assertTrue($document->save());
        $this->assertTrue(!is_null($document->id) && $document->id > 0);

        $document->read($document->id);

        $s = $document->toJson();

        $test = new Document($this->pm);
        $test->fromJson($s);
        $this->assertTrue($document->equals($test));
    }

}

?>
