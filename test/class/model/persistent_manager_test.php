<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/news.php');
include_once(BASE_PATH . 'class/model/event.php');
include_once(BASE_PATH . 'class/model/album.php');
include_once(BASE_PATH . 'class/model/photo.php');
include_once(BASE_PATH . 'class/model/video.php');
include_once(BASE_PATH . 'class/model/document.php');

/*
 *  Unit testing the persistent manager class.
 */

class PesistentManagerTest extends PHPUnit_Framework_TestCase
{
    protected $pm = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }
//
// TESTING FOR ADD
//

    //
    // ADD
    //

    // testing that calling add with null value result in ModelException
    public function testAddNullValue()
    {
        $this->setExpectedException('ModelException');
        $this->pm->add(NULL);
    }
   
    // testing that add work as expected for news
    public function testAddNews() 
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST';
        $news->details = 'TEST PM ADD NEWS';

        $id = $this->pm->add($news);

        $this->assertTrue($id > 0);

        $test = new News($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($news->title == $test->title);
        $this->assertTrue($news->details == $test->details);
    }

    // testing that add work as expected for event
    public function testAddEvent() 
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST';
        $event->details = 'TEST PM ADD EVENT';

        $id = $this->pm->add($event);

        $this->assertTrue($id > 0);

        $test = new Event($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($event->title == $test->title);
        $this->assertTrue($event->details == $test->details);
    }

    // testing that add work as expected for album
    public function testAddAlbum() 
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST';
        $album->details = 'TEST PM ADD ALBUM';

        $id = $this->pm->add($album);

        $this->assertTrue($id > 0);

        $test = new Album($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($album->title == $test->title);
        $this->assertTrue($album->description == $test->description);
    }

    // testing that add work as expected for photo
    public function testAddPhoto() 
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST';
        $album->details = 'TEST PM ADD PHOTO';
        $aid = $this->pm->add($album);
        $this->assertTrue($aid > 0);

        $album = NULL;

        $album = new Album($this->pm);
        $album->id = $aid;
        $this->assertTrue($album->read());
       
        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->description = 'TEST PM ADD PHOTO';
        $photo->album = $album;
        $photo->file = '1.png';

        $id = $this->pm->add($photo);

        $this->assertTrue($id > 0);

        $test = new Photo($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertEquals($id, $test->id);
        $this->assertEquals($photo->creator, $test->creator);
        $this->assertEquals($photo->album->id, $test->album->id);
        $this->assertEquals($photo->description, $test->description);
        $this->assertEquals($photo->file, $test->file);
    }

    public function testAddVideo() 
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'test.mp4';
        $video->description = 'TEST PM ADD VIDEO';

        $id = $this->pm->add($video);

        $this->assertTrue($id > 0);

        $test = new Video($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($video->file == $test->file);
        $this->assertTrue($video->description == $test->description);
    }

    public function testAddDocument() 
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'test.pdf';
        $document->description = 'TEST PM ADD DOCUMENT';

        $id = $this->pm->add($document);

        $this->assertTrue($id > 0);

        $test = new Document($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($document->file == $test->file);
        $this->assertTrue($document->description == $test->description);
    }

    //
    // ADD LIST
    //

    // testing that calling with null value result in ModelException
    public function testAddListNullValue()
    {
        $this->setExpectedException('ModelException');
        $this->pm->addList(NULL, 'News');
    }

    // testing that calling with non array value results in ModelException
    public function testAddListNonArray()
    {
        $this->setExpectedException('ModelException');
        $this->pm->addList(1, 'News');
    }

    // testing that calling with diffrent type in input array results in ModelException
    // e.g type is News but there is one object with type event or array etc.
    public function testAddDifferentType()
    {
        $this->setExpectedException('ModelException');
        $a = array(2);

        $a[0] = new News($this->pm);
        $a[0]->creator = 1;
        $a[0]->title = 'TEST ';
        $a[0]->details = 'TEST PM ADD LIST';

        $a[1] = array();

        $this->pm->addList($a, 'News');
    }

    // testing that add list work as expected in normal conditions
    public function testAddNewsList() 
    {
        $l = array(20);

        for ($i=0; $i<20; $i++) {
            $l[$i] = new News($this->pm);
            $l[$i]->creator = 1;
            $l[$i]->title = 'TEST ' . $i;
            $l[$i]->details = 'TEST PM ADD LIST';
        }

        $id = $this->pm->addList($l, 'News');

        $this->assertTrue(is_array($id));
        $this->assertEquals(count($id), 20);

        for ($i=0; $i<20; $i++) {
            $test = new News($this->pm);
            $test->id = $id[$i]; 
            $this->pm->read($test);

            $this->assertTrue($id[$i] == $test->id);
            $this->assertTrue($l[$i]->title == $test->title);
            $this->assertTrue($l[$i]->details == $test->details);

            $test = null;
        }
    }

    public function testAddEventList() 
    {
        $l = array(20);

        for ($i=0; $i<20; $i++) {
            $l[$i] = new Event($this->pm);
            $l[$i]->creator = 1;
            $l[$i]->title = 'TEST ' . $i;
            $l[$i]->details = 'TEST PM ADD LIST';
        }

        $id = $this->pm->addList($l, 'Event');

        $this->assertTrue(is_array($id));
        $this->assertEquals(count($id), 20);

        for ($i=0; $i<20; $i++) {
            $test = new Event($this->pm);
            $test->id = $id[$i]; 
            $this->pm->read($test);

            $this->assertTrue($id[$i] == $test->id);
            $this->assertTrue($l[$i]->title == $test->title);
            $this->assertTrue($l[$i]->details == $test->details);

            $test = null;
        }
    }

    public function testAddAlbumList() 
    {
        $l = array(20);

        for ($i=0; $i<20; $i++) {
            $l[$i] = new Album($this->pm);
            $l[$i]->creator = 1;
            $l[$i]->title = 'TEST ' . $i;
            $l[$i]->description = 'TEST PM ADD LIST';
        }

        $id = $this->pm->addList($l, 'Album');

        $this->assertTrue(is_array($id));
        $this->assertEquals(count($id), 20);

        for ($i=0; $i<20; $i++) {
            $test = new Album($this->pm);
            $test->id = $id[$i]; 
            $this->pm->read($test);

            $this->assertTrue($id[$i] == $test->id);
            $this->assertTrue($l[$i]->title == $test->title);
            $this->assertTrue($l[$i]->description == $test->description);

            $test = null;
        }
    }

    public function testAddPhotoList() 
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST';
        $album->details = 'TEST PM GET LIST PHOTO';
        $aid = $this->pm->add($album);
        $this->assertTrue($aid > 0);

        $album = NULL;

        $album = new Album($this->pm);
        $album->id = $aid;
        $this->assertTrue($album->read());

        $l = array(20);

        for ($i=0; $i<20; $i++) {
            $l[$i] = new Photo($this->pm);
            $l[$i]->creator = 1;
            $l[$i]->description = "TEST PM ADD LIST $i";
            $l[$i]->album = $album;
            $l[$i]->file = "$i.png";
        }

        $id = $this->pm->addList($l, 'Photo');

        $this->assertTrue(is_array($id));
        $this->assertEquals(count($id), 20);

        for ($i=0; $i<20; $i++) {
            $test = new Photo($this->pm);
            $test->id = $id[$i]; 
            $this->pm->read($test);

            $this->assertEquals($id[$i], $test->id);
            $this->assertEquals($l[$i]->creator, $test->creator);
            $this->assertEquals($l[$i]->album->id, $test->album->id);
            $this->assertEquals($l[$i]->description, $test->description);
            $this->assertEquals($l[$i]->file, $test->file);

            $test = null;
        }
    }

    public function testAddVideoList() 
    {
        $l = array(20);

        for ($i=0; $i<20; $i++) {
            $l[$i] = new Video($this->pm);
            $l[$i]->creator = 1;
            $l[$i]->file = 'test' . $i . '.mp4';
            $l[$i]->description = 'TEST PM ADD LIST';
        }

        $id = $this->pm->addList($l, 'Video');

        $this->assertTrue(is_array($id));
        $this->assertEquals(count($id), 20);

        for ($i=0; $i<20; $i++) {
            $test = new Video($this->pm);
            $test->id = $id[$i]; 
            $this->pm->read($test);

            $this->assertTrue($id[$i] == $test->id);
            $this->assertTrue($l[$i]->file == $test->file);
            $this->assertTrue($l[$i]->description == $test->description);

            $test = null;
        }
    }

    public function testAddDocumentList() 
    {
        $l = array(20);

        for ($i=0; $i<20; $i++) {
            $l[$i] = new Document($this->pm);
            $l[$i]->creator = 1;
            $l[$i]->file = 'test' . $i . '.pdf';
            $l[$i]->description = 'TEST PM ADD LIST';
        }

        $id = $this->pm->addList($l, 'Document');

        $this->assertTrue(is_array($id));
        $this->assertEquals(count($id), 20);

        for ($i=0; $i<20; $i++) {
            $test = new Document($this->pm);
            $test->id = $id[$i]; 
            $this->pm->read($test);

            $this->assertTrue($id[$i] == $test->id);
            $this->assertTrue($l[$i]->file == $test->file);
            $this->assertTrue($l[$i]->description == $test->description);

            $test = null;
        }
    }

//
// TESTING FOR READ
//
    public function testReadNullValue()
    {
        $this->setExpectedException('ModelException');
        $this->pm->read(NULL);
    }

    public function testReadNullId()
    {
        $this->setExpectedException('ModelException');
        $news = new News($this->pm);
        $this->pm->read($news);
    }

    public function testReadNonNumericId()
    {
        $this->setExpectedException('ModelException');
        $news = new News($this->pm);
        $news->id = 'a';
        $this->pm->read($news);
    }

    public function testReadZeroId()
    {
        $this->setExpectedException('ModelException');
        $news = new News($this->pm);
        $news->id = 0;
        $this->pm->read($news);
    }

    public function testReadNonExistentId()
    {
        $obj = new News($this->pm);
        $obj->id = 9999999999999999;
        $this->assertFalse($this->pm->read($obj));

        $obj = NULL;
        $obj = new Event($this->pm);
        $obj->id = 9999999999999999;
        $this->assertFalse($this->pm->read($obj));

        $obj = NULL;
        $obj = new Album($this->pm);
        $obj->id = 9999999999999999;
        $this->assertFalse($this->pm->read($obj));
    }

    public function testReadNews()
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST';
        $news->details = 'TEST PM READ';
        $id = $this->pm->add($news);
        
        $test = new News($this->pm);
        $test->id = $id; 

        $this->assertTrue($this->pm->read($test)); 
        $this->assertTrue($id == $test->id);
        $this->assertTrue($news->title == $test->title);
        $this->assertTrue($news->details == $test->details);
    } 

    public function testReadEvent()
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST';
        $event->details = 'TEST PM READ';
        $id = $this->pm->add($event);
        
        $test = new Event($this->pm);
        $test->id = $id; 

        $this->assertTrue($this->pm->read($test)); 
        $this->assertTrue($id == $test->id);
        $this->assertTrue($event->title == $test->title);
        $this->assertTrue($event->details == $test->details);
    } 

    public function testReadAlbum()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST';
        $album->description = 'TEST PM READ';
        $id = $this->pm->add($album);
        
        $test = new Album($this->pm);
        $test->id = $id; 

        $this->assertTrue($this->pm->read($test)); 
        $this->assertTrue($id == $test->id);
        $this->assertTrue($album->title == $test->title);
        $this->assertTrue($album->description == $test->description);
    } 

    public function testReadPhoto() 
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST';
        $album->details = 'TEST PM READ PHOTO';
        $aid = $this->pm->add($album);
        $this->assertTrue($aid > 0);

        $album = NULL;

        $album = new Album($this->pm);
        $album->id = $aid;
        $this->assertTrue($album->read());
       
        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->description = 'TEST PM READ PHOTO';
        $photo->album = $album;
        $photo->file = '1.png';

        $id = $this->pm->add($photo);

        $this->assertTrue($id > 0);

        $test = new Photo($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertEquals($id, $test->id);
        $this->assertEquals($photo->creator, $test->creator);
        $this->assertEquals($photo->album->id, $test->album->id);
        $this->assertEquals($photo->description, $test->description);
        $this->assertEquals($photo->file, $test->file);
    }

    public function testReadVideo()
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'test.mp4';
        $video->description = 'TEST PM READ';
        $id = $this->pm->add($video);
        
        $test = new Video($this->pm);
        $test->id = $id; 

        $this->assertTrue($this->pm->read($test)); 
        $this->assertTrue($id == $test->id);
        $this->assertTrue($video->file == $test->file);
        $this->assertTrue($video->description == $test->description);
    } 

    public function testReadDocument()
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'test.pdf';
        $document->description = 'TEST PM READ';
        $id = $this->pm->add($document);
        
        $test = new Document($this->pm);
        $test->id = $id; 

        $this->assertTrue($this->pm->read($test)); 
        $this->assertTrue($id == $test->id);
        $this->assertTrue($document->file == $test->file);
        $this->assertTrue($document->description == $test->description);
    } 

//
// TESTING FOR DELETE
//
    public function testDeleteNullValue()
    {
        $this->setExpectedException('ModelException');
        $this->pm->delete(NULL);
    }

    public function testDeleteNullId()
    {
        $this->setExpectedException('ModelException');
        $news = new News($this->pm);
        $this->pm->delete($news);
    }

    public function testDeleteNonNumericId()
    {
        $this->setExpectedException('ModelException');
        $news = new News($this->pm);
        $news->id = 'a';
        $this->pm->delete($news);
    }

    public function testDeleteZeroId()
    {
        $this->setExpectedException('ModelException');
        $news = new News($this->pm);
        $news->id = 0;
        $this->pm->delete($news);
    }

    public function testDeleteNews()
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST';
        $news->details = 'TEST PM DELETE';
        $id = $this->pm->add($news);
        
        $test = new News($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($news->title == $test->title);
        $this->assertTrue($news->details == $test->details);

        $this->assertTrue($this->pm->delete($test));
        $this->assertFalse($this->pm->read($test));
    }

    public function testDeleteEvent()
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST';
        $event->details = 'TEST PM DELETE';
        $id = $this->pm->add($event);
        
        $test = new Event($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($event->title == $test->title);
        $this->assertTrue($event->details == $test->details);

        $this->assertTrue($this->pm->delete($test));
        $this->assertFalse($this->pm->read($test));
    }

    public function testDeleteAlbum()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST';
        $album->description = 'TEST PM DELETE';
        $id = $this->pm->add($album);
        
        $test = new Album($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($album->title == $test->title);
        $this->assertTrue($album->description == $test->description);

        $this->assertTrue($this->pm->delete($test));
        $this->assertFalse($this->pm->read($test));
    }

    public function testDeletePhoto() 
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST';
        $album->details = 'TEST PM DELETE PHOTO';
        $aid = $this->pm->add($album);
        $this->assertTrue($aid > 0);

        $album = NULL;

        $album = new Album($this->pm);
        $album->id = $aid;
        $this->assertTrue($album->read());
       
        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->description = 'TEST PM DELETE PHOTO';
        $photo->album = $album;
        $photo->file = '1.png';

        $id = $this->pm->add($photo);

        $this->assertTrue($id > 0);

        $test = new Photo($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertEquals($id, $test->id);
        $this->assertEquals($photo->creator, $test->creator);
        $this->assertEquals($photo->album->id, $test->album->id);
        $this->assertEquals($photo->description, $test->description);
        $this->assertEquals($photo->file, $test->file);

        $this->assertTrue($this->pm->delete($test));
        $this->assertFalse($this->pm->read($test));
    }

    public function testDeleteVideo()
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'test.mp4';
        $video->description = 'TEST PM DELETE';
        $id = $this->pm->add($video);
        
        $test = new Video($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($video->file == $test->file);
        $this->assertTrue($video->description == $test->description);

        $this->assertTrue($this->pm->delete($test));
        $this->assertFalse($this->pm->read($test));
    }

    public function testDeleteDocument()
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'test.pdf';
        $document->description = 'TEST PM DELETE';
        $id = $this->pm->add($document);
        
        $test = new Document($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($document->file == $test->file);
        $this->assertTrue($document->description == $test->description);

        $this->assertTrue($this->pm->delete($test));
        $this->assertFalse($this->pm->read($test));
    }

//
// TESTING FOR UPDATE
//
    public function testUpdateNullValue()
    {
        $this->setExpectedException('ModelException');
        $this->pm->update(NULL);
    }

    public function testUpdateNullId()
    {
        $this->setExpectedException('ModelException');
        $news = new News($this->pm);
        $this->pm->update($news);
    }

    public function testUpdateNonNumericId()
    {
        $this->setExpectedException('ModelException');
        $news = new News($this->pm);
        $news->id = 'a';
        $this->pm->update($news);
    }

    public function testUpdateZeroId()
    {
        $this->setExpectedException('ModelException');
        $news = new News($this->pm);
        $news->id = 0;
        $this->pm->update($news);
    }

    public function testUpdateNews()
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'what';
        $news->details = 'Whatever!';
        $id = $this->pm->add($news);

        $news->id = $id;
        $news->creator = 2;
        $news->title = 'TEST';
        $news->details = 'TEST UPDATE';

        $this->pm->update($news);

        $test = new News($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($news->title == $test->title);
        $this->assertTrue($news->details == $test->details);
    }

    public function testUpdateEvent()
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'what';
        $event->details = 'Whatever!';
        $id = $this->pm->add($event);

        $event->id = $id;
        $event->creator = 2;
        $event->title = 'TEST';
        $event->details = 'TEST UPDATE';

        $this->pm->update($event);

        $test = new Event($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($event->title == $test->title);
        $this->assertTrue($event->details == $test->details);
    }

    public function testUpdateAlbum()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'what';
        $album->description = 'Whatever!';
        $id = $this->pm->add($album);

        $album->id = $id;
        $album->creator = 2;
        $album->title = 'TEST';
        $album->description = 'TEST UPDATE';

        $this->pm->update($album);

        $test = new Album($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($album->title == $test->title);
        $this->assertTrue($album->description == $test->description);
    }

    public function testUpdatePhoto()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST';
        $album->details = 'TEST PM UPDATE PHOTO';
        $aid = $this->pm->add($album);
        $this->assertTrue($aid > 0);

        $album = NULL;

        $album = new Album($this->pm);
        $album->id = $aid;
        $this->assertTrue($album->read());
       
        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->description = 'Whatever';
        $photo->album = $album;
        $photo->file = '1.png';

        $id = $this->pm->add($photo);

        $this->assertTrue($id > 0);

        $photo->id = $id;
        $photo->creator = 2;
        $photo->description = 'TEST UPDATE';
        $photo->file = 'test.png';

        $this->pm->update($photo);

        $test = new Photo($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($photo->creator == $test->creator);
        $this->assertTrue($photo->description == $test->description);
        $this->assertTrue($photo->file == $test->file);       
    }

    public function testUpdateVideo()
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'what.mp4';
        $video->description = 'Whatever!';
        $id = $this->pm->add($video);

        $video->id = $id;
        $video->creator = 2;
        $video->file = 'test.mp4';
        $video->description = 'TEST UPDATE';

        $this->pm->update($video);

        $test = new Video($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($video->file == $test->file);
        $this->assertTrue($video->description == $test->description);
    }

    public function testUpdateDocument()
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'what.pdf';
        $document->description = 'Whatever!';
        $id = $this->pm->add($document);

        $document->id = $id;
        $document->creator = 2;
        $document->file = 'test.pdf';
        $document->description = 'TEST UPDATE';

        $this->pm->update($document);

        $test = new Document($this->pm);
        $test->id = $id; 
        $this->pm->read($test);

        $this->assertTrue($id == $test->id);
        $this->assertTrue($document->file == $test->file);
        $this->assertTrue($document->description == $test->description);
    }
   
//
// TESTING FOR COUNT
//

    public function testCountNullObject()
    {
        $this->setExpectedException('ModelException');
        $this->pm->getCount(null); 
    }

    public function testCountNews()
    {
        $news = new News($this->pm);
        $news->creator = 1;
        $news->title = 'TEST';
        $news->details = 'TEST PM COUNT';
        $id = $this->pm->add($news);

        $cnt = $this->pm->getCount('News');
        $this->assertTrue($cnt > 0);
    }

    public function testCountEvent()
    {
        $event = new Event($this->pm);
        $event->creator = 1;
        $event->title = 'TEST';
        $event->details = 'TEST PM COUNT';
        $id = $this->pm->add($event);

        $cnt = $this->pm->getCount('Event');
        $this->assertTrue($cnt > 0);
    }

    public function testCountAlbum()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST';
        $album->description = 'TEST PM COUNT';
        $id = $this->pm->add($album);

        $cnt = $this->pm->getCount('Album');
        $this->assertTrue($cnt > 0);
    }

    public function testCountPhoto()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST';
        $album->details = 'TEST PM COUNT PHOTO';
        $aid = $this->pm->add($album);
        $this->assertTrue($aid > 0);

        $album = NULL;

        $album = new Album($this->pm);
        $album->id = $aid;
        $this->assertTrue($album->read());
       
        $photo = new Photo($this->pm);
        $photo->creator = 1;
        $photo->description = 'TEST PM COUNT';
        $photo->album = $album;
        $photo->file = '1.mp4';

        $id = $this->pm->add($photo);

        $this->assertTrue($id > 0);

        $cnt = $this->pm->getCount('Photo');
        $this->assertTrue($cnt > 0);
    }

    public function testCountVideo()
    {
        $video = new Video($this->pm);
        $video->creator = 1;
        $video->file = 'test.mp4';
        $video->description = 'TEST PM COUNT';
        $id = $this->pm->add($video);

        $cnt = $this->pm->getCount('Video');
        $this->assertTrue($cnt > 0);
    }

    public function testCountDocument()
    {
        $document = new Document($this->pm);
        $document->creator = 1;
        $document->file = 'test.pdf';
        $document->description = 'TEST PM COUNT';
        $id = $this->pm->add($document);

        $cnt = $this->pm->getCount('Document');
        $this->assertTrue($cnt > 0);
    }
   
//
// TESTING FOR GETLIST
//

    public function testGetListInvalidCurrentPage()
    {
        $this->setExpectedException('ModelException');
        $a = $this->pm->getList(NULL, 20, 'News');
    }

    public function testGetListInvalidPageSize()
    {
        $this->setExpectedException('ModelException');
        $a = $this->pm->getList(1, NULL, 'News');
    }
   
    public function testGetListNews()
    {
        $id = array();

        for ($i=1; $i <= 20; $i++) {
            $news = new News($this->pm);
            $news->creator = 1;
            $news->title = "TEST $i";
            $news->details = "TEST PM GET LIST $i";
            $id[] = $this->pm->add($news);
        }
        
        $a = $this->pm->getList(1, 20, 'News');
        $this->assertTrue(count($a) >= 20);
    }
 
    public function testGetListEvent()
    {
        $id = array();

        for ($i=1; $i <= 20; $i++) {
            $event = new Event($this->pm);
            $event->creator = 1;
            $event->title = "TEST $i";
            $event->details = "TEST PM GET LIST $i";
            $id[] = $this->pm->add($event);
        }
        
        $a = $this->pm->getList(1, 20, 'Event');
        $this->assertTrue(count($a) >= 20);
    }

    public function testGetListAlbum()
    {
        $id = array();

        for ($i=1; $i <= 20; $i++) {
            $album = new Album($this->pm);
            $album->creator = 1;
            $album->title = "TEST $i";
            $album->description = "TEST PM GET LIST $i";
            $id[] = $this->pm->add($album);
        }
        
        $a = $this->pm->getList(1, 20, 'Album');
        $this->assertTrue(count($a) >= 20);
    }

    public function testGetListPhoto()
    {
        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = 'TEST';
        $album->details = 'TEST PM GET LIST PHOTO';
        $aid = $this->pm->add($album);
        $this->assertTrue($aid > 0);

        $album = NULL;

        $album = new Album($this->pm);
        $album->id = $aid;
        $this->assertTrue($album->read());

        $id = array();

        for ($i=1; $i <= 20; $i++) {
            $photo = new Photo($this->pm);
            $photo->creator = 1;
            $photo->description = "TEST PM GET LIST $i";
            $photo->album = $album;
            $photo->file = "$i.mp4";
            $id[] = $this->pm->add($photo);
        }

        $a = $this->pm->getList(1, 20, 'Photo');
        $this->assertTrue(count($a) >= 20);
    }

    public function testGetListVideo()
    {
        $id = array();

        for ($i=1; $i <= 20; $i++) {
            $video = new Video($this->pm);
            $video->creator = 1;
            $video->file = "test$i.mp4";
            $video->description = "TEST PM GET LIST $i";
            $id[] = $this->pm->add($video);
        }
        
        $a = $this->pm->getList(1, 20, 'Video');
        $this->assertTrue(count($a) >= 20);
    }

    public function testGetListDocument()
    {
        $id = array();

        for ($i=1; $i <= 20; $i++) {
            $document = new Document($this->pm);
            $document->creator = 1;
            $document->file = "test$i.pdf";
            $document->description = "TEST PM GET LIST $i";
            $id[] = $this->pm->add($document);
        }
        
        $a = $this->pm->getList(1, 20, 'Document');
        $this->assertTrue(count($a) >= 20);
    }
 
}

?>
