<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/model_list.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');
include_once(BASE_PATH . 'class/model/news.php');

class MyModelList extends ModelList
{
    // to test things getCount shoud be working but it is an abstract function
    // and we need to realise it here. Put public $count so that testing functions
    // can set it.
    public $count = 0;

    public function getCount()
    {
        return $this->count;
    }

    public function getList() 
    {  
        return array();
    }
}

class ModelListTest extends PHPUnit_Framework_TestCase
{
    protected $pm = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

    public function testConstruct()
    {
        $this->setExpectedException('ModelException');
        $model = new MyModelList($this);
    }

    public function testPageSize()
    {
        // even if we set null. non-numeric and less than zero
        // values set function should automatically set default
        $mml = new MyModelList($this->pm);
        $mml->setPageSize(null);
        $this->assertEquals($mml->getPageSize(), ModelList::DEFAULT_PAGE_SIZE);
        $mml->setPageSize('a');
        $this->assertEquals($mml->getPageSize(), ModelList::DEFAULT_PAGE_SIZE);
        $mml->setPageSize(array());
        $this->assertEquals($mml->getPageSize(), ModelList::DEFAULT_PAGE_SIZE);
        $mml->setPageSize(-1);
        $this->assertEquals($mml->getPageSize(), ModelList::DEFAULT_PAGE_SIZE);

        // test when normal value is set
        $mml->setPageSize(5);
        $this->assertEquals($mml->getPageSize(), 5);
    }

    public function testCurrentPage()
    {
        // even if we set null. non-numeric and less than zero
        // values set function should automatically set default
            $mml = new MyModelList($this->pm);
            $mml->setCurrentPage(null);
            $this->assertEquals($mml->getCurrentPage(), 0);
            $mml->setCurrentPage('a');
            $this->assertEquals($mml->getCurrentPage(), 0);
            $mml->setCurrentPage(array());
            $this->assertEquals($mml->getCurrentPage(), 0);
            $mml->setCurrentPage(-1);
            $this->assertEquals($mml->getCurrentPage(), 0);

        // when we set current page greater than total page
        // current page should automatically align with total page
            $cnt = 10;
            $nl = array($cnt);

            for ($i=0; $i<$cnt; $i++) {
                $nl[$i] = new News($this->pm);
                $nl[$i]->creator = 1;
                $nl[$i]->title = 'TEST ' . $i;
                $nl[$i]->details = 'TEST PM ADD LIST';
            }

            $this->pm->addList($nl, 'News');

            $mml->count = $cnt;       
            $mml->setPageSize(2);

            $mml->setCurrentPage($cnt + 10);
            $this->assertEquals($mml->getCurrentPage(), $mml->getTotalPage());

        // test when normal value is set
            $mml->setCurrentPage(3);
            $this->assertEquals($mml->getCurrentPage(), 3);
    }
}

?>
