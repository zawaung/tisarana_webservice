<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/document.php');
include_once(BASE_PATH . 'class/model/document_list.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');

class DocumentListTest extends PHPUnit_Framework_TestCase
{
    protected $pm = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

    public function testCount()
    {
        $l = new DocumentList($this->pm);

        $na = array(); // document array

        for ($i=1; $i<=20; $i++) {
            $document = new Document($this->pm);
            $document->creator = 1;
            $document->file = "test$i.pdf";
            $document->description = "TEST LIST COUNT $i";

            if ($document->save())
                $na[] = $document;

            $document = null;
        }

        $this->assertTrue(count($na) == 20);
        $this->assertTrue($l->getCount() > 0);

        foreach($na as $a) {
            $a->delete();
        }
    }

    public function testGetList()
    {
        $l = new DocumentList($this->pm);

        $na = array(); // document array

        for ($i=1; $i<=20; $i++) {
            $document = new Document($this->pm);
            $document->creator = 1;
            $document->file = "test$i.pdf";
            $document->description = "TEST LIST COUNT $i";
    
            if ($document->save())
                $na[] = $document;

            $document = null;
        }

        $this->assertTrue(count($na) == 20);
        $this->assertTrue($l->getList() >= 20);

        foreach($na as $a) {
            $a->delete();
        }
    }

    public function testAddList()
    {
        $l = new DocumentList($this->pm);

        $na = array(20);

        for ($i=0; $i<20; $i++) {
            $na[$i] = new Document($this->pm);
            $na[$i]->creator = 1;
            $na[$i]->file = "test$i.pdf";
            $na[$i]->description = 'TEST LIST ADD LIST';
        }

        $id = $l->addList($na);

        $this->assertTrue(is_array($id));
        $this->assertEquals(count($id), 20);

        for ($i=0; $i<20; $i++) {
            $test = new Document($this->pm);
            $test->id = $id[$i]; 
            $this->pm->read($test);

            $this->assertTrue($id[$i] == $test->id);
            $this->assertTrue($na[$i]->file == $test->file);
            $this->assertTrue($na[$i]->description == $test->description);

            $na[$i] = $test; // for deletion at the end
            $test = null;
        }

        foreach($na as $a) {
            $a->delete();
        }
    }
}

?>
