<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/album.php');
include_once(BASE_PATH . 'class/model/photo.php');
include_once(BASE_PATH . 'class/model/photo_list.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');

class PhotoListTest extends PHPUnit_Framework_TestCase
{
    protected $pm = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

    public function testCount()
    {
        $l = new PhotoList($this->pm);

        $na = array(); 

        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = "TEST LIST PHOTO";
        $album->description = "TEST LIST COUNT PHOTO";
        $album->save();

        for ($i=1; $i<=20; $i++) {
            $photo = new Photo($this->pm);
            $photo->creator = 1;
            $photo->file = 'test.jpg';
            $photo->album = $album;
            $photo->description = 'TEST LIST COUNT';
            if ($photo->save()) 
                $na[] = $photo;

            $photo = null;
        }

        $this->assertTrue(count($na) == 20);
        $this->assertTrue($l->getCount() > 0);

        foreach($na as $a) {
            $a->delete();
        }
    }

    public function testGetList()
    {
        $l = new PhotoList($this->pm);

        $na = array(); 

        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = "TEST LIST PHOTO";
        $album->description = "TEST LIST PHOTO GETLIST";
        $album->save();

        for ($i=1; $i<=20; $i++) {
            $photo = new Photo($this->pm);
            $photo->creator = 1;
            $photo->file = 'test.jpg';
            $photo->album = $album;
            $photo->description = 'TEST LIST GETLIST';
            if ($photo->save()) 
                $na[] = $photo;

            $photo = null;
        }

        $this->assertTrue(count($na) == 20);
        $this->assertTrue($l->getList() >= 20);

        foreach($na as $a) {
            $a->delete();
        }
    }

    public function testAddList()
    {
        $l = new PhotoList($this->pm);

        $na = array(20);

        $album = new Album($this->pm);
        $album->creator = 1;
        $album->title = "TEST LIST PHOTO";
        $album->description = "TEST ADD LIST PHOTO";
        $album->save();

        for ($i=0; $i<20; $i++) {
            $na[$i] = new Photo($this->pm);
            $na[$i]->creator = 1;
            $na[$i]->file = 'test.jpg';
            $na[$i]->album = $album;
            $na[$i]->description = 'TEST LIST ADD LIST';
        }

        $id = $l->addList($na);

        $this->assertTrue(is_array($id));
        $this->assertEquals(count($id), 20);

        for ($i=0; $i<20; $i++) {
            $test = new Photo($this->pm);
            $test->id = $id[$i]; 
            $this->pm->read($test);

            $this->assertEquals($id[$i], $test->id);
            $this->assertEquals($na[$i]->creator, $test->creator);
            $this->assertEquals($na[$i]->description, $test->description);
            $this->assertEquals($na[$i]->file, $test->file);

            $na[$i] = $test; // for deletion at the end
            $test = null;
        }

        foreach($na as $a) {
            $a->delete();
        }
    }
}

?>
