<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'class/model/model.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');

class MyModel extends Model
{
    protected $a;
    protected $b;

    public function validate(){}
    public function read(){}
    public function save(){}
    public function delete(){}
    public function toArray(){}
    public function fromArray($a){}
    public function toJson(){}
    public function fromJson($json){}
    public function equals($model){}
    public function equalsArray($a){}

// cannot test addError and clearError functions because they are protected, and thus,
// created a public functions here just to test them
    public function ae($a, $b)
    {
        $this->addError($a, $b);
    }

    public function ce()
    {
        $this->clearError();
    }
}

class ModelTest extends PHPUnit_Framework_TestCase
{
    public function testConstruct()
    {
        $this->setExpectedException('ModelException');
        $model = new MyModel($this);
    }

    /*
     * In the model class _set and _get functions are implemented so that
     * child class won't need to implement gettes and setters for each property.
     * Here, we are testing this set and get things work.
     *
     */
    public function testSetGet()
    {
        $model = new MyModel(new PersistentManager());

        $model->a = 1;
        $model->b = 'TEST';

        $this->assertTrue($model->a == 1);
        $this->assertTrue($model->b == 'TEST');
    }

    public function testErrorArray()
    {
        $model = new MyModel(new PersistentManager());
        
        $this->assertNotNull($model);
        $this->assertTrue(is_array($model->getError()));

        $model->ae('id', 'HELLO');
        $a = $model->getError();
        $this->assertTrue(count($a) > 0);
        $this->assertTrue('id' == $a[0]['item']);
        $this->assertTrue('HELLO' == $a[0]['error']);

        $model->ce();
        $this->assertTrue(count($model->getError()) <= 0);
    }
}

?>
