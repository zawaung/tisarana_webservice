<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/test_model.php');
include_once(BASE_PATH . 'test/class/model/security/action_double.php');
include_once(BASE_PATH . 'test/class/model/security/object_double.php');

/*
 *  Unit testing the Action class.
 */

class ActionModelTest extends TestModel
{
//
// TESTING THE VALIDATION OF ID
//
    public function testValidate()
    {
        $this->validationObject = new ActionDouble($this->pm);
        $this->validationAttributes = 
            array('id' => '',
                    'name' => 'name',
                    'object' => mt_rand());
        $this->_testValidate(); 
    }

//
// TEST SAVE
//
    public function testSaveAction() 
    {
        $action = new Action($this->pm);
        $action->creator = mt_rand();
        $action->name = 'TEST ACTION'. mt_rand();
        $action->object = $this->addObject();
        $this->assertTrue($action->save());
        $this->assertTrue(!is_null($action->id) && $action->id > 0);
    }

    public function testRead() 
    {
        $action = new Action($this->pm);
        $action->creator = mt_rand();
        $action->name = 'TEST ACTION'. mt_rand();
        $action->object = $this->addObject();
        $this->assertTrue($action->save());
        $this->assertTrue(!is_null($action->id) && $action->id > 0);

        $test = new Action($this->pm);
        $test->id = $action->id;

        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $action->creator);
        $this->assertTrue($test->name == $action->name);
        $this->assertTrue($test->object == $action->object);
    }

    public function testSaveUpdate() 
    {
        $action = new Action($this->pm);
        $action->creator = 1;
        $action->name = 'TEST ACTION'. mt_rand();
        $action->object = $this->addObject();
        $this->assertTrue($action->save());
        $this->assertTrue(!is_null($action->id) && $action->id > 0);

        $action->name = 'TEST ACTION'. mt_rand();

        $action->save();

        $test = new Action($this->pm);
        $test->id = $action->id;
        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $action->creator);
        $this->assertTrue($test->name == $action->name);
        $this->assertTrue($test->object == $action->object);
    }

    public function testDelete() 
    {
        $action = new Action($this->pm);
        $action->creator = 1;
        $action->name = 'TEST ACTION'. mt_rand();
        $action->object = $this->addObject();
        $this->assertTrue($action->save());
        $this->assertTrue(!is_null($action->id) && $action->id > 0);

        $action->delete();

        $this->assertTrue(is_null($action->id) || $action->id == 0);
        $this->assertTrue(is_null($action->createDate) || strlen(trim($action->createDate)) <= 0);
        $this->assertTrue(is_null($action->updateDate) || strlen(trim($action->updateDate)) <= 0);
        $this->assertTrue(is_null($action->creator) || $action->creator <= 0);
        $this->assertTrue(is_null($action->name) || strlen(trim($action->name)) <= 0);
        $this->assertTrue(is_null($action->object) || $action->object <= 0);
    }

    public function testEquals()
    {
        $action = new Action($this->pm);
        $action->creator = mt_rand();
        $action->object = $this->addObject();
        $action->name = 'TEST ACTION'. mt_rand();
        $this->assertTrue($action->save());
        $this->assertTrue(!is_null($action->id) && $action->id > 0);

        $action->read($action->id);

        $test = clone $action;

        $this->assertTrue($action->equals($test));

        $test->id = 99943000;
        $this->assertFalse($action->equals($test));

        $test->id = $action->id;
        $test->createDate = 'djksadkdsadsa';
        $this->assertFalse($action->equals($test));

        $test->createDate = $action->createDate;
        $test->updateDate = 'dsafgdgfgf';
        $this->assertFalse($action->equals($test));

        $test->updateDate = $action->updateDate;
        $test->creator = 999999;
        $this->assertFalse($action->equals($test));

        $test->creator = $action->creator;
        $test->name = 'fkreree';
        $this->assertFalse($action->equals($test));

        $test->name = $action->name;
        $test->object = 99999;
        $this->assertFalse($action->equals($test));
    }

    public function testEqualsArray()
    {
        $action = new Action($this->pm);
        $action->creator = mt_rand();
        $action->object = $this->addObject();
        $action->name = 'TEST ACTION'. mt_rand();
        $this->assertTrue($action->save());
        $this->assertTrue(!is_null($action->id) && $action->id > 0);

        $action->read($action->id);

        $a = $action->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($action->equalsArray($a));

        $a['id'] = 999999099;
        $this->assertFalse($action->equalsArray($a));

        $a['id'] = $action->id;
        $a['createDate'] = 'djksadkdsadsa';
        $this->assertFalse($action->equalsArray($a));

        $a['createDate'] = $action->createDate;
        $a['updateDate'] = 'dsafgdgfgf';
        $this->assertFalse($action->equalsArray($a));

        $a['updateDate'] = $action->updateDate;
        $a['creator'] = 999999;
        $this->assertFalse($action->equalsArray($a));

        $a['creator'] = $action->creator;
        $a['name'] = 'fkreree';
        $this->assertFalse($action->equalsArray($a));

        $a['name'] = $action->name;
        $a['object'] = 9999999;
        $this->assertFalse($action->equalsArray($a));
    }

    public function testToArray()
    {
        $action = new Action($this->pm);
        $action->creator = mt_rand();
        $action->object = $this->addObject();
        $action->name = 'TEST ACTION'. mt_rand();
        $this->assertTrue($action->save());
        $this->assertTrue(!is_null($action->id) && $action->id > 0);

        $action->read($action->id);

        $a = $action->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($action->equalsArray($a));
    }

    public function testFromArray()
    {
        $action = new Action($this->pm);

        $temp = mt_rand();

        $a = array('id' => 1, 
                    'createDate' => '2014-01-01 00:00:01', 
                    'updateDate' => '2001-01-01 00:00:01',
                    'creator' => mt_rand(),
                    'name' => 'name',
                    'object' => $this->addObject());

        $action->fromArray($a);

        $this->assertTrue($action->equalsArray($a));
    }

    public function testToJson()
    {
        $action = new Action($this->pm);
        $action->creator = mt_rand();
        $action->object = $this->addObject();
        $action->name = 'TEST ACTION'. mt_rand();
        $this->assertTrue($action->save());
        $this->assertTrue(!is_null($action->id) && $action->id > 0);

        $action->read($action->id);

        $s = $action->toJson();
        $this->assertTrue(strlen(trim($s)) > 0);
        $a = json_decode($s, true);
        $this->assertTrue($action->equalsArray($a));
    }

    public function testFromJson()
    {
        $action = new Action($this->pm);
        $action->creator = mt_rand();
        $action->name = 'TEST ACTION'. mt_rand();
        $action->object = $this->addObject();
        $this->assertTrue($action->save());
        $this->assertTrue(!is_null($action->id) && $action->id > 0);

        $action->read($action->id);

        $s = $action->toJson();

        $test = new Action($this->pm);
        $test->fromJson($s);
        $this->assertTrue($action->equals($test));
    }
 
    private function addObject()
    {
        $object = new Object($this->pm);
        $object->creator = 1;
        $object->name = 'TEST OBJECT'. mt_rand();
        $this->assertTrue($object->save());
        $this->assertTrue(!is_null($object->id) && $object->id > 0);

        return $object->id;
    }
}

?>
