<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/test_model.php');
include_once(BASE_PATH . 'class/model/security/acl.php');
include_once(BASE_PATH . 'class/model/security/group.php');
include_once(BASE_PATH . 'class/model/security/user.php');
include_once(BASE_PATH . 'class/model/security/object.php');
include_once(BASE_PATH . 'class/model/security/action.php');

/*
 *  Unit testing the Acl class.
 */

class AclModelTest extends TestModel
{
//
// TESTING THE VALIDATION OF ID
//
    public function testValidate()
    {
        $this->validationObject = new Acl($this->pm);
        $this->validationAttributes = 
            array('principal' => 1,
                    'action' => 1);
        $this->_testValidate(); 
    }

//
// TEST SAVE
//
    public function testSaveAcl() 
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addUser();
        $acl->action = $this->addAction();
        $acl->principal_type = 'U';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'U');
    }

    public function testSaveAclG() 
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addGroup();
        $acl->action = $this->addAction();
        $acl->principal_type = 'G';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'G');
    }

    public function testRead() 
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addUser();
        $acl->action = $this->addAction();
        $acl->principal_type = 'U';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'U');

        $test = new Acl($this->pm);
        $test->principal = $acl->principal;
        $test->action = $acl->action;
        $test->principal_type = $acl->principal_type;

        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $acl->creator);
        $this->assertTrue($test->principal == $acl->principal);
        $this->assertTrue($test->action == $acl->action);
        $this->assertTrue($test->principal_type == $acl->principal_type);
    }

    public function testReadG() 
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addGroup();
        $acl->action = $this->addAction();
        $acl->principal_type = 'G';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'G');

        $test = new Acl($this->pm);
        $test->principal = $acl->principal;
        $test->action = $acl->action;
        $test->principal_type = $acl->principal_type;

        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $acl->creator);
        $this->assertTrue($test->principal == $acl->principal);
        $this->assertTrue($test->action == $acl->action);
        $this->assertTrue($test->principal_type == $acl->principal_type);
    }

    //
    // NOTE: There is no update function for acl because this is just 
    //       a linking of principal(group/user) and action. We add a new record if we want to
    //       enable an action for a group or user. We delete the record for disabling.
    //

    public function testDelete() 
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addUser();
        $acl->action = $this->addAction();
        $acl->principal_type = 'U';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'U');

        $acl->delete();

        $this->assertTrue(is_null($acl->createDate) || strlen(trim($acl->createDate)) <= 0);
        $this->assertTrue(is_null($acl->creator) || $acl->creator <= 0);
        $this->assertTrue(is_null($acl->principal) || $acl->principal <= 0);
        $this->assertTrue(is_null($acl->action) || $acl->action <= 0);
        $this->assertTrue(is_null($acl->principal_type) || strlen(trim($acl->principal_type)) <= 0);
    }

    public function testDeleteG() 
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addGroup();
        $acl->action = $this->addAction();
        $acl->principal_type = 'G';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'G');

        $acl->delete();

        $this->assertTrue(is_null($acl->createDate) || strlen(trim($acl->createDate)) <= 0);
        $this->assertTrue(is_null($acl->creator) || $acl->creator <= 0);
        $this->assertTrue(is_null($acl->principal) || $acl->principal <= 0);
        $this->assertTrue(is_null($acl->action) || $acl->action <= 0);
        $this->assertTrue(is_null($acl->principal_type) || strlen(trim($acl->principal_type)) <= 0);
    }

    public function testEquals()
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addUser();
        $acl->action = $this->addAction();
        $acl->principal_type = 'U';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'U');

        $test = clone $acl;

        $this->assertTrue($acl->equals($test));

        $test->createDate = 'djksadkdsadsa';
        $this->assertFalse($acl->equals($test));

        $test->createDate = $acl->createDate;
        $test->creator = 999999;
        $this->assertFalse($acl->equals($test));

        $test->creator = $acl->creator;
        $test->principal = 9999999;
        $this->assertFalse($acl->equals($test));

        $test->principal = $acl->principal;
        $test->action = 9999999;
        $this->assertFalse($acl->equals($test));

        $test->action = $acl->action;
        $test->principal_type = 'G';
        $this->assertFalse($acl->equals($test));
    }

    public function testEqualsG()
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addGroup();
        $acl->action = $this->addAction();
        $acl->principal_type = 'G';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'G');

        $test = clone $acl;

        $this->assertTrue($acl->equals($test));

        $test->createDate = 'djksadkdsadsa';
        $this->assertFalse($acl->equals($test));

        $test->createDate = $acl->createDate;
        $test->creator = 999999;
        $this->assertFalse($acl->equals($test));

        $test->creator = $acl->creator;
        $test->principal = 9999999;
        $this->assertFalse($acl->equals($test));

        $test->principal = $acl->principal;
        $test->action = 9999999;
        $this->assertFalse($acl->equals($test));

        $test->action = $acl->action;
        $test->principal_type = 'U';
        $this->assertFalse($acl->equals($test));
    }

    public function testEqualsArray()
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addUser();
        $acl->action = $this->addAction();
        $acl->principal_type = 'U';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'U');

        $a = $acl->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($acl->equalsArray($a));

        $a['createDate'] = 'djksadkdsadsa';
        $this->assertFalse($acl->equalsArray($a));

        $a['createDate'] = $acl->createDate;
        $a['creator'] = 999999;
        $this->assertFalse($acl->equalsArray($a));

        $a['creator'] = $acl->creator;
        $a['principal'] = 99999999;
        $this->assertFalse($acl->equalsArray($a));

        $a['principal'] = $acl->principal;
        $a['action'] = 99999999;
        $this->assertFalse($acl->equalsArray($a));

        $a['action'] = $acl->action;
        $a['principal_type'] = 'G';
        $this->assertFalse($acl->equalsArray($a));
    }

    public function testEqualsArrayG()
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addGroup();
        $acl->action = $this->addAction();
        $acl->principal_type = 'G';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'G');

        $a = $acl->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($acl->equalsArray($a));

        $a['createDate'] = 'djksadkdsadsa';
        $this->assertFalse($acl->equalsArray($a));

        $a['createDate'] = $acl->createDate;
        $a['creator'] = 999999;
        $this->assertFalse($acl->equalsArray($a));

        $a['creator'] = $acl->creator;
        $a['principal'] = 99999999;
        $this->assertFalse($acl->equalsArray($a));

        $a['principal'] = $acl->principal;
        $a['action'] = 99999999;
        $this->assertFalse($acl->equalsArray($a));

        $a['action'] = $acl->action;
        $a['principal_type'] = 'U';
        $this->assertFalse($acl->equalsArray($a));
    }

    public function testToArray()
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addUser();
        $acl->action = $this->addAction();
        $acl->principal_type = 'U';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'U');

        $a = $acl->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($acl->equalsArray($a));
    }

    public function testToArrayG()
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addGroup();
        $acl->action = $this->addAction();
        $acl->principal_type = 'G';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'G');

        $a = $acl->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($acl->equalsArray($a));
    }

    public function testFromArray()
    {
        $acl = new Acl($this->pm);

        $temp = mt_rand();

        $a = array('createDate' => '2014-01-01 00:00:01', 
                    'creator' => 1,
                    'principal' => $this->addUser(),
                    'action' => $this->addAction(),
                    'principal_type' => 'U');

        $acl->fromArray($a);

        $this->assertTrue($acl->equalsArray($a));
    }

    public function testFromArrayG()
    {
        $acl = new Acl($this->pm);

        $temp = mt_rand();

        $a = array('createDate' => '2014-01-01 00:00:01', 
                    'creator' => 1,
                    'principal' => $this->addGroup(),
                    'action' => $this->addAction(),
                    'principal_type' => 'G');

        $acl->fromArray($a);

        $this->assertTrue($acl->equalsArray($a));
    }

    public function testToJson()
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addUser();
        $acl->action = $this->addAction();
        $acl->principal_type = 'U';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'U');

        $s = $acl->toJson();
        $this->assertTrue(strlen(trim($s)) > 0);
        $a = json_decode($s, true);
        $this->assertTrue($acl->equalsArray($a));
    }

    public function testToJsonG()
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addGroup();
        $acl->action = $this->addAction();
        $acl->principal_type = 'G';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'G');

        $s = $acl->toJson();
        $this->assertTrue(strlen(trim($s)) > 0);
        $a = json_decode($s, true);
        $this->assertTrue($acl->equalsArray($a));
    }

    public function testFromJson()
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addUser();
        $acl->action = $this->addAction();
        $acl->principal_type = 'U';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'U');

        $s = $acl->toJson();

        $test = new Acl($this->pm);
        $test->fromJson($s);
        $this->assertTrue($acl->equals($test));
    }

    public function testFromJsonG()
    {
        $acl = new Acl($this->pm);
        $acl->creator = 1;
        $acl->principal = $this->addGroup();
        $acl->action = $this->addAction();
        $acl->principal_type = 'G';
        $this->assertTrue($acl->save());
        $this->assertTrue(!is_null($acl->principal) && $acl->principal > 0);
        $this->assertTrue(!is_null($acl->action) && $acl->action > 0);
        $this->assertTrue(!is_null($acl->principal_type) && $acl->principal_type == 'G');

        $s = $acl->toJson();

        $test = new Acl($this->pm);
        $test->fromJson($s);
        $this->assertTrue($acl->equals($test));
    }
 
    private function addGroup()
    {
        $group = new Group($this->pm);
        $group->creator = 1;
        $group->name = 'TESTACL '. mt_rand();
        $this->assertTrue($group->save());
        $this->assertTrue(!is_null($group->id) && $group->id > 0);

        return $group->id;
    }    

    private function addUser()
    {
        $user = new User($this->pm);
        $user->creator = 1;
        $temp = mt_rand();
        $user->firstName = 'FIRST ' . $temp;
        $user->lastName = 'TESTACL';
        $user->email = $temp . '@tisarana.org.au';
        $user->loginName = 'login' . $temp;
        $user->password = 'password';
        $this->assertTrue($user->save());
        $this->assertTrue(!is_null($user->id) && $user->id > 0);

        return $user->id;
    }

    private function addAction()
    {
        $action = new Action($this->pm);
        $action->creator = mt_rand();
        $action->name = 'TESTACL '. mt_rand();
        $action->object = $this->addObject();
        $this->assertTrue($action->save());
        $this->assertTrue(!is_null($action->id) && $action->id > 0);

        return $action->id;
    }

    private function addObject()
    {
        $object = new Object($this->pm);
        $object->creator = 1;
        $object->name = 'TESTACL '. mt_rand();
        $this->assertTrue($object->save());
        $this->assertTrue(!is_null($object->id) && $object->id > 0);

        return $object->id;
    }
}

?>
