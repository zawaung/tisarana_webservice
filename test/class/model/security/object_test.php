<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/test_model.php');
include_once(BASE_PATH . 'test/class/model/security/object_double.php');

/*
 *  Unit testing the Object class.
 */

class ObjectModelTest extends TestModel
{
//
// TESTING THE VALIDATION OF ID
//
    public function testValidate()
    {
        $this->validationObject = new ObjectDouble($this->pm);
        $this->validationAttributes = 
            array('id' => '',
                    'name' => 'name');
        $this->_testValidate(); 
    }

//
// TEST SAVE
//
    public function testSaveObject() 
    {
        $object = new Object($this->pm);
        $object->creator = 1;
        $object->name = 'TEST OBJECT'. mt_rand();
        $this->assertTrue($object->save());
        $this->assertTrue(!is_null($object->id) && $object->id > 0);
    }

    public function testRead() 
    {
        $object = new Object($this->pm);
        $object->creator = 1;
        $object->name = 'TEST OBJECT'. mt_rand();
        $this->assertTrue($object->save());
        $this->assertTrue(!is_null($object->id) && $object->id > 0);

        $test = new Object($this->pm);
        $test->id = $object->id;

        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $object->creator);
        $this->assertTrue($test->name == $object->name);
    }

    public function testSaveUpdate() 
    {
        $object = new Object($this->pm);
        $object->creator = 1;
        $object->name = 'TEST OBJECT'. mt_rand();
        $this->assertTrue($object->save());
        $this->assertTrue(!is_null($object->id) && $object->id > 0);

        $object->name = 'TEST OBJECT'. mt_rand();

        $object->save();

        $test = new Object($this->pm);
        $test->id = $object->id;
        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $object->creator);
        $this->assertTrue($test->name == $object->name);
    }

    public function testDelete() 
    {
        $object = new Object($this->pm);
        $object->creator = 1;
        $object->name = 'TEST OBJECT'. mt_rand();
        $this->assertTrue($object->save());
        $this->assertTrue(!is_null($object->id) && $object->id > 0);

        $object->delete();

        $this->assertTrue(is_null($object->id) || $object->id == 0);
        $this->assertTrue(is_null($object->createDate) || strlen(trim($object->createDate)) <= 0);
        $this->assertTrue(is_null($object->updateDate) || strlen(trim($object->updateDate)) <= 0);
        $this->assertTrue(is_null($object->creator) || $object->creator <= 0);
        $this->assertTrue(is_null($object->name) || strlen(trim($object->name)) <= 0);
    }

    public function testEquals()
    {
        $object = new Object($this->pm);
        $object->creator = 1;
        $object->name = 'TEST OBJECT'. mt_rand();
        $this->assertTrue($object->save());
        $this->assertTrue(!is_null($object->id) && $object->id > 0);

        $object->read($object->id);

        $test = clone $object;

        $this->assertTrue($object->equals($test));

        $test->id = 99943000;
        $this->assertFalse($object->equals($test));

        $test->id = $object->id;
        $test->createDate = 'djksadkdsadsa';
        $this->assertFalse($object->equals($test));

        $test->createDate = $object->createDate;
        $test->updateDate = 'dsafgdgfgf';
        $this->assertFalse($object->equals($test));

        $test->updateDate = $object->updateDate;
        $test->creator = 999999;
        $this->assertFalse($object->equals($test));

        $test->creator = $object->creator;
        $test->name = 'fkreree';
        $this->assertFalse($object->equals($test));
    }

    public function testEqualsArray()
    {
        $object = new Object($this->pm);
        $object->creator = 1;
        $object->name = 'TEST OBJECT'. mt_rand();
        $this->assertTrue($object->save());
        $this->assertTrue(!is_null($object->id) && $object->id > 0);

        $object->read($object->id);

        $a = $object->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($object->equalsArray($a));

        $a['id'] = 999999099;
        $this->assertFalse($object->equalsArray($a));

        $a['id'] = $object->id;
        $a['createDate'] = 'djksadkdsadsa';
        $this->assertFalse($object->equalsArray($a));

        $a['createDate'] = $object->createDate;
        $a['updateDate'] = 'dsafgdgfgf';
        $this->assertFalse($object->equalsArray($a));

        $a['updateDate'] = $object->updateDate;
        $a['creator'] = 999999;
        $this->assertFalse($object->equalsArray($a));

        $a['creator'] = $object->creator;
        $a['name'] = 'fkreree';
        $this->assertFalse($object->equalsArray($a));
    }

    public function testToArray()
    {
        $object = new Object($this->pm);
        $object->creator = 1;
        $object->name = 'TEST OBJECT'. mt_rand();
        $this->assertTrue($object->save());
        $this->assertTrue(!is_null($object->id) && $object->id > 0);

        $object->read($object->id);

        $a = $object->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($object->equalsArray($a));
    }

    public function testFromArray()
    {
        $object = new Object($this->pm);

        $temp = mt_rand();

        $a = array('id' => 1, 
                    'createDate' => '2014-01-01 00:00:01', 
                    'updateDate' => '2001-01-01 00:00:01',
                    'creator' => 1,
                    'name' => 'name');

        $object->fromArray($a);

        $this->assertTrue($object->equalsArray($a));
    }

    public function testToJson()
    {
        $object = new Object($this->pm);
        $object->creator = 1;
        $object->name = 'TEST OBJECT'. mt_rand();
        $this->assertTrue($object->save());
        $this->assertTrue(!is_null($object->id) && $object->id > 0);

        $object->read($object->id);

        $s = $object->toJson();
        $this->assertTrue(strlen(trim($s)) > 0);
        $a = json_decode($s, true);
        $this->assertTrue($object->equalsArray($a));
    }

    public function testFromJson()
    {
        $object = new Object($this->pm);
        $object->creator = 1;
        $object->name = 'TEST OBJECT'. mt_rand();
        $this->assertTrue($object->save());
        $this->assertTrue(!is_null($object->id) && $object->id > 0);

        $object->read($object->id);

        $s = $object->toJson();

        $test = new Object($this->pm);
        $test->fromJson($s);
        $this->assertTrue($object->equals($test));
    }
}

?>
