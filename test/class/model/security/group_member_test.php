<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/test_model.php');
//include_once(BASE_PATH . 'test/class/model/security/group_member.php');
include_once(BASE_PATH . 'class/model/security/group_member.php');

/*
 *  Unit testing the GroupMember class.
 */

class GroupMemberModelTest extends TestModel
{
//
// TESTING THE VALIDATION OF ID
//
    public function testValidate()
    {
        $this->validationObject = new GroupMember($this->pm);
        $this->validationAttributes = 
            array('group_id' => 1,
                    'user_id' => 1);
        $this->_testValidate(); 
    }

//
// TEST SAVE
//
    public function testSaveGroupMember() 
    {
        $group_member = new GroupMember($this->pm);
        $group_member->creator = 1;
        $group_member->group_id = mt_rand();
        $group_member->user_id = mt_rand();
        $this->assertTrue($group_member->save());
        $this->assertTrue(!is_null($group_member->group_id) && $group_member->group_id > 0);
        $this->assertTrue(!is_null($group_member->user_id) && $group_member->user_id > 0);
    }

    public function testRead() 
    {
        $group_member = new GroupMember($this->pm);
        $group_member->creator = 1;
        $group_member->group_id = mt_rand();
        $group_member->user_id = mt_rand();
        $this->assertTrue($group_member->save());
        $this->assertTrue(!is_null($group_member->group_id) && $group_member->group_id > 0);
        $this->assertTrue(!is_null($group_member->user_id) && $group_member->user_id > 0);

        $test = new GroupMember($this->pm);
        $test->group_id = $group_member->group_id;
        $test->user_id = $group_member->user_id;

        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $group_member->creator);
        $this->assertTrue($test->group_id == $group_member->group_id);
        $this->assertTrue($test->user_id == $group_member->user_id);
    }

    //
    // NOTE: There is no update function for group member because this is just 
    //       a linking of group and member. We add a new record if we want to
    //       add a user to a group and we delete the record if we are to remove
    //       a user from a group.
    //

    public function testDelete() 
    {
        $group_member = new GroupMember($this->pm);
        $group_member->creator = 1;
        $group_member->group_id = mt_rand();
        $group_member->user_id = mt_rand();
        $this->assertTrue($group_member->save());
        $this->assertTrue(!is_null($group_member->group_id) && $group_member->group_id > 0);
        $this->assertTrue(!is_null($group_member->user_id) && $group_member->user_id > 0);

        $group_member->delete();

        $this->assertTrue(is_null($group_member->createDate) || strlen(trim($group_member->createDate)) <= 0);
        $this->assertTrue(is_null($group_member->creator) || $group_member->creator <= 0);
        $this->assertTrue(is_null($group_member->group_id) || $group_member->group_id <= 0);
        $this->assertTrue(is_null($group_member->user_id) || $group_member->user_id <= 0);
    }

    public function testEquals()
    {
        $group_member = new GroupMember($this->pm);
        $group_member->creator = 1;
        $group_member->group_id = mt_rand();
        $group_member->user_id = mt_rand();
        $this->assertTrue($group_member->save());
        $this->assertTrue(!is_null($group_member->group_id) && $group_member->group_id > 0);
        $this->assertTrue(!is_null($group_member->user_id) && $group_member->user_id > 0);

        $test = clone $group_member;

        $this->assertTrue($group_member->equals($test));

        $test->createDate = 'djksadkdsadsa';
        $this->assertFalse($group_member->equals($test));

        $test->createDate = $group_member->createDate;
        $test->creator = 999999;
        $this->assertFalse($group_member->equals($test));

        $test->creator = $group_member->creator;
        $test->group_id = 9999999;
        $this->assertFalse($group_member->equals($test));

        $test->group_id = $group_member->group_id;
        $test->user_id = 9999999;
        $this->assertFalse($group_member->equals($test));
    }

    public function testEqualsArray()
    {
        $group_member = new GroupMember($this->pm);
        $group_member->creator = 1;
        $group_member->group_id = mt_rand();
        $group_member->user_id = mt_rand();
        $this->assertTrue($group_member->save());
        $this->assertTrue(!is_null($group_member->group_id) && $group_member->group_id > 0);
        $this->assertTrue(!is_null($group_member->user_id) && $group_member->user_id > 0);

        $a = $group_member->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($group_member->equalsArray($a));

        $a['createDate'] = 'djksadkdsadsa';
        $this->assertFalse($group_member->equalsArray($a));

        $a['createDate'] = $group_member->createDate;
        $a['creator'] = 999999;
        $this->assertFalse($group_member->equalsArray($a));

        $a['creator'] = $group_member->creator;
        $a['group_id'] = 99999999;
        $this->assertFalse($group_member->equalsArray($a));

        $a['group_id'] = $group_member->group_id;
        $a['user_id'] = 99999999;
        $this->assertFalse($group_member->equalsArray($a));
    }

    public function testToArray()
    {
        $group_member = new GroupMember($this->pm);
        $group_member->creator = 1;
        $group_member->group_id = mt_rand();
        $group_member->user_id = mt_rand();
        $this->assertTrue($group_member->save());
        $this->assertTrue(!is_null($group_member->group_id) && $group_member->group_id > 0);
        $this->assertTrue(!is_null($group_member->user_id) && $group_member->user_id > 0);

        $a = $group_member->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($group_member->equalsArray($a));
    }

    public function testFromArray()
    {
        $group_member = new GroupMember($this->pm);

        $temp = mt_rand();

        $a = array('createDate' => '2014-01-01 00:00:01', 
                    'creator' => 1,
                    'group_id' => mt_rand(),
                    'user_id' => mt_rand());

        $group_member->fromArray($a);

        $this->assertTrue($group_member->equalsArray($a));
    }

    public function testToJson()
    {
        $group_member = new GroupMember($this->pm);
        $group_member->creator = 1;
        $group_member->group_id = mt_rand();
        $group_member->user_id = mt_rand();
        $this->assertTrue($group_member->save());
        $this->assertTrue(!is_null($group_member->group_id) && $group_member->group_id > 0);
        $this->assertTrue(!is_null($group_member->user_id) && $group_member->user_id > 0);

        $s = $group_member->toJson();
        $this->assertTrue(strlen(trim($s)) > 0);
        $a = json_decode($s, true);
        $this->assertTrue($group_member->equalsArray($a));
    }

    public function testFromJson()
    {
        $group_member = new GroupMember($this->pm);
        $group_member->creator = 1;
        $group_member->group_id = mt_rand();
        $group_member->user_id = mt_rand();
        $this->assertTrue($group_member->save());
        $this->assertTrue(!is_null($group_member->group_id) && $group_member->group_id > 0);
        $this->assertTrue(!is_null($group_member->user_id) && $group_member->user_id > 0);

        $s = $group_member->toJson();

        $test = new GroupMember($this->pm);
        $test->fromJson($s);
        $this->assertTrue($group_member->equals($test));
    }
}

?>
