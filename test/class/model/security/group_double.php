<?php

include_once(BASE_PATH . 'class/model/security/group.php');
include_once(BASE_PATH . 'class/model/model.php');

class GroupDouble extends Group
{
    public $state = Model::STATE_NORMAL; 
}

/*
 * NOTE: In the validate function of the model, the ID property is not checked when state 
 *       is not new. When we are testing the validate function of the model we do not want
 *       to create a new one saved in the database everytime we test. We need a way to change
 *       the state property after creating a new model object. However, state property is 
 *       protected and we can not change it from test object. For this reason, we create a 
 *       mock double object and make the state property public.
 */
?>
