<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/test_model.php');
include_once(BASE_PATH . 'test/class/model/security/user_double.php');

/*
 *  Unit testing the User class.
 */

class UserModelTest extends TestModel
{
//
// TESTING THE VALIDATION OF ID
//
    public function testValidate()
    {
        $this->validationObject = new UserDouble($this->pm);
        $this->validationAttributes = 
            array('id' => '',
                'firstName' => 'FIRST', 
                'lastName' => 'LAST',
                'loginName' => 'login',
                'password' => 'password');
        $this->_testValidate(); 
    }

//
// TEST SAVE
//
    public function testSaveUser() 
    {
        $user = new User($this->pm);
        $user->creator = 1;
        $user->firstName = 'FIRST';
        $user->lastName = 'TEST';
        $temp = mt_rand();
        $user->email = $temp . '@tisarana.org.au';
        $user->loginName = 'login' . $temp;
        $user->password = 'password';
        $this->assertTrue($user->save());
        $this->assertTrue(!is_null($user->id) && $user->id > 0);
    }

    public function testRead() 
    {
        $user = new User($this->pm);
        $user->creator = 1;
        $user->firstName = 'FIRST';
        $user->lastName = 'TEST';
        $temp = mt_rand();
        $user->email = $temp . '@tisarana.org.au';
        $user->loginName = 'login' . $temp;
        $user->password = 'password';
        $this->assertTrue($user->save());
        $this->assertTrue(!is_null($user->id) && $user->id > 0);

        $test = new User($this->pm);
        $test->id = $user->id;

        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $user->creator);
        $this->assertTrue($test->firstName == $user->firstName);
        $this->assertTrue($test->lastName == $user->lastName);
        $this->assertTrue($test->email == $user->email);
        $this->assertTrue($test->loginName == $user->loginName);
        $this->assertTrue($test->password == $user->password);
    }

    public function testSaveUpdate() 
    {
        $user = new User($this->pm);
        $user->creator = 1;
        $user->firstName = 'FIRST';
        $user->lastName = 'TEST';
        $temp = mt_rand();
        $user->email = $temp . '@tisarana.org.au';
        $user->loginName = 'login' . $temp;
        $user->password = 'password';
        $this->assertTrue($user->save());
        $this->assertTrue(!is_null($user->id) && $user->id > 0);

        $user->firstName = 'FIRST 1';
        $user->lastName = 'TEST 1';
        $temp = mt_rand();
        $user->email = $temp . '@tisarana.org.au';
        $user->loginName = 'login' . $temp;
        $user->password = 'pa12w0rd';

        $user->save();

        $test = new User($this->pm);
        $test->id = $user->id;
        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $user->creator);
        $this->assertTrue($test->firstName == $user->firstName);
        $this->assertTrue($test->lastName == $user->lastName);
        $this->assertTrue($test->email == $user->email);
        $this->assertTrue($test->loginName == $user->loginName);
        $this->assertTrue($test->password == $user->password);
    }

    public function testDelete() 
    {
        $user = new User($this->pm);
        $user->creator = 1;
        $user->firstName = 'FIRST';
        $user->lastName = 'TEST';
        $temp = mt_rand();
        $user->email = $temp . '@tisarana.org.au';
        $user->loginName = 'login' . $temp;
        $user->password = 'password';
        $this->assertTrue($user->save());
        $this->assertTrue(!is_null($user->id) && $user->id > 0);

        $user->delete();

        $this->assertTrue(is_null($user->id) || $user->id == 0);
        $this->assertTrue(is_null($user->createDate) || strlen(trim($user->createDate)) <= 0);
        $this->assertTrue(is_null($user->updateDate) || strlen(trim($user->updateDate)) <= 0);
        $this->assertTrue(is_null($user->creator) || $user->creator <= 0);
        $this->assertTrue(is_null($user->firstName) || strlen(trim($user->firstName)) <= 0);
        $this->assertTrue(is_null($user->lastName) || strlen(trim($user->lastName)) <= 0);
        $this->assertTrue(is_null($user->email) || strlen(trim($user->email)) <= 0);
        $this->assertTrue(is_null($user->loginName) || strlen(trim($user->loginName)) <= 0);
        $this->assertTrue(is_null($user->password) || strlen(trim($user->password)) <= 0);
        $this->assertTrue(is_null($user->suspended) || $user->suspended == false);
        $this->assertTrue(is_null($user->suspendedDate) || strlen(trim($user->suspendedDate)) <= 0);
    }

    public function testEquals()
    {
        $user = new User($this->pm);
        $user->creator = 1;
        $user->firstName = 'FIRST';
        $user->lastName = 'TEST';
        $temp = mt_rand();
        $user->email = $temp . '@tisarana.org.au';
        $user->loginName = 'login' . $temp;
        $user->password = 'password';
        $this->assertTrue($user->save());
        $this->assertTrue(!is_null($user->id) && $user->id > 0);

        $user->read($user->id);

        $test = clone $user;

        $this->assertTrue($user->equals($test));

        $test->id = 99943000;
        $this->assertFalse($user->equals($test));

        $test->id = $user->id;
        $test->createDate = 'djksadkdsadsa';
        $this->assertFalse($user->equals($test));

        $test->createDate = $user->createDate;
        $test->updateDate = 'dsafgdgfgf';
        $this->assertFalse($user->equals($test));

        $test->updateDate = $user->updateDate;
        $test->creator = 999999;
        $this->assertFalse($user->equals($test));

        $test->creator = $user->creator;
        $test->firstName = 'fkreree';
        $this->assertFalse($user->equals($test));

        $test->firstName = $user->firstName;
        $test->lastName = 'dsdsadsa';
        $this->assertFalse($user->equals($test));

        $test->lastName = $user->lastName;
        $test->email = 'dsdsadsa';
        $this->assertFalse($user->equals($test));

        $test->email = $user->email;
        $test->loginName = 'dsdsadsa';
        $this->assertFalse($user->equals($test));

        $test->loginName = $user->loginName;
        $test->password = 'ererddsdsadsa';
        $this->assertFalse($user->equals($test));

        $test->password = $user->password;
        $test->suspended = !$user->suspended;
        $this->assertFalse($user->equals($test));

        $test->suspended = $user->suspended;
        $test->suspendedDate = 'fdrdrd';
        $this->assertFalse($user->equals($test));
    }

    public function testEqualsArray()
    {
        $user = new User($this->pm);
        $user->creator = 1;
        $user->firstName = 'FIRST';
        $user->lastName = 'TEST';
        $temp = mt_rand();
        $user->email = $temp . '@tisarana.org.au';
        $user->loginName = 'login' . $temp;
        $user->password = 'password';
        $this->assertTrue($user->save());
        $this->assertTrue(!is_null($user->id) && $user->id > 0);

        $user->read($user->id);

        $a = $user->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($user->equalsArray($a));

        $a['id'] = 999999099;
        $this->assertFalse($user->equalsArray($a));

        $a['id'] = $user->id;
        $a['createDate'] = 'djksadkdsadsa';
        $this->assertFalse($user->equalsArray($a));

        $a['createDate'] = $user->createDate;
        $a['updateDate'] = 'dsafgdgfgf';
        $this->assertFalse($user->equalsArray($a));

        $a['updateDate'] = $user->updateDate;
        $a['creator'] = 999999;
        $this->assertFalse($user->equalsArray($a));

        $a['creator'] = $user->creator;
        $a['firstName'] = 'fkreree';
        $this->assertFalse($user->equalsArray($a));

        $a['firstName'] = $user->firstName;
        $a['lastName'] = 'dsdsadsa';
        $this->assertFalse($user->equalsArray($a));

        $a['lastName'] = $user->lastName;
        $a['email'] = 'dsdsadsa';
        $this->assertFalse($user->equalsArray($a));

        $a['email'] = $user->email;
        $a['loginName'] = 'dsdsadsa';
        $this->assertFalse($user->equalsArray($a));

        $a['loginName'] = $user->loginName;
        $a['password'] = 'ererddsdsadsa';
        $this->assertFalse($user->equalsArray($a));

        $a['password'] = $user->password;
        $a['suspended'] = !$user->suspended;
        $this->assertFalse($user->equalsArray($a));

        $a['suspended'] = $user->suspended;
        $a['suspendedDate'] = 'fdrdrd';
        $this->assertFalse($user->equalsArray($a));
    }

    public function testToArray()
    {
        $user = new User($this->pm);
        $user->creator = 1;
        $user->firstName = 'FIRST';
        $user->lastName = 'TEST';
        $temp = mt_rand();
        $user->email = $temp . '@tisarana.org.au';
        $user->loginName = 'login' . $temp;
        $user->password = 'password';
        $this->assertTrue($user->save());
        $this->assertTrue(!is_null($user->id) && $user->id > 0);

        $user->read($user->id);

        $a = $user->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($user->equalsArray($a));
    }

    public function testFromArray()
    {
        $user = new User($this->pm);

        $temp = mt_rand();

        $a = array('id' => 1, 
                    'createDate' => '2014-01-01 00:00:01', 
                    'updateDate' => '2001-01-01 00:00:01',
                    'creator' => 1,
                    'firstName' => 'firstName',
                    'lastName' => 'lastName',
                    'email' => $temp . '@tisarana.org.au',
                    'loginName' => 'login' . $temp,
                    'password' => 'password' . $temp,
                    'suspended' => false,
                    'suspendedDate' => '');

        $user->fromArray($a);

        $this->assertTrue($user->equalsArray($a));
    }

    public function testToJson()
    {
        $user = new User($this->pm);
        $user->creator = 1;
        $user->firstName = 'FIRST';
        $user->lastName = 'TEST';
        $temp = mt_rand();
        $user->email = $temp . '@tisarana.org.au';
        $user->loginName = 'login' . $temp;
        $user->password = 'password';
        $this->assertTrue($user->save());
        $this->assertTrue(!is_null($user->id) && $user->id > 0);

        $user->read($user->id);

        $s = $user->toJson();
        $this->assertTrue(strlen(trim($s)) > 0);
        $a = json_decode($s, true);
        $this->assertTrue($user->equalsArray($a));
    }

    public function testFromJson()
    {
        $user = new User($this->pm);
        $user->creator = 1;
        $user->firstName = 'FIRST';
        $user->lastName = 'TEST';
        $temp = mt_rand();
        $user->email = $temp . '@tisarana.org.au';
        $user->loginName = 'login' . $temp;
        $user->password = 'password';
        $this->assertTrue($user->save());
        $this->assertTrue(!is_null($user->id) && $user->id > 0);

        $user->read($user->id);

        $s = $user->toJson();

        $test = new User($this->pm);
        $test->fromJson($s);
        $this->assertTrue($user->equals($test));
    }
}

?>
