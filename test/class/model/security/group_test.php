<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/test_model.php');
include_once(BASE_PATH . 'test/class/model/security/group_double.php');

/*
 *  Unit testing the Group class.
 */

class GroupModelTest extends TestModel
{
//
// TESTING THE VALIDATION OF ID
//
    public function testValidate()
    {
        $this->validationObject = new GroupDouble($this->pm);
        $this->validationAttributes = 
            array('id' => '',
                    'name' => 'name');
        $this->_testValidate(); 
    }

//
// TEST SAVE
//
    public function testSaveGroup() 
    {
        $group = new Group($this->pm);
        $group->creator = 1;
        $group->name = 'TEST GROUP'. mt_rand();
        $this->assertTrue($group->save());
        $this->assertTrue(!is_null($group->id) && $group->id > 0);
    }

    public function testRead() 
    {
        $group = new Group($this->pm);
        $group->creator = 1;
        $group->name = 'TEST GROUP'. mt_rand();
        $this->assertTrue($group->save());
        $this->assertTrue(!is_null($group->id) && $group->id > 0);

        $test = new Group($this->pm);
        $test->id = $group->id;

        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $group->creator);
        $this->assertTrue($test->name == $group->name);
    }

    public function testSaveUpdate() 
    {
        $group = new Group($this->pm);
        $group->creator = 1;
        $group->name = 'TEST GROUP'. mt_rand();
        $this->assertTrue($group->save());
        $this->assertTrue(!is_null($group->id) && $group->id > 0);

        $group->name = 'TEST GROUP'. mt_rand();

        $group->save();

        $test = new Group($this->pm);
        $test->id = $group->id;
        $this->assertTrue($test->read());
        $this->assertTrue($test->creator == $group->creator);
        $this->assertTrue($test->name == $group->name);
    }

    public function testDelete() 
    {
        $group = new Group($this->pm);
        $group->creator = 1;
        $group->name = 'TEST GROUP'. mt_rand();
        $this->assertTrue($group->save());
        $this->assertTrue(!is_null($group->id) && $group->id > 0);

        $group->delete();

        $this->assertTrue(is_null($group->id) || $group->id == 0);
        $this->assertTrue(is_null($group->createDate) || strlen(trim($group->createDate)) <= 0);
        $this->assertTrue(is_null($group->updateDate) || strlen(trim($group->updateDate)) <= 0);
        $this->assertTrue(is_null($group->creator) || $group->creator <= 0);
        $this->assertTrue(is_null($group->name) || strlen(trim($group->name)) <= 0);
    }

    public function testEquals()
    {
        $group = new Group($this->pm);
        $group->creator = 1;
        $group->name = 'TEST GROUP'. mt_rand();
        $this->assertTrue($group->save());
        $this->assertTrue(!is_null($group->id) && $group->id > 0);

        $group->read($group->id);

        $test = clone $group;

        $this->assertTrue($group->equals($test));

        $test->id = 99943000;
        $this->assertFalse($group->equals($test));

        $test->id = $group->id;
        $test->createDate = 'djksadkdsadsa';
        $this->assertFalse($group->equals($test));

        $test->createDate = $group->createDate;
        $test->updateDate = 'dsafgdgfgf';
        $this->assertFalse($group->equals($test));

        $test->updateDate = $group->updateDate;
        $test->creator = 999999;
        $this->assertFalse($group->equals($test));

        $test->creator = $group->creator;
        $test->name = 'fkreree';
        $this->assertFalse($group->equals($test));
    }

    public function testEqualsArray()
    {
        $group = new Group($this->pm);
        $group->creator = 1;
        $group->name = 'TEST GROUP'. mt_rand();
        $this->assertTrue($group->save());
        $this->assertTrue(!is_null($group->id) && $group->id > 0);

        $group->read($group->id);

        $a = $group->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($group->equalsArray($a));

        $a['id'] = 999999099;
        $this->assertFalse($group->equalsArray($a));

        $a['id'] = $group->id;
        $a['createDate'] = 'djksadkdsadsa';
        $this->assertFalse($group->equalsArray($a));

        $a['createDate'] = $group->createDate;
        $a['updateDate'] = 'dsafgdgfgf';
        $this->assertFalse($group->equalsArray($a));

        $a['updateDate'] = $group->updateDate;
        $a['creator'] = 999999;
        $this->assertFalse($group->equalsArray($a));

        $a['creator'] = $group->creator;
        $a['name'] = 'fkreree';
        $this->assertFalse($group->equalsArray($a));
    }

    public function testToArray()
    {
        $group = new Group($this->pm);
        $group->creator = 1;
        $group->name = 'TEST GROUP'. mt_rand();
        $this->assertTrue($group->save());
        $this->assertTrue(!is_null($group->id) && $group->id > 0);

        $group->read($group->id);

        $a = $group->toArray();

        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);
        $this->assertTrue($group->equalsArray($a));
    }

    public function testFromArray()
    {
        $group = new Group($this->pm);

        $temp = mt_rand();

        $a = array('id' => 1, 
                    'createDate' => '2014-01-01 00:00:01', 
                    'updateDate' => '2001-01-01 00:00:01',
                    'creator' => 1,
                    'name' => 'name');

        $group->fromArray($a);

        $this->assertTrue($group->equalsArray($a));
    }

    public function testToJson()
    {
        $group = new Group($this->pm);
        $group->creator = 1;
        $group->name = 'TEST GROUP'. mt_rand();
        $this->assertTrue($group->save());
        $this->assertTrue(!is_null($group->id) && $group->id > 0);

        $group->read($group->id);

        $s = $group->toJson();
        $this->assertTrue(strlen(trim($s)) > 0);
        $a = json_decode($s, true);
        $this->assertTrue($group->equalsArray($a));
    }

    public function testFromJson()
    {
        $group = new Group($this->pm);
        $group->creator = 1;
        $group->name = 'TEST GROUP'. mt_rand();
        $this->assertTrue($group->save());
        $this->assertTrue(!is_null($group->id) && $group->id > 0);

        $group->read($group->id);

        $s = $group->toJson();

        $test = new Group($this->pm);
        $test->fromJson($s);
        $this->assertTrue($group->equals($test));
    }
}

?>
