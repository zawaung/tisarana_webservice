<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/test_dao.php');
include_once(BASE_PATH . 'class/data/security/object_dao.php');
include_once(BASE_PATH . 'class/data/dao_exception.php');

class ObjectDAOTest extends TestDAO
{
    protected $dao = null;
    protected $ids = array();
    
    public function __construct()
    {
        parent::__construct();
        $this->dao = new ObjectDAO($this->pdo);
    }

    public function __destruct()
    {
        parent::__destruct();
        $this->dao = null;
    }

//
// OBJECT 
//
    
    // test null values array when calling insert resulted in DAOException 
    public function testInsertNullValueArray()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insert(NULL);
    }

    // test null or '0' value in first name when calling insert resulted in DAOException
    public function testInsertNullName()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insert(array());
    } 


    // test null or '0' value in creator when calling insert resulted in DAOException 
    public function testInsertNullCreator()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insert(array('name'=>'Admin')); 
    }

    // test when calling insert with valid values would function as expected 
    public function testInsert()
    {
        $data = array('creator'=>1, 'name'=>'Admin'); 

        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $this->assertTrue($id > 0);
        $this->ids[] = $id;
    }

    // test null value in id when calling select resulted in DAOException 
    public function testSelectNullID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->select(NULL);
    }

    // test non-numeric value in id when calling select resulted in DAOException 
    public function testSelectNonNumericID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->select('a');
    }

    // test calling select with valid id return an array with data
    public function testSelect()
    {
        $data = array('creator'=>1, 'name'=>'Admin Select'); 

        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $a = $this->dao->select($id);
        $this->ids[] = $id;

        $this->assertNotNull($a);
        $this->assertTrue(is_array($a));
        $this->assertEquals($data['name'], $a['name']);
        $this->assertEquals($data['creator'], $a['creator']);
    }

    // test null value in id when calling delete resulted in DAOException 
    public function testDeleteNullID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->delete(NULL);
    }

    // test non-numeric value in id when calling delete resulted in DAOException 
    public function testDeleteNonNumericID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->delete('a');
    }

    // test calling delete with valid id work
    public function testDelete()
    {
        $data = array('creator'=>1, 'name'=>'Admin Delete'); 

        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        try {
            $this->pdo->beginTransaction();
            $this->assertTrue($this->dao->delete($id));
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }
        
        $a = $this->dao->select($id);

        $this->assertTrue(count($a) <= 0);
    }

    // test null value in id when calling update resulted in DAOException 
    public function testUpdateNullID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->update(NULL, array());
    }

    // test non-numeric value in id when calling update resulted in DAOException 
    public function testUpdateNonNumericID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->update('a', array());
    }

    // test null values array when calling update results in DAOException
    public function testUpdateNullValueArray()
    {
        $this->setExpectedException('DAOException');
        $this->dao->update(1, NULL);
    }

    // test calling update work 
    public function testUpdate()
    {
        $data = array('creator'=>1, 'name'=>'Admin Update'); 

        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        try {
            $data['creator'] = 2;
            $data['name'] = 'Admin Update 1';
            $this->pdo->beginTransaction();
            $this->dao->update($id, $data); 
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $a = $this->dao->select($id);
        $this->assertEquals($data['name'], $a['name']);
        $this->assertEquals($data['creator'], $a['creator']);
    }
    
    public function testCount()
    {
        $data = array('creator'=>1, 'name'=>'Admin Count'); 
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $cnt = $this->dao->count();
        $this->assertTrue($cnt > 0);
    }

    public function testSelectListEmptyPage()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList('', 20);
    }

    public function testSelectListNonNumericPage()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList('a', 20);
    }

    public function testSelectListEmptyPageSize()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList(1, '');
    }

    public function testSelectListNonNumericPageSize()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList(1, 'a');
    }

    public function testSelectList()
    {
        try {
            $this->pdo->beginTransaction();
            
            for ($i=1; $i<=20;$i++) {
                $id = $this->dao->insert(array('creator'=>1, 'name'=>"LIST $i"));
            }

            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }
       
        $a = $this->dao->selectList(1, 20);
        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) >= 20);
        $this->assertTrue(is_array($a[0]));
    }
}

?>
