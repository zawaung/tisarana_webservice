<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/test_dao.php');
include_once(BASE_PATH . 'class/data/security/acl_dao.php');
include_once(BASE_PATH . 'class/data/security/user_dao.php');
include_once(BASE_PATH . 'class/data/security/group_dao.php');
include_once(BASE_PATH . 'class/data/security/action_dao.php');
include_once(BASE_PATH . 'class/data/security/object_dao.php');
include_once(BASE_PATH . 'class/data/dao_exception.php');

class AclDAOTest extends TestDAO
{
    protected $dao = null;
    protected $ids = array();
    protected $udao = null;
    protected $gdao = null;
    protected $adao = null;
    protected $odao = null;

    public function __construct()
    {
        parent::__construct();
        $this->dao = new AclDAO($this->pdo);
        $this->udao = new UserDAO($this->pdo);
        $this->gdao = new GroupDAO($this->pdo);
        $this->adao = new ActionDAO($this->pdo);
        $this->odao = new ObjectDAO($this->pdo);
    }

    public function __destruct()
    {
        parent::__destruct();

        $this->dao = null;
        $this->udao = null;
        $this->gdao = null;
        $this->adao = null;
        $this->ids = null;
        $this->odao = null;
    }

    public function initData()
    {
        $r = array();

        $this->pdo->beginTransaction();
        try {
            $r['pid'] = $this->udao->insert(array('creator'=>1, 
                                        'first_name'=>'TEST', 
                                        'last_name'=>'acl', 
                                        'login_name'=>'testacl',
                                        'password'=>'password',
                                        'email'=>'testacl@test.com',
                                        'suspended'=>'N'));
            $r['oid'] = $this->odao->insert(array('name'=>'TEST ACL', 'creator'=>$r['pid']));
            $r['aid'] = $this->adao->insert(array('creator'=>$r['pid'], 'name'=>'DELETE', 'object'=>$r['oid']));
            $this->pdo->commit();
        }
        catch (Exception $e) {
            $e->getMessage();
            $this->pdo->rollback();
        }

        return $r;
    }

    public function cleanupData($r)
    {
        $this->pdo->beginTransaction();
        $this->adao->delete($r['aid']);
        $this->odao->delete($r['oid']);
        $this->udao->delete($r['pid']);
        $this->pdo->commit();
    }

//
// ACL 
//
    // test when calling insert with valid values would function as expected 
    public function testInsert()
    {
        $r = $this->initData();
        $ret = false;

        try {
            $this->pdo->beginTransaction();
            $ret = $this->dao->insert(array('principal'=>$r['pid'], 'principal_type'=>'U', 'action'=>$r['aid'], 'creator'=>$r['pid'])); 
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            $this->pdo->rollBack();
        }

        $this->assertTrue($ret);
        $this->cleanupData($r);
    }
 
    // test calling select with valid id return an array with data
    public function testSelect()
    {
        $r = $this->initData();
        $this->pdo->beginTransaction();
        $ret = $this->dao->insert(array('principal'=>$r['pid'], 'principal_type'=>'U', 'action'=>$r['aid'], 'creator'=>$r['pid'])); 
        $this->pdo->commit();

        $a = $this->dao->select(array('principal'=>$r['pid'], 'principal_type'=>'U', 'action'=>$r['aid']));

        $this->assertNotNull($a);
        $this->assertTrue(is_array($a));
        $this->assertEquals($r['pid'], $a['principal']);
        $this->assertEquals('U', $a['principal_type']);
        $this->assertEquals($r['aid'], $a['action']);
        $this->assertEquals($r['pid'], $a['creator']);
        $this->cleanupData($r);
    }

    // test calling delete with valid id work
    public function testDelete()
    {
        $r = $this->initData();
        $this->pdo->beginTransaction();
        $ret = $this->dao->insert(array('principal'=>$r['pid'], 'principal_type'=>'U', 'action'=>$r['aid'], 'creator'=>$r['pid'])); 
        $this->pdo->commit();

        try {
            $this->pdo->beginTransaction();
            $this->assertTrue($this->dao->delete(array('principal'=>$r['pid'], 'principal_type'=>'U', 'action'=>$r['aid'])));
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            $this->pdo->rollBack();
        }
        
        $a = $this->dao->select(array('principal'=>$r['pid'], 'principal_type'=>'U', 'action'=>$r['aid']));

        $this->assertTrue(count($a) <= 0);
        $this->cleanupData($r);
    }
}

?>
