<?php

define('BASE_PATH', '');

include_once(BASE_PATH . 'test/test_dao.php');
include_once(BASE_PATH . 'class/data/security/group_member_dao.php');
include_once(BASE_PATH . 'class/data/dao_exception.php');

class GroupMemberDAOTest extends TestDAO
{
    protected $dao = null;
    protected $ids = array();
    
    public function __construct()
    {
        parent::__construct();
        $this->dao = new GroupMemberDAO($this->pdo);
    }

    public function __destruct()
    {
        parent::__destruct();
        $this->dao = null;
    }

//
// GROUP MEMBER
//
    
    // test null values array when calling insert resulted in DAOException 
    public function testInsertNullValueArray()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insert(NULL);
    }

    // test null or '0' value in group id when calling insert resulted in DAOException
    public function testInsertNullGroup()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insert(array());
    } 


    // test null or '0' value in use id when calling insert resulted in DAOException 
    public function testInsertNullUser()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insert(array('group_id'=>1)); 
    }

    // test when calling insert with valid values would function as expected 
    public function testInsert()
    {
        $data = array('group_id'=>1, 'user_id'=>mt_rand(), 'creator'=>1); 

        $id = false;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $this->assertTrue($id);
    }

    // test null value in compomd key when calling select resulted in DAOException 
    public function testSelectNullKey()
    {
        $this->setExpectedException('DAOException');
        $this->dao->select(NULL);
    }

    // test non-array value in compound key when calling select resulted in DAOException 
    public function testSelectNonArrayKey()
    {
        $this->setExpectedException('DAOException');
        $this->dao->select('a');
    }

    // test non-numeric value in keys when calling select resulted in DAOException 
    public function testSelectNonNumericKey()
    {
        $this->setExpectedException('DAOException');
        $this->dao->select(array('group_id' => 'a'));
    }

    // test calling select with valid key return an array with data
    public function testSelect()
    {
        $data = array('group_id'=>1, 'user_id'=>mt_rand(), 'creator' => 1); 

        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $a = $this->dao->select($data);

        $this->assertNotNull($a);
        $this->assertTrue(is_array($a));
        $this->assertEquals($data['group_id'], $a['group_id']);
        $this->assertEquals($data['user_id'], $a['user_id']);
    }

    // test null value in key when calling delete resulted in DAOException 
    public function testDeleteNullKey()
    {
        $this->setExpectedException('DAOException');
        $this->dao->delete(NULL);
    }

    // test non array value in key when calling delete resulted in DAOException 
    public function testDeleteNonArrayKey()
    {
        $this->setExpectedException('DAOException');
        $this->dao->delete('a');
    }

    // test non-numeric value in key when calling delete resulted in DAOException 
    public function testDeleteNonNumericKey()
    {
        $this->setExpectedException('DAOException');
        $this->dao->delete(array('group_id' => 'a'));
    }

    // test calling delete with valid id work
    public function testDelete()
    {
        $data = array('group_id'=>1, 'user_id'=>mt_rand(), 'creator'=>1); 

        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        try {
            $this->pdo->beginTransaction();
            $this->assertTrue($this->dao->delete($data));
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }
        
        $a = $this->dao->select($data);

        $this->assertTrue(count($a) <= 0);
    }

    public function testCount()
    {
        $data = array('group_id'=>1, 'user_id'=>mt_rand(), 'creator'=>1); 
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $cnt = $this->dao->count();
        $this->assertTrue($cnt > 0);
    }

    public function testSelectListEmptyPage()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList('', 20);
    }

    public function testSelectListNonNumericPage()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList('a', 20);
    }

    public function testSelectListEmptyPageSize()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList(1, '');
    }

    public function testSelectListNonNumericPageSize()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList(1, 'a');
    }

    public function testSelectList()
    {
        try {
            $this->pdo->beginTransaction();
            
            for ($i=1; $i<=20;$i++) {
                $id = $this->dao->insert(array('group_id'=>1, 'user_id'=>mt_rand(), 'creator'=>1));
            }

            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }
       
        $a = $this->dao->selectList(1, 20);
        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) >= 20);
        $this->assertTrue(is_array($a[0]));
    }
}

?>
