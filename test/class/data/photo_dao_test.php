<?php

define('BASE_PATH', '');
include_once(BASE_PATH . 'test/test_dao.php');
include_once(BASE_PATH . 'class/data/photo_dao.php');
include_once(BASE_PATH . 'class/data/dao_exception.php');

class PhotoDAOTest extends TestDAO
{
    protected $dao = null;
    protected $ids = array();
    
    public function __construct()
    {
        parent::__construct();
        $this->dao = new PhotoDAO($this->pdo);
    }

    public function __destruct()
    {
        parent::__destruct();
        $this->dao = null;
    }

//
// ALBUM
//
    
    // test null values array when calling insert resulted in DAOException 
    public function testInsertNullValueArray()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insert(NULL);
    }

    // test null value in creator when calling insert resulted in DAOException 
    public function testInsertNullCreator()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insert(array());
    }

    // test '0' value in creator when calling insert resulted in DAOException 
    public function testInsertZeroCreator()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insert(array('creator'=>0));
    }

    // test null value in album when calling insert resulted in DAOException 
    public function testInsertNullAlbum()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insert(array('creator'=>1));
    }

    // test '0' value in album when calling insert resulted in DAOException 
    public function testInsertZeroAlbum()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insert(array('creator'=>1, 'album'=>0));
    }

    // test null value in file when calling insert resulted in DAOException
    public function testInsertNullFile()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insert(array('creator'=>'1'));
    } 

    // test blank value in file when calling insert resulted in DAOException
    public function testInsertBlankFile()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insert(array('creator'=>'1', 'file'=>''));
    } 

    // test when calling insert with valid values would function as expected 
    public function testInsert()
    {
        $data = array('creator'=>1, 'album'=> 1, 'file'=>'TEST', 'description'=>'TESTING INSERT');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $this->assertTrue($id > 0);
        $this->ids[] = $id;
    }

    // test null value in id when calling select resulted in DAOException 
    public function testSelectNullID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->select(NULL);
    }

    // test non-numeric value in id when calling select resulted in DAOException 
    public function testSelectNonNumericID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->select('a');
    }

    // test calling select with valid id return an array with data
    public function testSelect()
    {
        $data = array('creator'=>1, 'album'=> 1, 'file'=>'TEST', 'description'=>'TESTING SELECT');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $a = $this->dao->select($id);
        $this->ids[] = $id;

        $this->assertNotNull($a);
        $this->assertTrue(is_array($a));
        $this->assertEquals($data['creator'], $a['creator']);
        $this->assertEquals($data['album'], $a['album']);
        $this->assertEquals($data['file'], $a['file']);
        $this->assertEquals($data['description'], $a['description']);
    }

    // test null value in album id when calling select album photo resulted in DAOException 
    public function testSelectAlbumPhotoNullID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->selectAlbumPhoto(NULL);
    }

    // test non-numeric value in album id when calling select album photo resulted in DAOException 
    public function testSelectAlbumPhotoNonNumericID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->selectAlbumPhoto('a');
    }

    // test calling select album photo with valid album id return an array with data
    public function testSelectAlbumPhoto()
    {
        $data = array('creator'=>1, 'album'=> 1, 'file'=>'TEST', 'description'=>'TESTING ALBUM PHOTO SELECT');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $a = $this->dao->selectAlbumPhoto(1);
        $this->ids[] = $id;

        $this->assertNotNull($a);
        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) > 0);

        foreach ($a as $b) {
            $this->assertTrue(is_array($a));
            $this->assertTrue(count($a) > 0);
        }
    }

    // test null value in id when calling delete resulted in DAOException 
    public function testDeleteNullID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->delete(NULL);
    }

    // test non-numeric value in id when calling delete resulted in DAOException 
    public function testDeleteNonNumericID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->delete('a');
    }

    // test calling delete photo with valid id work
    public function testDelete()
    {
        $data = array('creator'=>1, 'album'=> 1, 'file'=>'TEST', 'description'=>'TESTING DELETE PHOTO');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        try {
            $this->pdo->beginTransaction();
            $this->assertTrue($this->dao->delete($id));
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }
        
        $a = $this->dao->select($id);

        $this->assertTrue(count($a) <= 0);
    }

    // test null value in album id when calling delete album photo resulted in DAOException 
    public function testDeleteAlbumPhotoNullID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->deleteAlbumPhoto(NULL);
    }

    // test non-numeric value in album id when calling delete album photo resulted in DAOException 
    public function testDeleteAlbumPhotoNonNumericID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->deleteAlbumPhoto('a');
    }

    // test calling delete album photo with valid album id work
    public function testDeleteAlbumPhoto()
    {
        $data = array('creator'=>1, 'album'=> 1, 'file'=>'TEST', 'description'=>'TESTING ALBUM PHOTO DELETE');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        try {
            $this->pdo->beginTransaction();
            $this->assertTrue($this->dao->deleteAlbumPhoto(1));
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }
        
        $a = $this->dao->selectAlbumPhoto(1);

        $this->assertTrue(count($a) <= 0);
    }

    // test null value in id when calling update resulted in DAOException 
    public function testUpdateNullID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->update(NULL, array());
    }

    // test non-numeric value in id when calling update resulted in DAOException 
    public function testUpdateNonNumericID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->update('a', array());
    }

    // test null values array when calling update results in DAOException
    public function testUpdateNullValueArray()
    {
        $this->setExpectedException('DAOException');
        $this->dao->update(1, NULL);
    }

    // test calling update with null album id result in DAOException 
    public function testUpdateNullAlbumId()
    {
        $data = array('creator'=>1, 'album'=> 1, 'file'=>'whatever', 'description'=>'whatever');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $this->setExpectedException('DAOException');

        try {
            $this->pdo->beginTransaction();
            $this->dao->update($id, array('creator'=>2, 
                                    'album'=> NULL, 
                                    'file'=>'TEST', 
                                    'description'=>'TESTING UPDATE'));
            $this->pdo->commit();
        } 
        finally {
            $this->pdo->rollBack();
        }
    }

    // test calling update with non numeric album id result in DAOException 
    public function testUpdateNonNumericAlbumId()
    {
        $data = array('creator'=>1, 'album'=> 1, 'file'=>'whatever', 'description'=>'whatever');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $this->setExpectedException('DAOException');

        try {
            $this->pdo->beginTransaction();
            $this->dao->update($id, array('creator'=>2, 
                                    'album'=> 'a', 
                                    'file'=>'TEST', 
                                    'description'=>'TESTING UPDATE'));
            $this->pdo->commit();
        } 
        finally {
            $this->pdo->rollBack();
        }
    }

    // test calling update with zero album id result in DAOException 
    public function testUpdateZeroAlbumId()
    {
        $data = array('creator'=>1, 'album'=> 1, 'file'=>'whatever', 'description'=>'whatever');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $this->setExpectedException('DAOException');

        try {
            $this->pdo->beginTransaction();
            $this->dao->update($id, array('creator'=>2, 
                                    'album'=> 0, 
                                    'file'=>'TEST', 
                                    'description'=>'TESTING UPDATE'));
            $this->pdo->commit();
        } 
        finally {
            $this->pdo->rollBack();
        }
    }

    // test calling update with null file result in DAOException 
    public function testUpdateNullFile()
    {
        $data = array('creator'=>1, 'album'=> 1, 'file'=>'whatever', 'description'=>'whatever');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $this->setExpectedException('DAOException');

        try {
            $this->pdo->beginTransaction();
            $this->dao->update($id, array('creator'=>2, 
                                    'album'=> 2, 
                                    'file'=>NULL, 
                                    'description'=>'TESTING UPDATE'));
            $this->pdo->commit();
        } 
        finally {
            $this->pdo->rollBack();
        }
    }

    // test calling update with blank file result in DAOException 
    public function testUpdateBlankFile()
    {
        $data = array('creator'=>1, 'album'=> 1, 'file'=>'whatever', 'description'=>'whatever');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $this->setExpectedException('DAOException');

        try {
            $this->pdo->beginTransaction();
            $this->dao->update($id, array('creator'=>2, 
                                    'album'=> 2, 
                                    'file'=>'', 
                                    'description'=>'TESTING UPDATE'));
            $this->pdo->commit();
        } 
        finally {
            $this->pdo->rollBack();
        }
    }

    // test calling update work 
    public function testUpdate()
    {
        $data = array('creator'=>1, 'album'=> 1, 'file'=>'whatever', 'description'=>'whatever');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        try {
            $this->pdo->beginTransaction();
            $this->dao->update($id, array('creator'=>2, 
                                    'album'=> 2, 
                                    'file'=>'TEST', 
                                    'description'=>'TESTING UPDATE'));
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $a = $this->dao->select($id);
        $this->assertTrue($a['creator'] == 2);
        $this->assertTrue($a['album'] == 2);
        $this->assertTrue($a['file'] == 'TEST');
        $this->assertTrue($a['description'] == 'TESTING UPDATE');
    }

    public function testCount()
    {
        $data = array('creator'=>1, 'album'=> 1, 'file'=>'TEST', 'description'=>'TESTING COUNT');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $cnt = $this->dao->count();
        $this->assertTrue($cnt > 0);
    }

    // test null value in album id when calling count album photo resulted in DAOException 
    public function testCountAlbumPhotoNullID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->countAlbumPhoto(NULL);
    }

    // test non-numeric value in album id when calling count album photo resulted in DAOException 
    public function testCountAlbumPhotoNonNumericID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->countAlbumPhoto('a');
    }

    public function testCountAlbumPhoto()
    {
        $data = array('creator'=>1, 'album'=> 1, 'file'=>'TEST', 'description'=>'TESTING ALBUM COUNT');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $cnt = $this->dao->countAlbumPhoto(1);
        $this->assertTrue($cnt > 0);
    }

    // test null values array when calling insert list resulted in DAOException 
    public function testInsertListNullList()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insertList(NULL);
    }

    public function testInsertListNotArray()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insertList('a');
    }
    
    // test null or '0' value in creator when calling insert list resulted in DAOException 
    public function testInsertListNullCreator()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insertList(array(array()));
    }

    // test null value in album when calling insert list resulted in DAOException 
    public function testInsertListNullAlbum()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insertList(array(array('creator'=>1)));
    }

    // test '0' value in album when calling insert list resulted in DAOException 
    public function testInsertListZeroAlbum()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insertList(array(array('creator'=>1, 'album'=>0)));
    }

    // test null value in file when calling insert list resulted in DAOException
    public function testInsertListNullFile()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insertList(array(array('creator'=>'1')));
    } 

    // test blank value in file when calling insert list resulted in DAOException
    public function testInsertListBlankFile()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insertList(array(array('creator'=>'1', 'file'=>'')));
    } 

    // test when calling insert list with valid values would function as expected 
    public function testInsertList()
    {
        $data = array();

        for ($i=1; $i<=20; $i++) {
            $data[] = array('creator'=>1, 'album'=> 1, 'file'=>"TEST $i", 'description'=>'TESTING INSERT LIST');
        }

        $id = NULL;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insertList($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $this->assertTrue(is_array($id));
        $this->assertEquals(count($id), 20);
        $this->ids[] = array_merge($this->ids, $id);
    }

    public function testSelectListEmptyPage()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList('', 20);
    }

    public function testSelectListNonNumericPage()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList('a', 20);
    }

    public function testSelectListEmptyPageSize()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList(1, '');
    }

    public function testSelectListNonNumericPageSize()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList(1, 'a');
    }

    public function testSelectList()
    {
        $data = array();

        for ($i=1; $i<=20; $i++) {
            $data[] = array('creator'=>1, 'album'=> 1, 'file'=>"TEST $i", 'description'=>'TESTING INSERT LIST');
        }

        $id = NULL;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insertList($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }
       
        $a = $this->dao->selectList(1, 20);
        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) >= 20);
        $this->assertTrue(is_array($a[0]));
    }
}

?>
