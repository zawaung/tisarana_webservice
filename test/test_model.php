<?php

include_once(BASE_PATH . 'test/config.php');
include_once(BASE_PATH . 'class/model/persistent_manager.php');

class TestModel extends PHPUnit_Framework_TestCase
{
    protected $pm = null;
    protected $validationAttributes = array();
    protected $validationObject = null;

    public function __construct()
    {
        $this->pm = new PersistentManager();
        $this->pm->connect(DB_DSN, DB_USER, DB_PWD);
    }

    public function _testValidate()
    {
        $z = count($this->validationAttributes);

        foreach ($this->validationAttributes as $attr => $val) {
            if ('id' == $attr) 
                $this->_testID();
            else 
                $this->_testString($attr, $val);
        }
    }

    public function _testID()
    {
        $id = 1;

    // CHECK UNDEFINED OR NULL 
        $this->validationObject->id = NULL;
        $this->validationObject->validate();
        $a = $this->validationObject->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'id') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NON NUMERIC 
        $this->validationObject->id = 'a';
        $this->validationObject->validate();
        $a = null;
        $a = $this->validationObject->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'id') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NOT GREATER THAN ZERO
        $this->validationObject->id = 0;
        $this->validationObject->validate();
        $a = null;
        $a = $this->validationObject->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'id') {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $this->validationObject->id = $id;
        $this->validationObject->validate();
        $a = null;
        $a = $this->validationObject->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == 'id') {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }

    public function _testString($attr, $val)
    {
    // CHECK UNDEFINED OR NULL 
        $this->validationObject->validate();
        $a = $this->validationObject->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == $attr) {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);
    
     // CHECK BLANK  
        $this->validationObject->{$attr} = ' ';
        $this->validationObject->validate();
        $a = $this->validationObject->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == $attr) {
                $found = true;
                break;
            } 
        }

        $this->assertTrue($found);

    // CHECK NORMAL OR NOTHING WRONG CASE
        $this->validationObject->{$attr} = $val;
        $this->validationObject->validate();
        $a = null;
        $a = $this->validationObject->getError();
        $this->assertNotNull($a);

        $found = false;
        foreach ($a as $b) {
            if ($b['item'] == $attr) {
                $found = true;
                break;
            } 
        }

        $this->assertFalse($found);
    }
}

?>
