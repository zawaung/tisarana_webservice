<?php

// 
// Error reporting is disable to stop PHP's error and warning messages messing up with JSON response
//
error_reporting(0);

include_once('../../class/rest/video_rest.php');

$obj = new VideoRest();
$obj->run();

error_reporting(1);

?>
