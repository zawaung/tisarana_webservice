<?php

// 
// Error reporting is disable to stop PHP's error and warning messages messing up with JSON response
//
$er = 0;
if (isset($_GET['er']))
    $er = 1;

error_reporting($er);

require_once(dirname(__FILE__) . '/../../conf.php');
include_once(BASE_PATH . 'class/rest/news_rest.php');

$obj = new NewsRest();
$obj->run();

error_reporting(1);

?>
